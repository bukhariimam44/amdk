<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});
Route::redirect('/','login');
Route::get('/catalog', [App\Http\Controllers\CatalogController::class, 'index'])->name('catalog');
Route::get('logs', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
Route::get('/daftar', [App\Http\Controllers\HomeController::class, 'daftar'])->name('daftar');
Route::post('daftar', [App\Http\Controllers\Auth\RegisterController::class, 'daftar'])->name('daftar');


Auth::routes(['register'=>true]);


Route::get('/provinsi', [App\Http\Controllers\BaseController::class, 'provinsi'])->name('provinsi');
Route::post('/kabupaten', [App\Http\Controllers\BaseController::class, 'kabupaten'])->name('kabupaten');
Route::post('/kecamatan', [App\Http\Controllers\BaseController::class, 'kecamatan'])->name('kecamatan');
Route::post('/kelurahan', [App\Http\Controllers\BaseController::class, 'kelurahan'])->name('kelurahan');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('auth');

Route::middleware(['role:admin,principle,distributor,agen'])->group(function () {

	Route::get('akumulasi', [App\Http\Controllers\HomeController::class, 'akumulasi'])->name('agen-akumulasi');
	Route::post('akumulasi', [App\Http\Controllers\HomeController::class, 'akumulasi'])->name('agen-akumulasi-search');
});

Route::group(['prefix' => 'api','middleware'=>'auth'], function () {
	Route::post('/grafik', [App\Http\Controllers\HomeController::class, 'statistik'])->name('grafik');

	Route::get('/pabrik', [App\Http\Controllers\HomeController::class, 'pabrik'])->name('pabrik');
	Route::get('/supplier', [App\Http\Controllers\HomeController::class, 'supplier'])->name('supplier');
	Route::get('/material', [App\Http\Controllers\HomeController::class, 'material'])->name('material');
});

Route::group(['prefix' => 'api','middleware'=>'auth'], function () {
	Route::get('/pabrik', [App\Http\Controllers\HomeController::class, 'pabrik'])->name('pabrik');
	Route::get('/supplier', [App\Http\Controllers\HomeController::class, 'supplier'])->name('supplier');
	Route::get('/material', [App\Http\Controllers\HomeController::class, 'material'])->name('material');
	Route::post('api-quantity', [App\Http\Controllers\HomeController::class, 'api_quantity'])->name('api.quantity');

});
Route::middleware(['role:admin,principle'])->group(function () {
	Route::get('material-masuk', [App\Http\Controllers\Admin\MaterialController::class, 'material_masuk'])->name('admin-material-masuk');
	Route::post('material-masuk', [App\Http\Controllers\Admin\MaterialController::class, 'material_masuk'])->name('admin-add-material-masuk');
	Route::post('material-masuk-create', [App\Http\Controllers\Admin\MaterialController::class, 'material_masuk'])->name('admin-create-material-masuk')->middleware('scope:Saldo Masuk');
	
	Route::get('material-keluar', [App\Http\Controllers\Admin\MaterialController::class, 'material_keluar'])->name('admin-material-keluar');
	Route::post('material-keluar', [App\Http\Controllers\Admin\MaterialController::class, 'material_keluar'])->name('admin-add-material-keluar');
	Route::post('material-keluar-create', [App\Http\Controllers\Admin\MaterialController::class, 'material_keluar'])->name('admin-create-material-keluar')->middleware('scope:Saldo Keluar');

	Route::get('mutasi-saldo', [App\Http\Controllers\Admin\MaterialController::class, 'mutasi_saldo'])->name('admin-mutasi-saldo');
	Route::post('mutasi-saldo', [App\Http\Controllers\Admin\MaterialController::class, 'mutasi_saldo'])->name('admin-post-mutasi-saldo');

	Route::get('saldo-pabrik', [App\Http\Controllers\SaldoController::class, 'index'])->name('admin-saldo-pabrik');

});
Route::group(['prefix' => 'admin','middleware'=>'role:admin'], function () {
	Route::get('home', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin-home');
	//USER
	Route::get('data-admin', [App\Http\Controllers\Admin\UserController::class, 'user_admin'])->name('admin-data-user-admin');
	Route::get('data-admin/{id}/edit', [App\Http\Controllers\Admin\UserController::class, 'user_admin_edit'])->name('admin-edit-user-admin');
	Route::post('data-admin', [App\Http\Controllers\Admin\UserController::class, 'user_admin'])->name('admin-data-user-admin');
	Route::get('data-user-principle', [App\Http\Controllers\Admin\UserController::class, 'user_principle'])->name('admin-data-user-principle');
	Route::get('data-user-distributor', [App\Http\Controllers\Admin\UserController::class, 'user_distributor'])->name('admin-data-user-distributor');
	Route::get('data-user-agen', [App\Http\Controllers\Admin\UserController::class, 'user_agen'])->name('admin-data-user-agen');
	Route::get('data-user-end-user', [App\Http\Controllers\Admin\UserController::class, 'user_end_user'])->name('admin-data-user-end-user');
	Route::get('detail-user/{id}', [App\Http\Controllers\Admin\UserController::class, 'detail_user'])->name('admin-detail-user');
	Route::get('data-user/create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('admin-create-data-user');
	Route::post('data-user/create', [App\Http\Controllers\Admin\UserController::class, 'create'])->name('admin-create-data-user');
	//CATALOG
	Route::get('category-product', [App\Http\Controllers\Admin\CategoryProductController::class, 'index'])->name('admin-category-product');
	Route::post('category-product', [App\Http\Controllers\Admin\CategoryProductController::class, 'index'])->name('admin-category-product');
	Route::get('create-category-product', [App\Http\Controllers\Admin\CategoryProductController::class, 'create'])->name('admin-create-category-product');
	Route::post('create-category-product', [App\Http\Controllers\Admin\CategoryProductController::class, 'create'])->name('admin-create-category-product');
	//PRODUCT
	Route::get('product', [App\Http\Controllers\Admin\ProductController::class, 'index'])->name('admin-product');
	Route::post('product', [App\Http\Controllers\Admin\ProductController::class, 'index'])->name('admin-product');
	Route::get('product/create', [App\Http\Controllers\Admin\ProductController::class, 'create'])->name('admin-create-product');
	Route::post('product/create', [App\Http\Controllers\Admin\ProductController::class, 'create'])->name('admin-create-product');
	Route::get('product/{id}/edit', [App\Http\Controllers\Admin\ProductController::class, 'edit'])->name('admin-edit-product');
	Route::post('product/{id}/edit', [App\Http\Controllers\Admin\ProductController::class, 'edit'])->name('admin-edit-product');
	Route::post('product/delete', [App\Http\Controllers\Admin\ProductController::class, 'delete'])->name('admin-delete-product');
	//ORDER
	Route::get('orderan-distributor', [App\Http\Controllers\Admin\OrderController::class, 'orderan_distributor'])->name('admin-orderan-distributor');
	Route::post('orderan-distributor', [App\Http\Controllers\Admin\OrderController::class, 'orderan_distributor'])->name('admin-orderan-distributor');
	Route::get('orderan-agen', [App\Http\Controllers\Admin\OrderController::class, 'orderan_agen'])->name('admin-orderan-agen');
	Route::post('orderan-agen', [App\Http\Controllers\Admin\OrderController::class, 'orderan_agen'])->name('admin-orderan-agen');
	Route::get('orderan-enduser', [App\Http\Controllers\Admin\OrderController::class, 'orderan_enduser'])->name('admin-orderan-enduser');
	Route::post('orderan-enduser', [App\Http\Controllers\Admin\OrderController::class, 'orderan_enduser'])->name('admin-orderan-enduser');

	//REPORT
	Route::get('data-order', [App\Http\Controllers\Admin\OrderController::class, 'data_order'])->name('admin-data-order');
	Route::get('data-order/{id}/detail', [App\Http\Controllers\Admin\OrderController::class, 'detail_request_order'])->name('admin-detail-data-order');
	Route::post('data-order', [App\Http\Controllers\Admin\OrderController::class, 'data_order'])->name('admin-data-order');

	Route::post('user-role', [App\Http\Controllers\Admin\UserController::class, 'user_role'])->name('admin-user-role');

	//Data Pabrik
	Route::get('pabrik', [App\Http\Controllers\Admin\PabrikController::class, 'index'])->name('admin-pabrik');
	Route::get('pabrik/form', [App\Http\Controllers\Admin\PabrikController::class, 'form'])->name('admin-form-pabrik');
	Route::post('pabrik/form', [App\Http\Controllers\Admin\PabrikController::class, 'form'])->name('admin-create-pabrik');
	Route::get('pabrik/{id}/edit', [App\Http\Controllers\Admin\PabrikController::class, 'edit'])->name('admin-edit-pabrik');
	Route::post('pabrik/{id}/edit', [App\Http\Controllers\Admin\PabrikController::class, 'edit'])->name('admin-update-pabrik');
	
	
	Route::get('supplier', [App\Http\Controllers\Admin\SupplierController::class, 'index'])->name('admin-supplier');
	Route::get('supplier/form', [App\Http\Controllers\Admin\SupplierController::class, 'form'])->name('admin-form-supplier');
	Route::post('supplier/form', [App\Http\Controllers\Admin\SupplierController::class, 'form'])->name('admin-create-supplier');
	Route::get('supplier/{id}/edit', [App\Http\Controllers\Admin\SupplierController::class, 'edit'])->name('admin-edit-supplier');
	Route::post('supplier/{id}/edit', [App\Http\Controllers\Admin\SupplierController::class, 'edit'])->name('admin-update-supplier');

	Route::get('material', [App\Http\Controllers\Admin\MaterialController::class, 'index'])->name('admin-material');
	Route::get('material/form', [App\Http\Controllers\Admin\MaterialController::class, 'form'])->name('admin-form-material');
	Route::post('material/form', [App\Http\Controllers\Admin\MaterialController::class, 'form'])->name('admin-create-material');
	Route::get('material/{id}/edit', [App\Http\Controllers\Admin\MaterialController::class, 'edit'])->name('admin-edit-material');
	Route::post('material/{id}/edit', [App\Http\Controllers\Admin\MaterialController::class, 'edit'])->name('admin-update-material');
//AKSES PRINCIPLE
	Route::get('principle-akses', [App\Http\Controllers\Admin\SettingController::class, 'principle_akses'])->name('admin-principle-akses');
	Route::get('data-principle-akses', [App\Http\Controllers\Admin\SettingController::class, 'data_principle_akses'])->name('admin-data-principle-akses');
	Route::post('change-akses', [App\Http\Controllers\Admin\SettingController::class, 'change_akses'])->name('admin-change-akses');

	Route::get('data-order-material', [App\Http\Controllers\Admin\PabrikController::class, 'material'])->name('admin-data-order-material');

	Route::get('data-pre-order', [App\Http\Controllers\Admin\OrderController::class, 'total_order'])->name('admin-data-pre-order');
	Route::post('data-pre-order', [App\Http\Controllers\Admin\OrderController::class, 'total_order'])->name('admin-data-pre-order');

	Route::post('ganti-password', [App\Http\Controllers\Admin\UserController::class, 'ganti_password'])->name('admin.ganti-password');
	Route::get('delete-user/{id}', [App\Http\Controllers\Admin\UserController::class, 'delete_user'])->name('admin.delete-user');


});
//ROUTE PRINCIPLE
Route::group(['prefix' => 'Principle','middleware'=>'role:principle'], function () {
	Route::get('home', [App\Http\Controllers\Principle\HomeController::class, 'index'])->name('principle-home');
	Route::get('data-user-principle', [App\Http\Controllers\Principle\UserController::class, 'user_principle'])->name('principle-data-user-principle');
	Route::get('data-user-distributor', [App\Http\Controllers\Principle\UserController::class, 'user_distributor'])->name('principle-data-user-distributor');
	Route::get('data-user-agen', [App\Http\Controllers\Principle\UserController::class, 'user_agen'])->name('principle-data-user-agen');
	Route::get('data-user-end-user', [App\Http\Controllers\Principle\UserController::class, 'user_end_user'])->name('principle-data-user-end-user');
	Route::get('detail-user/{id}', [App\Http\Controllers\Principle\UserController::class, 'detail_user'])->name('principle-detail-user');

	Route::get('data-user/create', [App\Http\Controllers\Principle\UserController::class, 'create'])->name('principle-create-data-user');
	Route::post('data-user/create', [App\Http\Controllers\Principle\UserController::class, 'create'])->name('principle-create-data-user');
	//CATALOG
	Route::get('category-product', [App\Http\Controllers\Principle\CategoryProductController::class, 'index'])->name('principle-category-product');
	Route::post('category-product', [App\Http\Controllers\Principle\CategoryProductController::class, 'index'])->name('principle-category-product');
	Route::get('create-category-product', [App\Http\Controllers\Principle\CategoryProductController::class, 'create'])->name('principle-create-category-product');
	Route::post('create-category-product', [App\Http\Controllers\Principle\CategoryProductController::class, 'create'])->name('principle-create-category-product');
	//PRODUCT
	Route::get('product', [App\Http\Controllers\Principle\ProductController::class, 'index'])->name('principle-product');
	Route::post('product', [App\Http\Controllers\Principle\ProductController::class, 'index'])->name('principle-product');
	Route::get('product/create', [App\Http\Controllers\Principle\ProductController::class, 'create'])->name('principle-create-product');
	Route::post('product/create', [App\Http\Controllers\Principle\ProductController::class, 'create'])->name('principle-create-product');
	Route::get('product/{id}/edit', [App\Http\Controllers\Principle\ProductController::class, 'edit'])->name('principle-edit-product');
	Route::put('product/{id}/edit', [App\Http\Controllers\Principle\ProductController::class, 'edit'])->name('principle-edit-product');
	Route::delete('product/delete', [App\Http\Controllers\Principle\ProductController::class, 'delete'])->name('principle-delete-product');
//ORDER
	Route::get('request-order', [App\Http\Controllers\Principle\OrderController::class, 'index'])->name('principle-request-order');
	Route::post('request-order', [App\Http\Controllers\Principle\OrderController::class, 'index'])->name('principle-request-order');
	Route::get('request-order/{id}/detail', [App\Http\Controllers\Principle\OrderController::class, 'detail_request_order'])->name('principle-detail-request-order');
	Route::post('proses-pembayaran', [App\Http\Controllers\Principle\OrderController::class, 'proses_pembayaran'])->name('principle-proses-pembayaran');
	Route::get('status-order', [App\Http\Controllers\Principle\OrderController::class, 'status_order'])->name('principle-status-order');
	Route::get('status-order/{id}/detail', [App\Http\Controllers\Principle\OrderController::class, 'detail_request_order'])->name('principle-detail-status-order');
	Route::post('status-order', [App\Http\Controllers\Principle\OrderController::class, 'status_order'])->name('principle-status-order');
	Route::get('data-order', [App\Http\Controllers\Principle\OrderController::class, 'data_order'])->name('principle-data-order');
	Route::get('data-order/{id}/detail', [App\Http\Controllers\Principle\OrderController::class, 'detail_request_order'])->name('principle-detail-data-order');
	Route::post('data-order', [App\Http\Controllers\Principle\OrderController::class, 'data_order'])->name('principle-data-order');
//PABRIK
	Route::get('data-pabrik', [App\Http\Controllers\Principle\PabrikController::class, 'index'])->name('principle-data-pabrik');

	//ORDER
	Route::get('orderan-distributor', [App\Http\Controllers\Principle\OrderController::class, 'orderan_distributor'])->name('principle-orderan-distributor');
	Route::post('orderan-distributor', [App\Http\Controllers\Principle\OrderController::class, 'orderan_distributor'])->name('principle-orderan-distributor');
	Route::get('orderan-agen', [App\Http\Controllers\Principle\OrderController::class, 'orderan_agen'])->name('principle-orderan-agen');
	Route::post('orderan-agen', [App\Http\Controllers\Principle\OrderController::class, 'orderan_agen'])->name('principle-orderan-agen');
	Route::get('orderan-enduser', [App\Http\Controllers\Principle\OrderController::class, 'orderan_enduser'])->name('principle-orderan-enduser');
	Route::post('orderan-enduser', [App\Http\Controllers\Principle\OrderController::class, 'orderan_enduser'])->name('principle-orderan-enduser');

	Route::get('data-pre-order', [App\Http\Controllers\Principle\OrderController::class, 'total_order'])->name('principle-data-pre-order');

});
//ROUTE Distributor
Route::group(['prefix' => 'Distributor','middleware'=>'role:distributor'], function () {
	Route::get('home', [App\Http\Controllers\Distributor\HomeController::class, 'index'])->name('distributor-home');
	Route::get('data-agen', [App\Http\Controllers\Distributor\UserController::class, 'index'])->name('distributor-data-agen');
	Route::get('create-data-agen', [App\Http\Controllers\Distributor\UserController::class, 'create'])->name('distributor-create-data-agen');
	Route::post('create-data-agen', [App\Http\Controllers\Distributor\UserController::class, 'create'])->name('distributor-create-data-agen');
	Route::get('detail-agen/{id}', [App\Http\Controllers\Distributor\UserController::class, 'detail_agen'])->name('distributor-detail-agen');
	
	Route::get('data-end-user', [App\Http\Controllers\Distributor\UserController::class, 'end_user'])->name('distributor-data-end-user');
	//PRODUCT
	Route::get('product', [App\Http\Controllers\Distributor\ProductController::class, 'index'])->name('distributor-product');
	Route::get('product/{id}/detail', [App\Http\Controllers\Distributor\ProductController::class, 'detail'])->name('distributor-detail-product');
	Route::get('keranjang', [App\Http\Controllers\Distributor\ProductController::class, 'keranjang'])->name('distributor-keranjang');
	Route::post('product', [App\Http\Controllers\Distributor\ProductController::class, 'product'])->name('distributor-cart');
	Route::post('keranjang', [App\Http\Controllers\Distributor\ProductController::class, 'qty'])->name('distributor-keranjang');
	//MY ORDER
	Route::post('order-sekarang', [App\Http\Controllers\Distributor\OrderController::class, 'order_sekarang'])->name('distributor-order-sekarang');
	Route::get('status-transaksi', [App\Http\Controllers\Distributor\OrderController::class, 'status_transaksi'])->name('distributor-status-transaksi');
	Route::post('status-transaksi', [App\Http\Controllers\Distributor\OrderController::class, 'status_transaksi'])->name('distributor-status-transaksi');
	Route::get('status-transaksi/{id}/detail', [App\Http\Controllers\Distributor\OrderController::class, 'detail_request_order'])->name('distributor-detail-status-transaksi');
	Route::get('status-order', [App\Http\Controllers\Distributor\OrderController::class, 'status_order'])->name('status-order');
	Route::post('status-order', [App\Http\Controllers\Distributor\OrderController::class, 'status_order'])->name('distributor-status-order');
	Route::get('report-transaksi', [App\Http\Controllers\Distributor\OrderController::class, 'report_transaksi'])->name('distributor-report-transaksi');
	Route::post('report-transaksi', [App\Http\Controllers\Distributor\OrderController::class, 'report_transaksi'])->name('distributor-report-transaksi');
	Route::get('report-transaksi/{id}', [App\Http\Controllers\Distributor\OrderController::class, 'detail_request_transaksi'])->name('distributor-detail-report-transaksi');

	//REQ ORDER
	Route::get('request-order', [App\Http\Controllers\Distributor\OrderController::class, 'request_order'])->name('distributor-request-order');
	Route::post('request-order', [App\Http\Controllers\Distributor\OrderController::class, 'request_order'])->name('distributor-request-order');
	Route::get('detail-order/{id}/detail', [App\Http\Controllers\Distributor\OrderController::class, 'detail_request_order'])->name('distributor-detail-request-order');
	Route::post('proses-pembayaran', [App\Http\Controllers\Distributor\OrderController::class, 'proses_pembayaran'])->name('distributor-proses-pembayaran');
	Route::get('status-order/{id}/detail', [App\Http\Controllers\Distributor\OrderController::class, 'detail_request_order'])->name('distributor-detail-status-order');
	Route::get('report-order', [App\Http\Controllers\Distributor\OrderController::class, 'report_order'])->name('distributor-report-order');
	Route::post('report-order', [App\Http\Controllers\Distributor\OrderController::class, 'report_order'])->name('distributor-report-order');
	Route::get('report-order/{id}', [App\Http\Controllers\Distributor\OrderController::class, 'detail_request_order'])->name('distributor-detail-report-order');
	// REPORT
	Route::get('orderan-agen', [App\Http\Controllers\Distributor\OrderController::class, 'orderan_agen'])->name('distributor-orderan-agen');
	Route::post('orderan-agen', [App\Http\Controllers\Distributor\OrderController::class, 'orderan_agen'])->name('distributor-orderan-agen');
	Route::get('orderan-enduser', [App\Http\Controllers\Distributor\OrderController::class, 'orderan_enduser'])->name('distributor-orderan-enduser');
	Route::post('orderan-enduser', [App\Http\Controllers\Distributor\OrderController::class, 'orderan_enduser'])->name('distributor-orderan-enduser');


});
//AGEN
Route::group(['prefix' => 'Agen','middleware'=>'role:agen'], function () {
	Route::get('home', [App\Http\Controllers\Agen\HomeController::class, 'index'])->name('agen-home');
	Route::get('data-end-user', [App\Http\Controllers\Agen\UserController::class, 'index'])->name('agen-data-end-user');
	Route::get('create-data-end-user', [App\Http\Controllers\Agen\UserController::class, 'create'])->name('agen-create-data-end-user');
	Route::post('create-data-end-user', [App\Http\Controllers\Agen\UserController::class, 'create'])->name('agen-create-data-end-user');
	Route::get('detail-enduser/{id}', [App\Http\Controllers\Agen\UserController::class, 'detail_agen'])->name('agen-detail-end-user');

	//TRENSAKSI
	Route::get('product', [App\Http\Controllers\Agen\ProductController::class, 'index'])->name('agen-product');
	Route::get('product/{id}/detail', [App\Http\Controllers\Agen\ProductController::class, 'detail'])->name('agen-detail-product');
	Route::get('keranjang', [App\Http\Controllers\Agen\ProductController::class, 'keranjang'])->name('agen-keranjang');
	Route::post('product', [App\Http\Controllers\Agen\ProductController::class, 'product'])->name('agen-cart');
	Route::post('keranjang', [App\Http\Controllers\Agen\ProductController::class, 'qty'])->name('agen-keranjang');
	Route::post('order-sekarang', [App\Http\Controllers\Agen\OrderController::class, 'order_sekarang'])->name('agen-order-sekarang');
	Route::get('status-transaksi', [App\Http\Controllers\Agen\OrderController::class, 'status_transaksi'])->name('agen-status-transaksi');
	Route::post('status-transaksi', [App\Http\Controllers\Agen\OrderController::class, 'status_transaksi'])->name('agen-status-transaksi');
	Route::get('status-transaksi/{id}/detail', [App\Http\Controllers\Agen\OrderController::class, 'detail_status_transaksi'])->name('agen-detail-status-transaksi');
	Route::get('status-order', [App\Http\Controllers\Agen\OrderController::class, 'status_order'])->name('agen-status-order');
	Route::post('status-order', [App\Http\Controllers\Agen\OrderController::class, 'status_order'])->name('agen-status-order');
	Route::get('report-transaksi', [App\Http\Controllers\Agen\OrderController::class, 'report_transaksi'])->name('agen-report-transaksi');
	Route::post('report-transaksi', [App\Http\Controllers\Agen\OrderController::class, 'report_transaksi'])->name('agen-report-transaksi');
	Route::get('report-transaksi/{id}', [App\Http\Controllers\Agen\OrderController::class, 'detail_item'])->name('agen-detail-report-transaksi');

	//REQ ORDER
	Route::get('request-order', [App\Http\Controllers\Agen\OrderController::class, 'request_order'])->name('agen-request-order');
	Route::get('detail-order/{id}/detail', [App\Http\Controllers\Agen\OrderController::class, 'detail_request_order'])->name('agen-detail-request-order');
	Route::post('proses-pembayaran', [App\Http\Controllers\Agen\OrderController::class, 'proses_pembayaran'])->name('agen-proses-pembayaran');
	Route::get('status-order/{id}/detail', [App\Http\Controllers\Agen\OrderController::class, 'detail_request_order'])->name('agen-detail-status-order');
	Route::get('report-order', [App\Http\Controllers\Agen\OrderController::class, 'report_order'])->name('agen-report-order');
	Route::post('report-order', [App\Http\Controllers\Agen\OrderController::class, 'report_order'])->name('agen-report-order');
	Route::get('report-order/{id}', [App\Http\Controllers\Agen\OrderController::class, 'detail_item'])->name('agen-detail-report-order');

	Route::get('orderan-enduser', [App\Http\Controllers\Agen\OrderController::class, 'orderan_enduser'])->name('agen-orderan-enduser');
	Route::post('orderan-enduser', [App\Http\Controllers\Agen\OrderController::class, 'orderan_enduser'])->name('agen-orderan-enduser');


});
//END USER
Route::group(['prefix' => 'EndUser','middleware'=>'role:end user'], function () {
	Route::get('home', [App\Http\Controllers\EndUser\HomeController::class, 'index'])->name('end-user-home');
	Route::get('product', [App\Http\Controllers\EndUser\ProductController::class, 'index'])->name('end-user-product');
	Route::get('product/{id}/detail', [App\Http\Controllers\EndUser\ProductController::class, 'detail'])->name('end-user-detail-product');
	Route::get('keranjang', [App\Http\Controllers\EndUser\ProductController::class, 'keranjang'])->name('end-user-keranjang');
	Route::post('product', [App\Http\Controllers\EndUser\ProductController::class, 'product'])->name('end-user-cart');
	Route::post('keranjang', [App\Http\Controllers\EndUser\ProductController::class, 'qty'])->name('end-user-keranjang');
	Route::post('order-sekarang', [App\Http\Controllers\EndUser\ProductController::class, 'order_sekarang'])->name('end-user-order-sekarang');
	Route::get('status-transaksi', [App\Http\Controllers\EndUser\ProductController::class, 'status_transaksi'])->name('end-user-status-transaksi');
	Route::post('status-transaksi', [App\Http\Controllers\EndUser\ProductController::class, 'status_transaksi'])->name('end-user-status-transaksi');
	Route::get('status-transaksi/{id}/detail', [App\Http\Controllers\EndUser\ProductController::class, 'detail_request_order'])->name('end-user-detail-status-transaksi');
	Route::get('status-order', [App\Http\Controllers\EndUser\ProductController::class, 'status_order'])->name('end-user-status-order');
	Route::post('status-order', [App\Http\Controllers\EndUser\ProductController::class, 'status_order'])->name('end-user-status-order');
	Route::get('report-transaksi', [App\Http\Controllers\EndUser\ProductController::class, 'report_transaksi'])->name('end-user-report-transaksi');
	Route::post('report-transaksi', [App\Http\Controllers\EndUser\ProductController::class, 'report_transaksi'])->name('end-user-report-transaksi');
	Route::get('report-transaksi/{id}', [App\Http\Controllers\EndUser\ProductController::class, 'detail_item'])->name('end-user-detail-report-transaksi');
//API

});

