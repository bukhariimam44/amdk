@extends('layouts.app')
@push('css')
<style>
    div.btn-toolbar{
        display: none !important;
    }
</style>
<meta name="_token" id="token" value="{{csrf_token()}}">
@endpush
@section('content')
<div class="page-content"  id="grafik">
                <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Dashboard</h4>
                    </div>
                    <div class="float-end d-sm-block">
                        <?php $yearNow =  date('Y');
                        ?>
                        <form action="{{route('home')}}" method="get">
                        @csrf
                            <select name="year" onchange="this.form.submit()">
                            @for($i=2021; $i <= $yearNow; $i++)
                                <option value="{{$i}}" @if($year == $i) selected @endif>{{$i}}</option>
                            @endfor
                            </select>
                        </form>
                        <!-- <select v-model="selectYear" id="" @change="load()">
                        <option v-for="year in years" :value="year">@{{ year }}</option>
                        </select> -->
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title mb-4">Sales Transaksi Total</h4>
                            <div id="line_chart_datalabel" :data-grafik="datas" class="apex-charts" dir="ltr"></div>
                        <br>
                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Label</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>Mei</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ags</th>
                                <th>Sept</th>
                                <th>Okt</th>
                                <th>Nov</th>
                                <th>Des</th>
                            </thead>
                            <tbody>
                                <tr v-for="data in JSON.parse(datas)" :key=data.id>
                                    <td>@{{data.name}}</td>
                                    <td v-for="dt in data.data">@{{toRupiah(dt)}}</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        
                        </div>
                    </div>
                    <!--end card-->
                </div>
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="header-title mb-4">Sales Transaksi/Product</h4>
                            <div id="line_chart_datalabel2" :data-category="categories" :data-maksimal="max" class="apex-charts" dir="ltr"></div>
                        </div>
                        <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <th>Product</th>
                                <th>Jan</th>
                                <th>Feb</th>
                                <th>Mar</th>
                                <th>Apr</th>
                                <th>Mei</th>
                                <th>Jun</th>
                                <th>Jul</th>
                                <th>Ags</th>
                                <th>Sept</th>
                                <th>Okt</th>
                                <th>Nov</th>
                                <th>Des</th>
                            </thead>
                            <tbody>
                                <tr v-for="categori in JSON.parse(categories)" :key=categori.id>
                                    <td>@{{categori.name}}</td>
                                    <td v-for="ct in categori.data">@{{toRupiah(ct)}}</td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>
                    
                </div>
            </div>
            <!-- end row -->

        </div>


    </div>
    <!-- container-fluid -->
</div>
@endsection
@push('js')
<script>
    function myFunction(){
        this.form.submit();
    }
</script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.3"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
    integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    var grafik = new Vue({
        el: "#grafik",
        data: {
            datas:[],
            categories:[],
            max:[],
            selectYear:"{{$year}}"
        },
        computed : {
            years () {
            const year = new Date().getFullYear()
            return Array.from({length: year - 2020}, (value, index) => 2021 + index)
            }
        },
        mounted(){
            this.load()
        },
        methods: {
            toRupiah(nominal) {
                var nominal = parseFloat(nominal).toFixed(0);
                const format = nominal.toString().split('').reverse().join('');
                const convert = format.match(/\d{1,3}/g);
                const rupiah = convert.join('.').split('').reverse().join('')
                return rupiah;
            },
            load(){
                const url = "<?php echo route('grafik');?>";
                this.$http.post(url, {year:this.selectYear}).then(function(response) {
                    this.datas = JSON.stringify(response.data.bulanan);
                    this.categories = JSON.stringify(response.data.product);
                    this.max = JSON.stringify(response.data.max);
                    this.loadJs1()
                    this.loadJs3()
                });
            },
            loadJs1() {
                let recaptchaScript = document.createElement('script')
                recaptchaScript.setAttribute("src", "{{asset('assets/js/pages/batang.js')}}")
                document.body.appendChild(recaptchaScript)
                this.reload_js("{{asset('assets/js/pages/batang.js')}}")
            },
            loadJs2() {
                let recaptchaScript = document.createElement('script')
                recaptchaScript.setAttribute("src", "{{asset('assets/js/pages/transaksi.js')}}")
                document.body.appendChild(recaptchaScript)
                this.reload_js("{{asset('assets/js/pages/transaksi.js')}}")
            },
            loadJs3() {
                let recaptchaScript = document.createElement('script')
                recaptchaScript.setAttribute("src", "{{asset('assets/libs/apexcharts/apexcharts.min.js')}}")
                document.body.appendChild(recaptchaScript)
                this.reload_js("{{asset('assets/libs/apexcharts/apexcharts.min.js')}}")
            },
            reload_js(src) {
                $('script[src="' + src + '"]').remove();
                $('<script>').attr('src', src).appendTo('head');
            },
        }
    })
</script>
<!-- <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script> -->
<!-- demo js-->
<!-- <script src="{{asset('assets/js/pages/batang.js')}}"></script> -->
@endpush