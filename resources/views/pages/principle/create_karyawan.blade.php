@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create Karyawan</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create Karyawan</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <form action="{{route('admin-create-karyawan')}}" method="post" enctype="multipart/form-data" id="create">
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                            Name *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('name') parsley-error @enderror"
                                                type="text" name="name" value="{{old('name')}}"
                                                placeholder="Enter Name" id="example-text-input">
                                            @error('name')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Username *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('username') parsley-error @enderror"
                                                type="text" name="username" value="{{old('username')}}"
                                                placeholder="Enter Username" id="example-text-input">
                                            @error('username')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Email *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('email') parsley-error @enderror"
                                                type="email" name="email" value="{{old('email')}}"
                                                placeholder="Enter Email" id="example-text-input">
                                            @error('email')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Phone *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('phone') parsley-error @enderror"
                                                type="text" name="phone" value="{{old('phone')}}"
                                                placeholder="Enter Phone" id="example-text-input">
                                            @error('phone')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Password *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('password') parsley-error @enderror"
                                                type="password" name="password" value="{{old('password')}}"
                                                placeholder="Enter Password" id="example-text-input">
                                            @error('password')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Account Type *</label>
                                        <div class="col-sm-10">
                                            <select name="user_role" id="" class="form-control select2-search-disable @error('user_role') parsley-error @enderror">
                                                <option value="">Select Account Type</option>
                                                @foreach($user_roles as $role)
                                                <option value="{{$role->id}}" @if(old('user_role') == $role->id) selected @endif>{{$role->role}}</option>
                                                @endforeach
                                            </select>
                                            @error('user_role')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-email-input" class="col-sm-2 col-form-label">Photo *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('photo') parsley-error @enderror"
                                                type="file" accept="image/*" name="photo">
                                            @error('photo')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Store *</label>
                                        <div class="col-sm-10">
                                            <select name="store_id" id="" class="form-control select2 @error('store_id') parsley-error @enderror">
                                                <option value="">Select Store</option>
                                                @foreach($stores as $store)
                                                <optgroup label="{{$store->store_name}}">
                                                    <option value="{{$store->id}}" {{old('store_id') == $store->id ? 'selected' : ''}}>{{$store->store_name}}</option>
                                                    @foreach($store->parentStore as $str)
                                                    <option value="{{$str->id}}" {{old('store_id') == $str->id ? 'selected' : ''}}>{{$str->store_name}}</option>
                                                    @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                            @error('store_id')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="create">
                                <div class="mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>
@endpush