@extends('layouts.app')
@push('css')
<style>
    button.mt-6{
        margin-top: 6px !important;
    }
    .table-primarys{
        background-color:#ebecf1 !important;
        box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        border-color:#ced4da;
    }
</style>
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a href="{{route('admin-create-store')}}" class="btn btn-success">Create</a>
                        </div>
                        <h4>Store</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Store</li>
                        </ol>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid" id="store">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin-store')}}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Store Name</label>
                                            <input type="text" name="store_name" value="{{$store_name}}" class="table-primary form-control" placeholder="Enter Store Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Address</label>
                                            <input type="text" name="address" value="{{$address}}" class="table-primary form-control" placeholder="Enter Address">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Store Name</th>
                                            <th>Address</th>
                                            <th>Karyawan</th>
                                            <th>Type</th>
                                            <th>Logo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($stores as $key => $store)
                                        <tr>
                                            <th scope="row">{{$key+1}}.</th>
                                            <td>{{$store->store_name}}</td>
                                            <td>{{$store->address}}</td>
                                            <td>{{$store->karyawan}}</td>
                                            <td><span class="badge @if($store->parent_id == 0) bg-success @else bg-primary @endif">{{$store->type}}</span></td>
                                            <td><img src="{{$store->url}}" alt="" width="50px"></td>
                                            <td>
                                                <a href="{{route('admin-qrcode-table',$store->id)}}" class="btn btn-outline-secondary btn-sm edit" title="QR Code Table">
                                                    <i class="mdi mdi-barcode-scan"></i>
                                                </a>
                                                <a href="{{route('admin-edit-store',$store->id)}}" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a class="btn btn-outline-secondary btn-sm delete" @click="hapus({{$store}})" title="Delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($stores) < 1)
                                        <tr>
                                            <th scope="row" colspan="7" class="text-center">Kosong</th>
                                        </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var form = new Vue({
    el:'#store',
    data:{},
    methods: {
        loading(text){
            Swal.fire({
                text: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
            );
        },
        hapus(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert " + id.store_name,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    this.delete(id.id)
                }
            })
        },
        async delete(id){
            this.loading('Waiting...')
            const deletestore = "<?php echo route('admin-delete-store');?>";
            const request = {store_id:id};
            await axios.delete(deletestore,{data:request}).then((response) =>{
                Swal.fire(
                    response.data.title,
                    response.data.message,
                    response.data.icon
                )
                console.log(response.data);
                if (response.data.code === 200) {
                    setTimeout(() => {
                        window.location.href = "{{route('admin-store',['store_name'=>$store_name,'address'=>$address])}}";
                    }, 1000);
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        }
    },
});
</script>
@endpush