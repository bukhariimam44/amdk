@extends('layouts.app')
@push('css')
<!-- <link rel="stylesheet" href="{{asset('assets/libs/twitter-bootstrap-wizard/prettify.css')}}"> -->
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create Store</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create Store</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper" id="form-store">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <form action="{{route('admin-create-store')}}" method="post" enctype="multipart/form-data" id="create">
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Type Store *</label>
                                        <div class="col-sm-10">
                                            <label class="block"><input type="radio" name="type" class="type @error('type') parsley-error @enderror" value="Main Store" {{old('type') == 'Main Store' ? 'checked' : ''}}>&nbsp; Main Store</label>
                                            @if(count($main_stores) > 0)<label class="block">&nbsp; &nbsp; &nbsp; <input type="radio" name="type" class="type @error('type') parsley-error @enderror" value="Outlet" {{old('type') == 'Outlet' ? 'checked' : ''}}>&nbsp; Outlet</label>@endif
                                            @error('type')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3 select_2">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Select Main Store *</label>
                                        <div class="col-sm-10">
                                            <select name="store_id" class="form-control select2 @error('store_id') parsley-error @enderror">
                                                <option>Select Main Store</option>
                                                @foreach($main_stores as $store)
                                                <option value="{{$store->id}}" {{old('store_id') == $store->id ? 'selected' : ''}}>{{$store->store_name}} | {{$store->address}}</option>
                                                @endforeach
                                            </select>
                                            @error('store_id')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Store
                                            Name *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('store_name') parsley-error @enderror"
                                                type="text" name="store_name"
                                                placeholder="Enter Store Name" value="{{old('store_name')}}" id="example-text-input">
                                            @error('store_name')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-search-input"
                                            class="col-sm-2 col-form-label">Address *</label>
                                        <div class="col-sm-10">
                                            <textarea name="address"
                                                class="form-control @error('address') parsley-error @enderror" cols="30"
                                                rows="4" placeholder="Enter Address"></textarea>
                                            @error('address')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-email-input" class="col-sm-2 col-form-label">Logo *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('logo') parsley-error @enderror"
                                                type="file" accept="image/png" name="logo" value="{{old('logo')}}">
                                            @error('logo')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="create">
                                <div class="mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<!-- form wizard init -->
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>
<script>
$(document).ready(function() {
    $('.select_2').hide();
    $('.type').change(function(){
        if(this.value === 'Outlet')
        $('.select_2').show();
        else
        $('.select_2').hide();
    });
});
</script>
@endpush