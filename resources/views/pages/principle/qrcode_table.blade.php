@extends('layouts.app')
@push('css')
<style>
    .pinggir {
        margin-bottom: 20px;
    }

    select.form-control {
        min-width: 300px !important;
    }
    .ridge {
        border-style: ridge;
    }
    .posisi{
        position: absolute;
        margin-top:-25px;
        float: right!important;
        right: 25px;
    }
    
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>QR Code Table</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item">Store</li>
                            <li class="breadcrumb-item active">QR Code {{$datas->store_name}}</li>
                        </ol>
                    </div>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper" id="qrcode">
            <div class="row">
                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-body">
                            <button class="open btn btn-success">Create QR Code</button>
                            <div class="form">
                                <form action="{{route('admin-qrcode-table',$datas->id)}}" method="post"
                                    enctype="multipart/form-data" id="create">
                                    <input type="hidden" name="action" value="create">
                                    @csrf
                                    <div class="row">

                                        <div class="row mb-3">
                                            <label for="example-text-input" class="col-sm-2 col-form-label">Table
                                                Name *</label>
                                            <div class="col-sm-10">
                                                <input class="form-control @error('table_name') parsley-error @enderror"
                                                    type="text" name="table_name" placeholder="Enter Table Name"
                                                    value="{{old('table_name')}}" id="example-text-input">
                                                @error('table_name')
                                                <ul class="parsley-errors-list filled" role="alert">
                                                    <li class="parsley-required">{{ $message }}</li>
                                                </ul>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="create">
                                    <div class="mb-0">
                                        <div>
                                            <button type="submit"
                                                class="btn btn-primary waves-effect waves-light me-1 float-right">
                                                Create QR Code
                                            </button>
                                            <button type="reset" class="btn btn-secondary waves-effect">
                                                Reset
                                            </button>
                                            <button type="button" class="btn btn-danger waves-effect close">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <hr>
                            <div class="row">
                                @foreach($datas->QRTable as $data)
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card">
                                        <div class="card-body ridge">
                                            
                                            <div class="product-img">
                                                    <img src="{{$data->UrlQrcode}}" alt=""
                                                    class="img-fluid mx-auto d-block">
                                            </div>
                                            
                                            <div class="text-left">
                                                <h4 class="mt-3 mb-0 mr-0">{{$data->table_name}} </h4>

                                            </div>
                                            <div class="posisi float-sm-end">
                                                <a href="{{route('admin-download-qrcode-table',$data->id)}}" class="btn btn-primary btn-sm" title="Download">
                                                    <i class="fas fa-download"></i>
                                                </a>
                                                <a @click="editTitle({{$data}})" class="btn btn-warning btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a @click="hapus({{$data}})" class="btn btn-danger btn-sm delete" title="Delete">
                                                    <i class="fas fa-trash-alt"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script>
    $(document).ready(function () {
        $('.form').hide();
        $('.open').click(function () {
            $('.form').show();
            $('.open').hide();
        });
        $('.close').click(function () {
            $('.form').hide();
            $('.open').show();
        });
    });
</script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var form = new Vue({
    el:'#qrcode',
    data:{},
    methods: {
        loading(text){
            Swal.fire({
                text: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                didOpen: ()=>{
                    Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
            );
        },
        hapus(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert " + id.table_name,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    this.delete(id.id)
                }
            })
        },
        async delete(id){
            this.loading('Waiting...')
            const url = "<?php echo route('admin-delete-qrcode-table');?>";
            const request = {table_id:id};
            await axios.delete(url,{data:request}).then((response) =>{
                Swal.fire(
                    response.data.title,
                    response.data.message,
                    response.data.icon
                )
                console.log(response.data);
                if (response.data.code === 200) {
                    setTimeout(() => {
                        window.location.href = "{{route('admin-qrcode-table',$datas->id)}}";
                    }, 1000);
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        },
        async editTitle(item){
            const { value: table_name } = await Swal.fire({
                title: 'Update Table Name',
                input: 'text',
                inputPlaceholder: 'Enter Table Name',
                confirmButtonText: 'Update',
                showCancelButton: true,
                inputValidator: (value) => {
                    if (!value) {
                    return 'Table Name is required!'
                    }
                }
            })

            if (table_name) {
                this.update(item.id,table_name)
                // Swal.fire(`Entered email: ${table_name}`)
            }
        },
        async update(id,table_name){
            this.loading('Waiting...')
            const url = "<?php echo route('admin-update-qrcode-table');?>";
            const request = {table_id:id, table_name:table_name};
            await axios.put(url,request).then((response) =>{
                Swal.fire(
                    response.data.title,
                    response.data.message,
                    response.data.icon
                )
                console.log(response.data);
                if (response.data.code === 200) {
                    setTimeout(() => {
                        window.location.href = "{{route('admin-qrcode-table',$datas->id)}}";
                    }, 1000);
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        },
    },
});
</script>
@endpush