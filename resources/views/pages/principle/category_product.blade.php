@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <!-- <div class="float-end d-sm-block">
                            <a href="{{route('principle-create-category-product')}}" class="btn btn-success">Create</a>
                        </div> -->
                        <h4>Category Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Category Product</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid" id="category">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Category</th>
                                            <!-- <th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $key => $category)
                                        <tr>
                                            <td>{{$key+1}}.</td>
                                            <td>{{$category->category}}</td>
                                            <!-- <td>
                                                <a @click="edit({{$category}})" href="#" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a @click="confirmHapus({{$category->id}})" class="btn btn-outline-secondary btn-sm delete" title="Delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td> -->
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var category = new Vue({
    el:'#category',
    data:{},
    methods: {
        confirmHapus(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    this.hapus(id)
                }
            })
        },
        async hapus(id){
            const url = "<?php echo route('principle-create-category-product');?>";
            const request = {category_id:id, action: 'hapus'};
            await axios.post(url,request).then((response) =>{
                Swal.fire({
                    icon: response.data.icon,
                    title: response.data.title,
                    text: response.data.message,
                })
                if (response.data.code === 200) {
                    location.reload()
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        },
        async edit(item){
            const {
                    value: text
                } = await Swal.fire({
                    input: 'textarea',
                    inputValue: item.category,
                    inputLabel: 'Category Product',
                    inputPlaceholder: 'Category Product',
                    inputAttributes: {
                        autocapitalize: 'on'
                    },
                    showCancelButton: true
                })

                if (text) {
                    this.updateCategory(item,text)
                }
        },
        async updateCategory(item,category){
            const url = "<?php echo route('principle-create-category-product');?>";
            const request = {category_id:item.id, category:category,action: 'update'};
            await axios.post(url,request).then((response) =>{
                Swal.fire({
                    icon: response.data.icon,
                    title: response.data.title,
                    text: response.data.message,
                })
                if (response.data.code === 200) {
                    location.reload()
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
        }            
    },
});
</script>
@endpush