@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/admin-resources/rwd-table/rwd-table.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    div.btn-toolbar{
        display: none !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        @if(count($datas) > 0)
                        <div class="float-end d-lg-block">
                            <form action="{{route('principle-orderan-agen')}}" method="post" id="transaksi">
                            @csrf
                            <input type="hidden" name="from" value="{{$from}}"/>
                            <input type="hidden" name="to" value="{{$to}}"/>
                            <input type="hidden" name="action" value="transaksi"/>
                            <input type="hidden" name="status" value="{{$status}}"/>
                            
                            </form>
                            <form action="{{route('principle-orderan-agen')}}" method="post" id="item">
                            @csrf
                            <input type="hidden" name="from" value="{{$from}}"/>
                            <input type="hidden" name="to" value="{{$to}}"/>
                            <input type="hidden" name="action" value="item"/>
                            <input type="hidden" name="status" value="{{$status}}"/>
                            </form>
                            <button type="button" class="btn btn-info" onclick="event.preventDefault(); document.getElementById('transaksi').submit();"><i class="mdi mdi-microsoft-excel"></i>   Export Transaksi</button> <button type="button"  onclick="event.preventDefault();
                                                    document.getElementById('item').submit();" class="btn btn-success"><i class="mdi mdi-microsoft-excel"></i>   Export Item</button>
                        </div>
                        @endif
                        <h4>Orderan Agen</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Orderan Agen</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body" id="product">
                        <form action="{{route('principle-orderan-agen')}}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Tanggal Dari</label>
                                            <input type="date" name="from" value="{{$from}}" class="table-primary form-control" placeholder="Tanggal Dari">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Tanggal Sampai</label>
                                            <input type="date" name="to" value="{{$to}}" class="table-primary form-control" placeholder="Tanggal Sampai">
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="cari">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Status</label>
                                            <select name="status" id="" class="form-control select2-search-disable" required>
                                                <option value="">Pilih Status</option>
                                                @foreach($statuses as $st)
                                                <option value="{{$st->id}}" @if($status == $st->id) selected @endif>{{$st->status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped table-primarys">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>No. Order</th>
                                                <th>Tanggal Order</th>
                                                <th>Item</th>
                                                <th>Qty</th>
                                                <th>Harga</th>
                                                <th>Status</th>
                                                <th>User</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $item = 0;
                                            $qty = 0;
                                            $harga = 0;
                                            @endphp
                                            @foreach($datas as $key => $data)
                                            @php
                                            $item+= count($data['item']);
                                            $qty+= $data->item->sum('qty');
                                            $harga+= $data->harga_total;
                                            @endphp
                                            <tr>
                                                <th scope="row">{{$key+1}}.</th>
                                                <td>{{$data->no_order}}</td>
                                                <td>{{date('d M Y H:i', strtotime($data->transaction_date))}}</td>
                                                <td>{{count($data['item'])}}</td>
                                                <td>{{$data->item->sum('qty')}}</td>
                                                <td>{{number_format($data->harga_total,0,',','.')}}</td>
                                                <td> <div class="badge @if($data->order_status_id == 2) badge-soft-primary @else badge-soft-danger @endif font-size-14">{{$data->status->status}}</div></td>
                                                <td>{{$data->user->nama_depan}}</td>
                                                <td>
                                                    <a href="{{route('principle-detail-data-order',encrypt($data->id))}}" class="btn btn-warning btn-sm edit" title="Edit">
                                                        <i class="fas fa-search"></i> Detail Item
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @if(count($datas) == 0)
                                            <tr>
                                                <td colspan="9" class="text-center">No Data</td>
                                            </tr>
                                            @else
                                            <thead>
                                            <tr>
                                                <th colspan="3" class="text-left">TOTAL</th>
                                                <th>{{$item}}</th>
                                                <th>{{$qty}}</th>
                                                <th>{{number_format($harga,0,',','.')}}</th>
                                                <th colspan="3"></th>
                                            </tr>
                                            </thead>
                                            
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<!-- Responsive Table js -->
<script src="{{asset('assets/libs/admin-resources/rwd-table/rwd-table.min.js')}}"></script>

<!-- Init js -->
<script src="{{asset('assets/js/pages/table-responsive.init.js')}}"></script>
@endpush