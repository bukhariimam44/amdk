@extends('layouts.app')
@push('css')
<link rel="stylesheet" href="{{asset('assets/libs/twitter-bootstrap-wizard/prettify.css')}}">
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .rupiah{
        text-align:left !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create Product</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper" id="form-store">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <form action="{{route('admin-edit-product',$product->id)}}" method="put" enctype="multipart/form-data" id="create">
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Product
                                            Name *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('product_name') parsley-error @enderror"
                                                type="text" name="product_name"
                                                placeholder="Enter Product Name" value="{{$product->product_name}}" id="example-text-input">
                                            @error('product_name')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Base Price *</label>
                                        <div class="col-sm-10">
                                            <input id="input-currency" name="base_price" value="{{$product->base_price}}" class="form-control input-mask text-left @error('base_price') parsley-error @enderror rupiah" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp ', 'placeholder': '0'">
                                            @error('base_price')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Selling Price *</label>
                                        <div class="col-sm-10">
                                        <input id="input-currency" name="selling_price" value="{{$product->selling_price}}" class="form-control input-mask text-left @error('selling_price') parsley-error @enderror rupiah" data-inputmask="'alias': 'numeric', 'groupSeparator': '.', 'digits': 0, 'digitsOptional': false, 'prefix': 'Rp ', 'placeholder': '0'">
                                            @error('selling_price')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Stok *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('stok') parsley-error @enderror"
                                                type="text" name="stok"
                                                placeholder="Enter Stok" value="{{$product->stok}}" id="example-text-input">
                                            @error('stok')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-search-input"
                                            class="col-sm-2 col-form-label">Description *</label>
                                        <div class="col-sm-10">
                                            <textarea name="description"
                                                class="form-control @error('description') parsley-error @enderror" cols="30"
                                                rows="4" placeholder="Enter Description">{{$product->description}}</textarea>
                                            @error('description')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-email-input" class="col-sm-2 col-form-label">Product Image</label>
                                        <div class="col-sm-10">
                                        <img src="{{$product->url}}" alt="" width="150px"><br>
                                            <input class="form-control @error('product_image') parsley-error @enderror"
                                                type="file" accept="image/*" name="product_image" value="{{old('product_image')}}">
                                            @error('product_image')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Update to Stores *</label>
                                        <div class="col-sm-10">
                                            <select name="store_id[]" class="select2 form-control select2-multiple @error('store_id') parsley-error @enderror" multiple="multiple" data-placeholder="Select Store ...">
                                                @foreach($main_stores as $key => $store)
                                                <option value="{{$store->id}}" {{(collect($product->AllStore[$key])->contains($store->id)) ? 'selected' : ''}}>{{$store->store_name}}</option>
                                                @endforeach
                                            </select>
                                            @error('store_id')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="update">
                                <div class="mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                            Update
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<!-- form wizard init -->
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>

<script src="{{asset('assets/libs/inputmask/jquery.inputmask.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-mask.init.js')}}"></script>
@endpush