@extends('layouts.app')
@push('css')

<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content" id="user">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            @if($role->id == 3)<a
                                href="{{route('principle-create-data-user',['role'=>$role->id])}}"
                                class="btn btn-success">Create</a>@endif
                        </div>
                        <h4>{{$role->role}}</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$role->role}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{$url}}" method="get">
                                @csrf
                                <input type="hidden" name="role" value="{{$role->id}}">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Nama</label>
                                            <input type="text" name="nama" value="{{$nama}}"
                                                class="table-primary form-control" placeholder="Masukan Nama">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Phone</label>
                                            <input type="number" name="phone" value="{{$phone}}"
                                                class="table-primary form-control" placeholder="Masukan Phone">
                                        </div>
                                    </div>
                                    @if($role->id == 4 || $role->id == 5)
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">@if($role->id ==
                                                5) Agen @elseif($role->id == 4) Distributor @endif</label>
                                            <select name="parent_id" id="" class="form-control select2">
                                                <option value="0">Semua</option>
                                                @foreach($users as $user)
                                                <option value="{{$user->id}}" @if($parent==$user->id) selected
                                                    @endif>{{$user->nama_depan}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>

                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>

                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>RT/RW</th>
                                            <th>Membership</th>
                                            @if($role->id == 5)
                                            <th>Agen</th>
                                            <th>Distributor</th>
                                            @elseif($role->id == 4)
                                            <th>Distributor</th>
                                            @endif
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <th scope="row">{{$key+1}}.</th>
                                            <td>{{$data->nama_depan}}</td>

                                            <td>{{$data->email}}</td>
                                            <td>{{$data->phone}}</td>
                                            <td>{{$data->rt}}/{{$data->rw}}</td>
                                            <td>{{$data->membership->membership}}</td>
                                            @if($role->id == 5)
                                            <td><a href="{{route('principle-detail-user',$data->parentId->id)}}">{{strtoupper($data->parentId->nama_depan)}}
                                                   </a> </td>
                                            <td> <a href="{{route('principle-detail-user',$data->parentId->parentId->id)}}">{{strtoupper($data->parentId->parentId->nama_depan)}}
                                                    </a></td>
                                            @elseif($role->id == 4)
                                            <td><a href="{{route('principle-detail-user',$data->parentId->id)}}">{{strtoupper($data->parentId->nama_depan)}}
                                                    </a> </td>
                                            @endif
                                            <td>
                                                <a href="{{route('principle-detail-user',$data->id)}}"
                                                    class="btn btn-outline-secondary btn-sm search"
                                                    title="Search">
                                                    <i class="fas fa-search"></i>
                                                </a>
                                                @if($role->id == 3)
                                                <a class="btn btn-outline-secondary btn-sm edit" title="Search">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) < 1) <tr>
                                            <td colspan="@if($role->id == 3) 8 @elseif($role->id == 4) 9 @else 7 @endif"
                                                align="center">Kosong</td>
                                            </tr>
                                            @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
@endpush