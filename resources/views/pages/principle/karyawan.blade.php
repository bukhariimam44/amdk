@extends('layouts.app')
@push('css')
<style>
    button.mt-6{
        margin-top: 6px !important;
    }
    .table-primarys{
        background-color:#ebecf1 !important;
        box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        border-color:#ced4da;
    }
</style>
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a href="{{route('admin-create-karyawan')}}" class="btn btn-success">Create</a>
                        </div>
                        <h4>Karyawan</h4> 
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Karyawan</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('admin-karyawan')}}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Name</label>
                                            <input type="text" name="name" value="{{$name}}" class="table-primary form-control" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Select Store</label>
                                            <select name="store_id" id="" class="form-control select2 @error('store_id') parsley-error @enderror">
                                                <option value="">All Store</option>
                                                @foreach($stores as $store)
                                                <optgroup label="{{$store->store_name}}">
                                                <option value="{{$store->id}}" @if($store_id == $store->id) selected @endif>{{$store->store_name}}</option>
                                                @foreach($store->parentStore as $parent)
                                                <option value="{{$parent->id}}" @if($store_id == $parent->id) selected @endif>{{$parent->store_name}}</option>
                                                @endforeach
                                                </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Select Account Type</label>
                                            <select name="user_role" id="" class="form-control select2-search-disable @error('user_role') parsley-error @enderror">
                                                <option value="">All Account Type</option>
                                                @foreach($user_roles as $role)
                                                <option value="{{$role->id}}" @if($roles == $role->id) selected @endif>{{$role->role}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Store</th>
                                            <th>Account Type</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <td>{{$key+1}}.</td>
                                            <td><img src="{{$data->url}}" alt="" width="50px"></td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->username}}</td>
                                            <td>{{$data->storeId->store_name}}</td>
                                            <td>{{$data->roleId->role}}</td>
                                            <td>@if($data->open == 1)<span class="badge bg-success">Active</span> @else <span class="badge bg-error">NonActive</span> @endif</td>
                                            <td>
                                                <a class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a class="btn btn-outline-secondary btn-sm delete" title="Delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) == 0)
                                        <tr>
                                            <td colspan="8" class="text-center">No Data</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>
@endpush