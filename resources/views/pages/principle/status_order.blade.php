@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Status Order</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Status Order</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body" id="product">
                        <form action="{{route('principle-status-order')}}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Nomor Order</label>
                                            <input type="text" name="no_order" value="{{$no_order}}" class="table-primary form-control" placeholder="Enter Nomor Order">
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="cari">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>No. Order</th>
                                            <th>Tanggal</th>
                                            <th>Item</th>
                                            <th>Qty</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                            <th>@if(Auth::user()->user_role_id == 2) Distributor @elseif(Auth::user()->user_role_id == 3) Agen @elseif(Auth::user()->user_role_id == 4) End User @endif</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <th scope="row">{{$key+1}}.</th>
                                            <td>{{$data->no_order}}</td>
                                            <td>{{date('d M Y H:i', strtotime($data['transaction_date']))}}</td>
                                            <td>{{count($data->item)}}</td>
                                            <td>{{$data->item->sum('qty')}}</td>
                                            <td>{{number_format($data->harga_total,0,',','.')}}</td>
                                            <td> <div class="badge @if($data->order_status_id == 2) badge-soft-primary @else badge-soft-danger @endif font-size-14">{{$data->status->status}}</div></td>
                                            <td>{{$data->user->nama_depan}}</td>
                                            <td>
                                            <form action="{{route('principle-status-order')}}" method="post">
                                            @csrf
                                            <input type="hidden" name="action" value="proses">
                                            <input type="hidden" name="no_order" value="{{encrypt($data->no_order)}}">
                                            @if($data->order_status_id == 2)<Button class="btn btn-primary btn-sm ">PROSES PENGIRIMAN </Button> @elseif($data->order_status_id == 3) <button class="btn btn-primary btn-sm ">DITERIMA</button> @endif
                                                <a href="{{route('principle-detail-status-order',encrypt($data->id))}}" class="btn btn-warning btn-sm edit" title="Edit">
                                                    <i class="fas fa-search"></i> Detail Item
                                                </a>
                                            </form>    
                                            
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) == 0)
                                        <tr>
                                            <td colspan="9" class="text-center">No Data</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var form = new Vue({
    el:'#product',
    data:{},
    methods: {
        loading(text){
            Swal.fire({
                text: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
            );
        },
        hapus(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert " + id.product_name,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    this.delete(id.id)
                }
            })
        },
        async delete(id){
            this.loading('Waiting...')
            const deletestore = "<?php echo route('principle-delete-product');?>";
            const request = {product_id:id};
            await axios.delete(deletestore,{data:request}).then((response) =>{
                Swal.fire(
                    response.data.title,
                    response.data.message,
                    response.data.icon
                )
                if (response.data.code === 200) {
                    setTimeout(() => {
                        window.location.href = "{{route('principle-product')}}";
                    }, 1000);
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        }
    },
});
</script>
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
@endpush