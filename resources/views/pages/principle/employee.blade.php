@extends('layouts.app')
@push('css')
<style>
    button.mt-6{
        margin-top: 6px !important;
    }
    .table-primarys{
        background-color:#ebecf1 !important;
        box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        border-color:#ced4da;
    }
</style>
@endpush
@section('content')
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a href="{{route('admin-create-employee')}}" class="btn btn-success">Create</a>
                        </div>
                        <h4>Employee</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Employee</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="" method="post">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Name</label>
                                            <input type="text" name="name" class="table-primary form-control" placeholder="Enter Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Username</label>
                                            <input type="text" name="username" class="table-primary form-control" placeholder="Enter Username">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Photo</th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Account Type</th>
                                            <th>Asign To Store</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <td>{{$key+1}}.</td>
                                            <td><img src="{{$data->url_photo}}" alt="" width="50px"></td>
                                            <td>{{$data->name}}</td>
                                            <td>{{$data->username}}</td>
                                            <td>{{$data->roleId->role}}</td>
                                            <td>
                                                @foreach($data->Store as $store)
                                                    @if($key > 0) , @endif {{$store->store_name}}&nbsp;
                                                @endforeach
                                            </td>
                                            <td><span class="badge bg-success">Active</span></td>
                                            <td><a class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a></td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) == 0)
                                        <tr>
                                            <td colspan="8" class="text-center">No Data</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush