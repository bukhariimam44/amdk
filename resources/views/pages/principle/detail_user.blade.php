@extends('layouts.app')
@push('css')

<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content" id="user">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Detail User</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Detail User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        NIK </label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{$user->nik}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Nama Lengkap </label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{strtoupper($user->nama_depan)}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Email </label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{$user->email}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Phone </label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->phone}}</label>
                                    </div>
                                </div>

                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Membership</label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{$user->membership->membership}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Posisi</label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{$user->roleId->role}}</label>
                                    </div>
                                </div>
                                @if($user->user_role_id == 5 )
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Agen</label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{strtoupper($user->parentId->nama_depan)}}</label>
                                    </div>
                                </div>
                                @endif
                                @if($user->user_role_id == 5 || $user->user_role_id == 4 )
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Distributor</label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: @if($user->user_role_id == 3){{strtoupper($user->parentId->parentId->nama_depan)}} @else {{strtoupper($user->parentId->nama_depan)}} @endif</label>
                                    </div>
                                </div>
                                @endif
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        RT / RW</label>
                                    <div class="col-sm-9">
                                        <label class="mt-2">: {{$user->rt}} / {{$user->rw}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Alamat</label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->alamat}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Pripinsi</label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->provinsi->name}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Kabupaten</label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->kabupaten->name}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Kecamatan</label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->kecamatan->name}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Kelurahan</label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->kelurahan->name}}</label>
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <label for="example-text-input" class="col-sm-3 col-form-label">
                                        Kode POS</label>
                                    <div class="col-sm-9">
                                    <label class="mt-2">: {{$user->kode_pos}}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
@endpush