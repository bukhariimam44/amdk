@extends('layouts.app')
@push('css')
<style>
    button.mt-6{
        margin-top: 6px !important;
    }
    .table-primarys{
        background-color:#ebecf1 !important;
        box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        border-color:#ced4da;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a href="{{route('agen-create-data-end-user')}}" class="btn btn-success">Create</a>
                        </div>
                        <h4>Data End User</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">End User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>RT/RW</th>
                                            <th>Membership</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <th scope="row">{{$key+1}}.</th>
                                            <td>{{$data->nama_depan}}</td>
                                            
                                            <td>{{$data->email}}</td>
                                            <td>{{$data->phone}}</td>
                                            <td>{{$data->rt}}/{{$data->rw}}</td>
                                            <td>{{$data->membership->membership}}</td>
                                            <td>
                                                <a href="{{route('agen-detail-end-user',encrypt($data->id))}}" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-search"></i>
                                                </a>
                                                <!-- <a class="btn btn-outline-secondary btn-sm edit" title="Search">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a> -->
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) < 1)
                                        <tr>
                                            <td colspan="6" align="center">Kosong</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush