@extends('layouts.app')
@push('css')
<link rel="stylesheet" href="{{asset('assets/libs/twitter-bootstrap-wizard/prettify.css')}}">
@endpush
@section('content')
<div class="page-content" id="create_user">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create End User</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">End User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <!-- <form action="{{route('principle-create-data-user')}}" method="post" enctype="multipart/form-data" id="create"> -->
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        NIK *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control "
                                                type="number" v-model="nik"
                                                placeholder="NIK" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('name_depan')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Nama Depan *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control "
                                                type="text" v-model="nama_depan"
                                                placeholder="Nama Depan" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('name_depan')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Nama Belakang *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="text" v-model="nama_belakang"
                                                placeholder="Nama Belakang" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('name_belakang')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Email *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="email" v-model="email"
                                                placeholder="Email" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('email')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Phone *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="text" v-model="phone"
                                                placeholder="Phone" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('phone')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Membership *</label>
                                        <div class="col-sm-10">
                                            <select v-model="membership_id" id="" class="form-control" v-on:keyup.enter="simpan">
                                                <option v-for="member in memberships" :key="member.id" :value="member.id">@{{member.membership}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        RT *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="interger" v-model="rt"
                                                placeholder="RT" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        RW *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="integer" v-model="rw"
                                                placeholder="RW" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Alamat *</label>
                                        <div class="col-sm-10">
                                            <textarea v-model="alamat" placeholder="Alamat" class="form-control"  v-on:keyup.enter="simpan" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Pripinsi *</label>
                                        <div class="col-sm-10">
                                            <select v-model="province_id" id="" class="form-control" @change="kabupaten($event)">
                                                <option value="">Pilih</option>
                                                <option v-for="province in provinces" :key="province.id" :value="province.id">@{{province.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kabupaten *</label>
                                        <div class="col-sm-10">
                                            <select v-model="regency_id" id="" class="form-control" @change="kecamatan($event)">
                                                <option value="">Pilih</option>
                                                <option v-for="regency in regencys" :key="regency.id" :value="regency.id">@{{regency.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kecamatan *</label>
                                        <div class="col-sm-10">
                                            <select v-model="district_id" id="" class="form-control" @change="kelurahan($event)">
                                                <option value="">Pilih</option>
                                                <option v-for="district in districts" :key="district.id" :value="district.id">@{{district.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kelurahan *</label>
                                        <div class="col-sm-10">
                                            <select v-model="village_id" id="" class="form-control">
                                                <option value="">Pilih</option>
                                                <option v-for="village in villages" :key="village.id" :value="village.id">@{{village.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kode POS *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="integer" v-model="kode_pos"
                                                placeholder="Kode POS" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Password *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="password" v-model="password"
                                                placeholder="Enter Password" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                    
                                </div>
                                <input type="hidden" name="action" value="create">
                                <div class="mb-0">
                                    <div>
                                        <button @click="simpan()" type="buttom" class="btn btn-primary waves-effect waves-light me-1">
                                            SAVE
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            <!-- </form> -->

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<!-- twitter-bootstrap-wizard js -->
<script src="{{asset('assets/libs/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>

<script src="{{asset('assets/libs/twitter-bootstrap-wizard/prettify.js')}}"></script>

<!-- form wizard init -->
<script src="{{asset('assets/js/pages/form-wizard.init.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    var user = new Vue({
        el:'#create_user',
        data:{
            nama_depan:'',
            nama_belakang:'',
            email:'',
            phone:'',
            membership_id:'',
            memberships:[
                {
                    id:'',
                    membership:'Pilih Membership'
                },
                {
                    id:1,
                    membership:'Anggota'
                },
                {
                    id:2,
                    membership:'Non Anggota'
                }
            ],
            nik:'',
            rt:'',
            rw:'',
            alamat:'',
            province_id:'',
            provinces:[],
            regency_id:'',
            regencys:[],
            district_id:'',
            districts:[],
            village_id:'',
            villages:[],
            kode_pos:'',
            password:'',
        },
        mounted(){
            this.propinsi()
        },
        methods:{
            async propinsi(){
                const url = "<?php echo route('provinsi');?>";
                await axios.get(url).then((response) =>{
                    console.log(response.data);
                    this.province_id = '';
                    this.provinces = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async kabupaten(e){
                const url = "<?php echo route('kabupaten');?>";
                const request = { propinsi_id: e.target.value}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    this.regency_id = '';
                    this.regencys = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async kecamatan(e){
                const url = "<?php echo route('kecamatan');?>";
                const request = { kabupaten_id: e.target.value}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    this.district_id = '';
                    this.districts = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async kelurahan(e){
                const url = "<?php echo route('kelurahan');?>";
                const request = { kecamatan_id: e.target.value}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    this.village_id = '';
                    this.villages = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async simpan(){
                const url = "<?php echo route('agen-create-data-end-user');?>";
                const request = { nik:this.nik, nama_depan : this.nama_depan, nama_belakang:this.nama_belakang,  email:this.email, phone:this.phone, membership_id:this.membership_id, rt:this.rt, rw:this.rw, alamat:this.alamat, province_id:this.province_id, regency_id:this.regency_id, district_id:this.district_id, village_id:this.village_id, kode_pos:this.kode_pos, password:this.password, action:'create'}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    Swal.fire({
                        icon: response.data.icon,
                        title: response.data.title,
                        text: response.data.message,
                    })
                    if (response.data.code === 200) {
                        window.location.href = "{{route('agen-data-end-user')}}";
                    }
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });
            }
        }
    })
</script>
@endpush