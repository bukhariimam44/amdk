@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Dashboard</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">

        <div class="page-content-wrapper">


            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body">



                            <h4 class="header-title mb-4 float-sm-start">Penjualan Tahun</h4>

                            <div class="float-sm-end">
                                <ul class="nav nav-pills">
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Day</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Week</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Month</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#">Year</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="clearfix"></div>


                            <div class="row align-items-center">
                                <div class="col-xl-9">

                                    <div>
                                        <div id="stacked-column-chart" class="apex-charts" dir="ltr"></div>
                                    </div>

                                </div>


                                <div class="col-xl-3">
                                    <div class="dash-info-widget mt-4 mt-lg-0 py-4 px-3 rounded">



                                        <div class="media dash-main-border pb-2 mt-2">
                                            <div class="avatar-sm mb-3 mt-2">
                                                <span class="avatar-title rounded-circle bg-white shadow">
                                                    <i class="mdi mdi-currency-inr text-primary font-size-18"></i>
                                                </span>
                                            </div>
                                            <div class="media-body ps-3">

                                                <h4 class="font-size-20">Rp 1000</h4>
                                                <p class="text-muted">Earning <a href="#" class="text-primary">Withdraw
                                                        <i class="mdi mdi-arrow-right"></i></a>
                                                </p>

                                            </div>

                                        </div>





                                        <div class="media mt-4 dash-main-border pb-2">
                                            <div class="avatar-sm mb-3 mt-2">
                                                <span class="avatar-title rounded-circle bg-white shadow">
                                                    <i
                                                        class="mdi mdi-credit-card-outline text-primary font-size-18"></i>
                                                </span>
                                            </div>
                                            <div class="media-body ps-3">
                                                <h4 class="font-size-20">Rp 1000</h4>
                                                <p class="text-muted">To Paid <a href="#" class="text-primary">Pay <i
                                                            class="mdi mdi-arrow-right"></i></a></p>
                                            </div>
                                        </div>



                                        <div class="media mt-4">
                                            <div class="avatar-sm mb-2 mt-2">
                                                <span class="avatar-title rounded-circle bg-white shadow">
                                                    <i class="mdi mdi-eye-outline text-primary font-size-18"></i>
                                                </span>
                                            </div>
                                            <div class="media-body ps-3">
                                                <h4 class="font-size-20">Rp 1000</h4>
                                                <p class="text-muted mb-0">To Online <a href="#"
                                                        class="text-primary">View <i
                                                            class="mdi mdi-arrow-right"></i></a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>


                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
 <!-- Plugins js-->
 <script src="{{asset('assets/libs/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('assets/libs/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- apexcharts -->
    <script src="{{asset('assets/libs/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/js/pages/dashboard.init.js')}}"></script>

@endpush