@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            @if($role->id == 4)<a href="{{route('distributor-create-data-agen')}}" class="btn btn-success">Create</a>@endif
                        </div>
                        <h4>Data {{$role->role}}</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">{{$role->role}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>RT/RW</th>
                                            <th>Membership</th>
                                            @if($role->id == 5)
                                            <th>Agen</th>
                                            @endif
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <th scope="row">{{$key+1}}.</th>
                                            <td>{{$data->nama_depan}}</td>
                                            
                                            <td>{{$data->email}}</td>
                                            <td>{{$data->phone}}</td>
                                            <td>{{$data->rt}}/{{$data->rw}}</td>
                                            <td>{{$data->membership->membership}}</td>
                                            @if($role->id == 5)
                                            <td> <a href="{{route('distributor-detail-agen',encrypt($data->parentId->id))}}">{{strtoupper($data->parentId->nama_depan)}}</a> </td>
                                            @endif
                                            <td>
                                                <a href="{{route('distributor-detail-agen',encrypt($data->id))}}" class="btn btn-outline-secondary btn-sm detail" title="Detail">
                                                    <i class="fas fa-search"></i>
                                                </a>
                                                @if($role->id == 4)
                                                <!-- <a class="btn btn-outline-secondary btn-sm edit" title="Search">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a> -->
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) < 1)
                                        <tr>
                                            <td colspan="6" align="center">Kosong</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush