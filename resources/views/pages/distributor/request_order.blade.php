@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Request Order</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Request Order</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                        <form action="{{route('distributor-request-order')}}" method="post">
                            <input type="hidden" name="action" value="cari" id="">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Nomor Order</label>
                                            <input type="text" name="no_order" value="{{$no_order}}" class="table-primary form-control" placeholder="Nomor Order">
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="cari" id="">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                            <th>No. Order</th>
                                            <th>Tanggal</th>
                                            <th>Item</th>
                                            <th>Qty</th>
                                            <th>Harga</th>
                                            <th>Status</th>
                                            <th>Agen</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                        <th scope="row">{{$key+1}}.</th>
                                            <td>{{$data->no_order}}</td>
                                            <td>{{date('d M Y H:i', strtotime($data['transaction_date']))}}</td>
                                            <td>{{count($data->item)}}</td>
                                            <td>{{$data->item->sum('qty')}}</td>
                                            <td>{{number_format($data->harga_total,0,',','.')}}</td>
                                            <td> <div class="badge  @if($data->order_status_id == 1) badge-soft-warning @elseif($data->order_status_id == 2) badge-soft-primary  @elseif($data->order_status_id == 3) badge-soft-danger @else badge-soft-success @endif font-size-14">{{$data->status->status}}</div></td>
                                            <td><a href="{{route('distributor-detail-agen',encrypt($data->user_id))}}">{{strtoupper($data->user->nama_depan)}}</a></td>
                                            <td><a href="{{route('distributor-detail-request-order',encrypt($data->id))}}" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-search"></i> Detail
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) < 1)
                                        <tr>
                                            <td colspan="7" align="center">Kosong</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush