@extends('layouts.app')
@push('css')
<style>
    button.mt-6 {
        margin-top: 6px !important;
    }

    .table-primarys {
        background-color: #ebecf1 !important;
        box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
        border-color: #ced4da;
    }
    .img-fluid {
        max-width: 100%;
        height: 300px !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Product</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                @foreach($datas as $data)
                                <div class="col-xl-3 col-sm-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="product-img">
                                                    <a href="{{route('distributor-detail-product',encrypt($data->id))}}">
                                                        <img src="{{$data->Url}}" alt=""
                                                    class="img-fluid mx-auto d-block"></a>
                                            </div>
                                            <div class="text-center  mt-4">
                                                <a href="{{route('distributor-detail-product',encrypt($data->id))}}" class="text-dark">
                                                    <h5 class="font-size-18">{{$data->product_name}}</h5>
                                                </a>
                                                <h4 class="mt-3 mb-0">Rp {{number_format($data->harga_distributor,0,',','.')}} <span
                                                        class="font-size-14 text-muted me-2">
                                                        <!-- <del>$240</del> -->
                                                    </span></h4>
                                                    @if(count($data->cartId) > 0)
                                                    <form action="{{route('distributor-cart')}}" method="post">
                                                        <input type="hidden" name="product_id" value="{{encrypt($data->id)}}">
                                                        <input type="hidden" name="action" value="delete" id="">
                                                        <button class="btn btn-danger mt-4"><i class="dripicons-cart"></i> Hapus</button>
                                                        @csrf
                                                    </form>
                                                    @else
                                                    <form action="{{route('distributor-cart')}}" method="post">
                                                        <input type="hidden" name="action" value="add" id="">
                                                        <input type="hidden" name="product_id" value="{{encrypt($data->id)}}">
                                                        <button class="btn btn-primary mt-4"><i class="dripicons-cart"></i> Masukan</button>
                                                        @csrf
                                                    </form>
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush