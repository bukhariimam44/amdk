@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Detail Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Detail Product</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-5">
                                    <div class="product-detail">
                                        <div class="row">
                                            <div class="col-md-12 col-12">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade show active" id="product-1"
                                                        role="tabpanel">
                                                        <div class="product-img">
                                                            <img src="{{$data->url}}" alt=""
                                                                class="img-fluid mx-auto d-block"
                                                                data-zoom="{{$data->url}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end product img -->
                                </div>
                                <div class="col-xl-7">
                                    <div class="mt-4 mt-xl-3">
                                        <!-- <a href="#" class="text-primary">Chair</a> -->
                                        <h5 class="mt-1 mb-3">{{$data->product_name}}</h5>

                                        <div class="d-inline-flex">
                                            <div class="text-muted me-3">
                                                <span class="mdi mdi-star text-warning"></span>
                                                <span class="mdi mdi-star text-warning"></span>
                                                <span class="mdi mdi-star text-warning"></span>
                                                <span class="mdi mdi-star text-warning"></span>
                                                <span class="mdi mdi-star-half text-warning"></span>
                                            </div>
                                        </div>

                                        <h5 class="mt-2">Rp {{number_format($data->harga_end_user,0,',','.')}} {{--<span
                                                class="text-danger font-size-12 ms-2">25 % Off</span>--}}</h5>

                                        <hr class="my-4">

                                        <div class="mt-4">
                                            <h6>Deskripsi :</h6>

                                            <div class="mt-4">
                                                <p class="text-muted mb-2">{!! $data->description !!}</p>
                                                
                                            </div>
                                        </div>

                                        <div class="mt-4">
                                        @if(count($data->cartId) > 0)
                                                    <form action="{{route('end-user-cart')}}" method="post">
                                                        <input type="hidden" name="product_id" value="{{encrypt($data->id)}}">
                                                        <input type="hidden" name="action" value="delete" id="">
                                                        <button class="btn btn-danger mt-4"><i class="dripicons-cart"></i> Hapus</button>
                                                        @csrf
                                                    </form>
                                                    @else
                                                    <form action="{{route('end-user-cart')}}" method="post">
                                                        <input type="hidden" name="action" value="add" id="">
                                                        <input type="hidden" name="product_id" value="{{encrypt($data->id)}}">
                                                        <button class="btn btn-primary mt-4"><i class="dripicons-cart"></i> Masukan</button>
                                                        @csrf
                                                    </form>
                                                    @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end row -->
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush