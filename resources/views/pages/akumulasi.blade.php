@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/admin-resources/rwd-table/rwd-table.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    div.btn-toolbar{
        display: none !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <!-- <div class="float-end d-lg-block">
                            <form action="" method="post" id="transaksi">
                            @csrf
                            <input type="hidden" name="from" value="{{$from}}"/>
                            <input type="hidden" name="to" value="{{$to}}"/>
                            <input type="hidden" name="action" value="transaksi"/>
                            
                            </form>
                            <form action="" method="post" id="item">
                            @csrf
                            <input type="hidden" name="from" value="{{$from}}"/>
                            <input type="hidden" name="to" value="{{$to}}"/>
                            <input type="hidden" name="action" value="item"/>
                            </form>
                            @if(count($items) > 0)
                            <button type="button" class="btn btn-info" onclick="event.preventDefault(); document.getElementById('transaksi').submit();"><i class="mdi mdi-microsoft-excel"></i>   Export Transaksi</button> 
                            <button type="button"  onclick="event.preventDefault();
                                                    document.getElementById('item').submit();" class="btn btn-success"><i class="mdi mdi-microsoft-excel"></i>   Export Item</button>
                            @endif
                        </div> -->
                        <h4>Akumulasi Order</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Akumulasi Order</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body" id="product">
                        <form action="{{route('agen-akumulasi-search')}}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Posisi *</label>
                                            <select name="posisi" id="" class="form-control" required>
                                                <option value="">Pilih</option>
                                                @if(Auth::user()->user_role_id == 1 || Auth::user()->user_role_id == 2 )
                                                    @foreach(App\Models\UserRole::whereIn('role',['Distributor','Agen','End User'])->orderBy('id','ASC')->get() as $stt)
                                                    <option value="{{$stt->id}}" @if($posisi == $stt->id) selected @endif>{{$stt->role}}</option>
                                                    @endforeach
                                                @elseif(Auth::user()->user_role_id == 3)
                                                    @foreach(App\Models\UserRole::whereIn('role',['Agen','End User'])->orderBy('id','ASC')->get() as $stt)
                                                    <option value="{{$stt->id}}" @if($posisi == $stt->id) selected @endif>{{$stt->role}}</option>
                                                    @endforeach
                                                @elseif(Auth::user()->user_role_id == 4)
                                                    @foreach(App\Models\UserRole::whereIn('role',['End User'])->orderBy('id','ASC')->get() as $stt)
                                                    <option value="{{$stt->id}}" @if($posisi == $stt->id) selected @endif>{{$stt->role}}</option>
                                                    @endforeach
                                                @endif
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Tanggal *</label>
                                            <div class="input-daterange input-group" id="datepicker6" data-date-format="dd-mm-yyyy" data-date-autoclose="true" data-provide="datepicker" data-date-container='#datepicker6'>
                                                <input type="text" class="form-control" name="from" placeholder="Start Date" value="{{date('d-m-Y', strtotime($from))}}" />
                                                <input type="text" class="form-control" name="to" placeholder="End Date" value="{{date('d-m-Y', strtotime($to))}}"/>
                                            </div>
                                            <!-- <input type="date" name="from" value="{{$from}}" class="table-primary form-control" placeholder="Tanggal Dari"> -->
                                        </div>
                                     </div>
                                     <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Status Order</label>
                                            <select name="status" id="" class="form-control">
                                                <option value="">Semua</option>
                                                @foreach(App\Models\OrderStatus::get() as $stt)
                                                <option value="{{$stt->id}}" @if($status == $stt->id) selected @endif>{{$stt->status}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!--<div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Tanggal Sampai</label>
                                            <input type="date" name="to" value="{{$to}}" class="table-primary form-control" placeholder="Tanggal Sampai">
                                        </div>
                                    </div> -->
                                    <input type="hidden" name="action" value="cari">
                                    
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-rep-plugin">
                                <div class="table-responsive mb-0" data-pattern="priority-columns">
                                    <table id="tech-companies-1" class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Product</th>
                                                <th>Harga Satuan</th>
                                                <th>Quantity</th>
                                                
                                                <th>Total Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $qty = 0;
                                                $sub = 0;
                                                $harga = 0;
                                            @endphp
                                            @foreach($items as $key => $item)
                                            @php
                                                $qty+= $item->quantity;
                                                $sub+= $item->total_harga/$item->quantity;
                                                $harga+= $item->total_harga;
                                            @endphp
                                            <tr>
                                                <th scope="row">{{$key+1}}.</th>
                                                <td>{{$item->product_name}}</td>
                                                <td>Rp {{number_format($item->total_harga/$item->quantity,0,',','.')}}</td>
                                                <td>{{$item->quantity}}</td>
                                                <td>Rp {{number_format($item->total_harga,0,',','.')}}</td>
                                            </tr>
                                            </tbody>

                                            @endforeach

                                            @if(count($items) == 0)
                                            <tr>
                                                <td colspan="5" class="text-center">No Data</td>
                                            </tr>
                                            </tbody>
                                            @else
                                            <thead>
                                                <tr>
                                                    <th scope="row" colspan="2">Total</th>
                                                    <th>Rp {{number_format($sub,0,',','.')}}</th>
                                                    <th>{{$qty}}</th>
                                                    <th>Rp {{number_format($harga,0,',','.')}}</th>
                                                </tr>
                                            </thead>
                                            
                                            @endif
                                        <!-- </tbody> -->
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
        <!-- Responsive Table js -->
<script src="{{asset('assets/libs/admin-resources/rwd-table/rwd-table.min.js')}}"></script>

<!-- Init js -->
<script src="{{asset('assets/js/pages/table-responsive.init.js')}}"></script>


@endpush