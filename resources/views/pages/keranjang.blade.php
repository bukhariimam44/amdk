@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css')}}" rel="stylesheet" />
@endpush
@section('content')
<div class="page-content" id="keranjang">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Keranjang</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Keranjang</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('order-sekarang')}}" method="post">
                            @csrf
                                <div class="table-responsive">
                                    <table class="table table-centered mb-0 table-nowrap">
                                        <thead class="bg-light">
                                            <tr>
                                                <th style="width: 120px">Product</th>
                                                <th>Product Desc</th>
                                                <th>Price</th>
                                                <th>Quantity</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $total = 0;
                                            @endphp
                                            @foreach($datas as $data)
                                            @php
                                            $total+= (int)($data->product->harga_distributor*$data->jumlah);
                                            @endphp
                                            <tr>
                                                <td>
                                                    <img src="{{$data->product->Url}}"
                                                        alt="product-img" title="product-img" class="avatar-md" />
                                                </td>
                                                <td>
                                                    <h5 class="font-size-14 text-truncate"><a
                                                            href="#"
                                                            class="text-dark">{{$data->product->product_name}}</a></h5>
                                                    <p class="mb-0">Category : <span
                                                            class="font-weight-medium">{{$data->product->category->category}}</span>
                                                    </p>
                                                </td>
                                                <td>
                                                    Rp {{number_format($data->product->harga_distributor,0,',','.')}}
                                                </td>
                                                <td>
                                                    <div class="product-cart-touchspin" style="width: 120px;">
                                                        <div class="input-group bootstrap-touchspin bootstrap-touchspin-injected">
                                                            <span class="input-group-btn input-group-prepend">
                                                                <form action="{{route('distributor-keranjang')}}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="id" value="{{encrypt($data->id)}}">
                                                                    <input type="hidden" name="action" value="kurang">
                                                                    <button type="submit" class="btn btn-primary bootstrap-touchspin-down">-</button>
                                                                </form>
                                                            </span>
                                                            <input type="text" name="jumlah[]" value="{{$data->jumlah}}" class="form-control">
                                                            <input type="hidden" name="product_id[]" value="{{$data->product_id}}">
                                                            <span class="input-group-btn input-group-append">
                                                                <form action="{{route('distributor-keranjang')}}" method="post">
                                                                @csrf
                                                                <input type="hidden" name="action" value="tambah">
                                                                    <input type="hidden" name="id" value="{{encrypt($data->id)}}">
                                                                    <button type="submit" class="btn btn-primary bootstrap-touchspin-up">+</button>
                                                                </form>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                    Rp
                                                    {{number_format($data->product->harga_distributor*$data->jumlah,0,',','.')}}
                                                </td>
                                                <td style="width: 90px;" class="text-center">
                                                    <form action="{{route('distributor-keranjang')}}" method="post">
                                                                @csrf
                                                        <input type="hidden" name="action" value="hapus">
                                                        <input type="hidden" name="id" value="{{encrypt($data->id)}}">
                                                        <button type="submit" class="action-icon text-danger"> 
                                                            <i class="mdi mdi-trash-can font-size-18"></i>
                                                        </button>
                                                    </form>
                                                </td>
                                            </tr>
                                            @endforeach
                                            
                                            <tr class="bg-light text-end">

                                                <th scope="row" colspan="4">
                                                    TOTAL :
                                                </th>

                                                <td class="text-center">
                                                    Rp {{number_format($total,0,',','.')}}
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                            
                                        </tbody>
                                    </table>
                                </div>
                                @if(count($datas) > 0)
                                <div>
                                <center><button type="submit" class="btn btn-success">Order Sekarang</button> <button type="button" class="btn btn-primary">Update Keranjang</button></center>
                                </div>
                                @endif
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js')}}"></script>

<script src="{{asset('assets/js/pages/ecommerce-cart.init.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    var keranjang = new Vue({
        el: "#keranjang",
        data: {

        },
        methods: {

        }
    })
</script>
@endpush