@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Request Order</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Request Order</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                        <th>#</th>
                                            <th>No. Order</th>
                                            <th>Tanggal</th>
                                            <th>Item</th>
                                            <th>Qty</th>
                                            <th>Total Harga</th>
                                            <th>Status</th>
                                            <th>@if(Auth::user()->user_role_id == 2) Distributor @elseif(Auth::user()->user_role_id == 3) Agen @elseif(Auth::user()->user_role_id == 4) End User @endif</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                        <th scope="row">{{$key+1}}.</th>
                                            <td> <a href="{{route('detail-request-order',encrypt($data->id))}}">#{{$data->no_order}}</a> </td>
                                            <td>{{date('d M Y H:i', strtotime($data['transaction_date']))}}</td>
                                            <td>{{count($data->item)}}</td>
                                            <td>{{$data->item->sum('qty')}}</td>
                                            <td>Rp {{number_format($data->harga_total,0,',','.')}}</td>
                                            <td> <div class="badge badge-soft-warning font-size-14">{{$data->status->status}}</div></td>
                                            <td>{{$data->user->nama_depan}}</td>
                                            <td>
                                                <!-- <a href="" class="btn btn-success btn-sm proses text-white" title="Proses">
                                                <i class="fas fa-chalkboard-teacher"></i> Proses
                                                </a> -->
                                                <a href="{{route('detail-request-order',encrypt($data->id))}}" class="btn btn-primary btn-sm edit" title="Edit">
                                                    <i class="fas fa-search"></i> Detail Order
                                                </a>
                                                
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) < 1)
                                        <tr>
                                            <td colspan="9" align="center">Kosong</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

@endpush