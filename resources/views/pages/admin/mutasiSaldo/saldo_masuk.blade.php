@extends('layouts.app')
@push('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

@endpush
@section('content')
<div class="page-content" id="saldomasuk">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <div>
                                @if(Auth::user()->user_role_id == 2 && in_array("Saldo Masuk",json_decode(Auth::user()->actions)))
                                <a class="popup-form btn btn-success" href="#test-form">Add</a>
                                @elseif(Auth::user()->user_role_id == 1)
                                <a class="popup-form btn btn-success" href="#test-form">Add</a>
                                @endif
                            </div>
                            <div class="card mfp-hide mfp-popup-form mx-auto" id="test-form">
                                <div class="card-body">
                                    <h4 class="mt-0 mb-4">Material Masuk</h4>   
                                    <form action="{{route('admin-create-material-masuk')}}" method="post">
                                    @csrf
                                    <input type="hidden" name="crud" value="saldo_masuk">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="mb-3">
                                                    <label class="form-label" for="name">Pabrik</label>
                                                    <select v-model="selPabrik" id="" class="form-control" :class="msg.pabrik ? 'is-invalid':''">
                                                        <option v-for="pb in pabriks" v-bind:value="pb.id">@{{pb.nama_pabrik}}</option>
                                                    </select>
                                                    <span :class="msg.pabrik ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.pabrik }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="mb-3">
                                                    <label class="form-label" for="name">Supplier</label>
                                                    <select v-model="selSupplier" id="" class="form-control" :class="msg.supplier ? 'is-invalid':''">
                                                        <option v-for="sp in suppliers" v-bind:value="sp.id">@{{sp.supplier}}</option>
                                                    </select>
                                                    <span :class="msg.supplier ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.supplier }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="mb-3">
                                                    <label class="form-label" for="name">Material</label>
                                                    <select v-model="selMaterial" id="" class="form-control" :class="msg.material ? 'is-invalid':''">
                                                        <option v-for="mt in materials" v-bind:value="mt.id">@{{mt.material}}</option>
                                                    </select>
                                                    <span :class="msg.material ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.material }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label class="form-label" for="date">Tanggal</label>
                                                    <input v-model="tanggal" type="date" data-date="" class="form-control tanggal" :class="msg.tanggal ? 'is-invalid':''" data-date-format="DD MMMM YYYY">
                                                    <span :class="msg.tanggal ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.tanggal }}</strong>
                                                    </span>
                                                    <!-- <input type="date" data-date="" data-date-format="DD MMMM YYYY" class="form-control" id="date" placeholder="Tanggal Masuk"> -->
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label class="form-label" for="email">Saldo</label>
                                                    <input type="number" class="form-control" :class="msg.saldo ? 'is-invalid':''" v-model="saldo" placeholder="Saldo">
                                                    <span :class="msg.saldo ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.saldo }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="mb-3">
                                                    <label class="form-label" for="subject">Keterangan</label>
                                                    <textarea class="form-control" :class="msg.keterangan ? 'is-invalid':''" id="subject" v-model="keterangan" placeholder="Keterangan" rows="3"></textarea>
                                                    <span :class="msg.keterangan ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.keterangan }}</strong>
                                                    </span>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-end">
                                                    <button type="button" @click="save()" class="btn btn-primary">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>                            
                        </div>
                        <h4>Material Masuk</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Material Masuk</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                        <form action="{{route('admin-add-material-masuk')}}" method="post">
                            @csrf
                                <div class="row">
                                    
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Pabrik</label>
                                            <select name="pabrik" id="" class="form-control select2-search-disable">
                                                <option value="">Semua</option>
                                                @foreach($pabriks as $pb)
                                                <option value="{{$pb->id}}" @if($pabrik == $pb->id) selected @endif>{{$pb->nama_pabrik}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Supplier</label>
                                            <select name="supplier" id="" class="form-control select2-search-disable">
                                                <option value="">Semua</option>
                                                @foreach($suppliers as $sp)
                                                <option value="{{$sp->id}}" @if($supplier == $sp->id) selected @endif>{{$sp->supplier}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Material</label>
                                            <select name="material" id="" class="form-control select2-search-disable">
                                                <option value="">Semua</option>
                                                @foreach($materials as $mt)
                                                <option value="{{$mt->id}}" @if($material == $mt->id) selected @endif>{{$mt->material}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Date Range</label>
                                            <div class="input-daterange input-group" id="datepicker6" data-date-format="dd-mm-yyyy" data-date-autoclose="true" data-provide="datepicker" data-date-container='#datepicker6'>
                                                <input type="text" class="form-control" value="{{$from}}" name="from" placeholder="Start Date" />
                                                <input type="text" class="form-control" value="{{$end}}" name="end" placeholder="End Date" />
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="cari">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>

                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tgl Order</th>
                                            <th>Pabrik</th>
                                            <th>Supplier</th>
                                            <th>Material</th>
                                            <th>Jumlah</th>
                                            <th>User Proses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                            <tr>
                                                <td>{{$key+1}}.</td>
                                                <td>{{$data->transaction_date}}</td>
                                                <td>{{$data->pabrik->nama_pabrik}}</td>
                                                <td>{{$data->supplier->supplier}}</td>
                                                <td>{{$data->material->material}}</td>
                                                <td>{{number_format($data->saldo,0,',','.')}}</td>
                                                <td>{{$data->admin->nama_depan}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script> -->
<!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.3"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    var masuk = new Vue({
        el:"#saldomasuk",
        data:{
            pabriks:[],
            suppliers:[],
            materials:[],
            selPabrik:'',
            selSupplier:'',
            selMaterial:'',
            tanggal:'{{date('Y-m-d')}}',
            saldo:'',
            keterangan:'',
            msg:[],

        },
        mounted(){
            this.getPabrik();
            this.getSupplier();
            this.getMaterial();
        },
        methods: {
            async getPabrik(){
                const url = "<?php echo route('pabrik');?>";
                this.$http.get(url).then(function(response) {
                    this.pabriks = response.data;
                    this.selPabrik = response.data[0].id;
                });
            },
            async getSupplier(){
                const url = "<?php echo route('supplier');?>";
                this.$http.get(url).then(function(response) {
                    this.suppliers = response.data;
                    this.selSupplier = response.data[0].id;
                });
            },
            async getMaterial(){
                const url = "<?php echo route('material');?>";
                this.$http.get(url).then(function(response) {
                    this.materials = response.data;
                    this.selMaterial = response.data[0].id;
                });
            },
            async save(){
                const url = "<?php echo route('admin-add-material-masuk');?>";
                const request = {
                    pabrik: this.selPabrik, supplier:this.selSupplier, material: this.selMaterial,tanggal:this.tanggal,saldo:this.saldo,keterangan:this.keterangan, action:'add'
                }
                this.$http.post(url,request).then(function(response) {
                    if (response.data.code === 401) {
                        this.msg = response.data.errors
                        return false;
                    }
                    Swal.fire({
                        position: 'top',
                        icon: 'error',
                        title: response.data.title,
                        text:response.data.message,
                        showConfirmButton: false,
                        timer: 2000
                    })
                    if (response.data.code === 200) {
                        setTimeout(() => {
                            window.location.href = "";
                        }, 1000);
                    }
                });
            }
        }
    })
</script>
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
<script>
    $("input").on("change", function() {
    this.setAttribute(
        "data-date",
        moment(this.value, "YYYY-MM-DD")
        .format( this.getAttribute("data-date-format") )
    )
}).trigger("change")
</script>
@endpush