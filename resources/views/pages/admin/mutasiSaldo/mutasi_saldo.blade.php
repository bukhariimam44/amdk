@extends('layouts.app')
@push('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{asset('assets/libs/magnific-popup/magnific-popup.css')}}" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

@endpush
@section('content')
<div class="page-content" id="saldomasuk">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Mutasi Saldo</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Mutasi Saldo</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                        <form action="{{route('admin-post-mutasi-saldo')}}" method="post">
                            @csrf
                                <div class="row">
                                    
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Pabrik</label>
                                            <select name="pabrik" id="" class="form-control select2-search-disable" required>
                                                <option value="">Pilih </option>
                                                @foreach($pabriks as $pb)
                                                <option value="{{$pb->id}}" @if($pabrik == $pb->id) selected @endif>{{$pb->nama_pabrik}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Material</label>
                                            <select name="material" id="" class="form-control select2-search-disable" required>
                                            <option value="">Pilih </option>
                                                @foreach($materials as $mt)
                                                <option value="{{$mt->id}}" @if($material == $mt->id) selected @endif>{{$mt->material}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Date Range</label>
                                            <div class="input-daterange input-group" id="datepicker6" data-date-format="dd-mm-yyyy" data-date-autoclose="true" data-provide="datepicker" data-date-container='#datepicker6'>
                                                <input type="text" class="form-control" value="{{$from}}" name="from" placeholder="Start Date" />
                                                <input type="text" class="form-control" value="{{$end}}" name="end" placeholder="End Date" />
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="action" value="cari">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>

                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Tgl Order</th>
                                            <th>Pabrik</th>
                                            <th>Material</th>
                                            <th>Masuk</th>
                                            <th>Keluar</th>
                                            <th>Balance</th>
                                            <th>Keterangan</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                            <tr>
                                                <td>{{$key+1}}.</td>
                                                <td>{{$data->transaction_date}}</td>
                                                <td>{{$data->pabrik->nama_pabrik}}</td>
                                                <td>{{$data->material->material}}</td>
                                                <td>@if($data->mutasi == 'masuk'){{number_format($data->saldo,0,',','.')}} @else 0 @endif</td>
                                                <td>@if($data->mutasi == 'keluar'){{number_format($data->saldo,0,',','.')}} @else 0 @endif</td>
                                                <td>{{number_format($data->balance,0,',','.')}}</td>
                                                <td>{{$data->keterangan}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script> -->
<!-- <script src="https://unpkg.com/axios/dist/axios.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"
integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ=="
crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.3"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    var masuk = new Vue({
        el:"#saldomasuk",
        data:{
            pabriks:[],
            suppliers:[],
            materials:[],
            selPabrik:'',
            selSupplier:'',
            selMaterial:'',
            tanggal:'{{date('Y-m-d')}}',
            saldo:'',
            keterangan:'',
            msg:[],

        },
        mounted(){
            this.getPabrik();
            this.getSupplier();
            this.getMaterial();
        },
        methods: {
            async getPabrik(){
                const url = "<?php echo route('pabrik');?>";
                this.$http.get(url).then(function(response) {
                    this.pabriks = response.data;
                    this.selPabrik = response.data[0].id;
                });
            },
            async getSupplier(){
                const url = "<?php echo route('supplier');?>";
                this.$http.get(url).then(function(response) {
                    this.suppliers = response.data;
                    this.selSupplier = response.data[0].id;
                });
            },
            async getMaterial(){
                const url = "<?php echo route('material');?>";
                this.$http.get(url).then(function(response) {
                    this.materials = response.data;
                    this.selMaterial = response.data[0].id;
                });
            },
            async save(){
                const url = "<?php echo route('admin-add-material-masuk');?>";
                const request = {
                    pabrik: this.selPabrik, supplier:this.selSupplier, material: this.selMaterial,tanggal:this.tanggal,saldo:this.saldo,keterangan:this.keterangan, action:'add'
                }
                this.$http.post(url,request).then(function(response) {
                    if (response.data.code === 401) {
                        this.msg = response.data.errors
                    }
                    alert(JSON.stringify(response.data))
                });
            }
        }
    })
</script>
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/libs/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/js/pages/lightbox.init.js')}}"></script>
<script>
    $("input").on("change", function() {
    this.setAttribute(
        "data-date",
        moment(this.value, "YYYY-MM-DD")
        .format( this.getAttribute("data-date-format") )
    )
}).trigger("change")
</script>
@endpush