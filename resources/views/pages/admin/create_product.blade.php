@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .rupiah{
        text-align:left !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create Product</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper" id="form-store">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <form action="{{route('admin-create-product')}}" method="post" enctype="multipart/form-data" id="create">
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Category Product
                                             *</label>
                                        <div class="col-sm-10">
                                            <select name="category_id" class="form-control @error('category_id') parsley-error @enderror">
                                            <option value="">Selecte Category</option>
                                            @foreach($categories as $category)    
                                            <option value="{{$category->id}}">{{$category->category}}</option>
                                            @endforeach
                                            </select>
                                            @error('category_id')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Product
                                            Name *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('product_name') parsley-error @enderror"
                                                type="text" name="product_name"
                                                placeholder="Product Name" value="{{old('product_name')}}" id="example-text-input">
                                            @error('product_name')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Harga Distributor *</label>
                                        <div class="col-sm-10">
                                        <input placeholder="Harga Distributor" name="harga_distributor" value="{{old('harga_distributor')}}" class="form-control text-left @error('harga_distributor') parsley-error @enderror rupiah" id="rupiah1">
                                            @error('harga_distributor')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Harga Agen *</label>
                                        <div class="col-sm-10">
                                        <input placeholder="Harga Agen" name="harga_agen" value="{{old('harga_agen')}}" class="form-control input-mask text-left @error('harga_agen') parsley-error @enderror rupiah" id="rupiah2">
                                            @error('harga_agen')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Harga End User *</label>
                                        <div class="col-sm-10">
                                        <input placeholder="Harga End User" name="harga_end_user" value="{{old('harga_end_user')}}" class="form-control input-mask text-left @error('harga_end_user') parsley-error @enderror rupiah" id="rupiah3">
                                            @error('harga_end_user')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Stok *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('stok') parsley-error @enderror"
                                                type="text" name="stok"
                                                placeholder="Enter Stok" value="{{old('stok')}}" id="example-text-input">
                                            @error('stok')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-search-input"
                                            class="col-sm-2 col-form-label">Description *</label>
                                        <div class="col-sm-10">
                                            <textarea name="description"
                                                class="form-control @error('description') parsley-error @enderror" cols="30"
                                                rows="4" placeholder="Enter Description">{{old('description')}}</textarea>
                                            @error('description')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-email-input" class="col-sm-2 col-form-label">Product Image *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('product_image') parsley-error @enderror"
                                                type="file" accept="image/*" name="product_image" value="{{old('product_image')}}">
                                            @error('product_image')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="create">
                                <div class="mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                            Submit
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script type="text/javascript">
		
		var rupiah = document.getElementById('rupiah1');
		rupiah1.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah1.value = formatRupiah(this.value, 'Rp ');
		});
        var rupiah2 = document.getElementById('rupiah2');
		rupiah2.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah2.value = formatRupiah(this.value, 'Rp ');
		});
        var rupiah3 = document.getElementById('rupiah3');
		rupiah3.addEventListener('keyup', function(e){
			// tambahkan 'Rp.' pada saat form di ketik
			// gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
			rupiah3.value = formatRupiah(this.value, 'Rp ');
		});
 
		/* Fungsi formatRupiah */
		function formatRupiah(angka, prefix){
			var number_string = angka.replace(/[^,\d]/g, '').toString(),
			split   		= number_string.split(','),
			sisa     		= split[0].length % 3,
			rupiah     		= split[0].substr(0, sisa),
			ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
 
			// tambahkan titik jika yang di input sudah menjadi angka ribuan
			if(ribuan){
				separator = sisa ? '.' : '';
				rupiah += separator + ribuan.join('.');
			}
 
			rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
			return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
		}
	</script>
<!-- form wizard init -->
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>

<script src="{{asset('assets/libs/inputmask/jquery.inputmask.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-mask.init.js')}}"></script>
@endpush