@extends('layouts.app')
@push('css')
<link rel="stylesheet" href="{{asset('assets/libs/twitter-bootstrap-wizard/prettify.css')}}">
@endpush
@section('content')
<div class="page-content" id="create_user">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Edit User</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Edit User</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <!-- <form action="" method="post" enctype="multipart/form-data" id="create"> -->
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        NIK *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control "
                                                type="number" v-model="detail.nik"
                                                placeholder="NIK" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('name_depan')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Nama Lengkap *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control "
                                                type="text" v-model="detail.nama_depan"
                                                placeholder="Nama Depan" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('name_depan')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Email *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="email" v-model="detail.email"
                                                placeholder="Email" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('email')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Phone *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="text" v-model="detail.phone"
                                                placeholder="Phone" id="example-text-input" v-on:keyup.enter="simpan">
                                            @error('phone')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Membership *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.membership_id" id="" class="form-control" v-on:keyup.enter="simpan">
                                                <option v-for="member in memberships" :key="member.id" :value="member.id">@{{member.membership}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Posisi *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.user_role_id" id="" class="form-control" v-on:keyup.enter="simpan">
                                                <option value="">Pilih</option>
                                                <option v-for="ps in posisi" :key="ps.id" :value="ps.id">@{{ps.text}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div v-if="detail.user_role_id === 4" class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label"> Distributor *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.parent_id" id="" class="form-control" v-on:keyup.enter="simpan">
                                                <option value="">Pilih</option>
                                                <option v-for="user in users" :key="user.id" :value="user.id">@{{user.nama_depan}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div v-if="detail.user_role_id === 5" class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Agen *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.parent_id" id="" class="form-control" v-on:keyup.enter="simpan">
                                                <option value="">Pilih</option>
                                                <option v-for="user in users" :key="user.id" :value="user.id">@{{user.nama_depan}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        RT *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="number" v-model="detail.rt"
                                                placeholder="RT" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        RW *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="number" v-model="detail.rw"
                                                placeholder="RW" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Alamat *</label>
                                        <div class="col-sm-10">
                                            <textarea v-model="detail.alamat" placeholder="Alamat" class="form-control"  v-on:keyup.enter="simpan" cols="30" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Pripinsi *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.province_id" id="" class="form-control" @change="kabupaten($event.target.value,'pilih')">
                                                <option value="">Pilih</option>
                                                <option v-for="province in provinces" :key="province.id" :value="province.id">@{{province.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kabupaten *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.regency_id" id="" class="form-control" @change="kecamatan($event.target.value,'pilih')">
                                                <option value="">Pilih</option>
                                                <option v-for="regency in regencys" :key="regency.id" :value="regency.id">@{{regency.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kecamatan *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.district_id" id="" class="form-control" @change="kelurahan($event.target.value,'pilih')">
                                                <option value="">Pilih</option>
                                                <option v-for="district in districts" :key="district.id" :value="district.id">@{{district.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kelurahan *</label>
                                        <div class="col-sm-10">
                                            <select v-model="detail.village_id" id="" class="form-control">
                                                <option value="">Pilih</option>
                                                <option v-for="village in villages" :key="village.id" :value="village.id">@{{village.name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">
                                        Kode POS *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control"
                                                type="number" v-model="detail.kode_pos"
                                                placeholder="Kode POS" id="example-text-input" v-on:keyup.enter="simpan">
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="create">
                                <div class="mb-0">
                                    <div>
                                        <button @click="simpan()" type="buttom" class="btn btn-primary waves-effect waves-light me-1">
                                            SAVE
                                        </button>
                                        <button data-bs-toggle="modal" data-bs-target=".bs-example-modal-center" type="button" class="btn btn-secondary waves-effect">
                                            Ganti Password
                                        </button>
                                    </div>
                                </div>
                            <!-- </form> -->
                                <div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title mt-0">Ganti Password</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                                    
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-3">
                                                    <label for="userpassword">Password Baru</label>
                                                    <div class="input-group" id="datepicker1">
                                                    <span class="input-group-text"><i class="mdi mdi-lock"></i></span>
                                                    <input :type="passwordFieldType" class="form-control" id="userpassword" v-model="password_baru"
                                                        placeholder="Password Baru" v-on:keyup.enter="simpan_password">
                                                        <span @click="switchVisibility()" class="input-group-text"><i class="mdi" :class="passwordFieldType === 'password'? 'mdi-eye':'mdi-eye-off'"></i></span>
                                                        <span :class="msg.password ? 'invalid-feedback':''" role="alert">
                                                            <strong>@{{ msg.password }}</strong>
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="userpassword">Ulangi Password Baru</label>
                                                    <div class="input-group" id="datepicker1">
                                                        <span class="input-group-text"><i class="mdi mdi-lock"></i></span>
                                                        <input :type="passwordFieldType" class="form-control" id="userpassword" v-model="ulangi_password_baru"
                                                            placeholder="Ulangi Password Baru" v-on:keyup.enter="simpan_password">
                                                        <span  :class="msg.password_confirmation ? 'invalid-feedback':''" role="alert">
                                                            <strong>@{{ msg.password_confirmation }}</strong>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<!-- twitter-bootstrap-wizard js -->
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<!-- form wizard init -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    var user = new Vue({
        el:'#create_user',
        data:{
            id:"{{$id}}",
            detail:'',
            nama_depan:'',
            nama_belakang:'',
            email:'',
            phone:'',
            membership_id:'',
            memberships:[
                {
                    id:'',
                    membership:'Pilih Membership'
                },
                {
                    id:1,
                    membership:'Anggota'
                },
                {
                    id:2,
                    membership:'Non Anggota'
                }
            ],
            parent_id:'',
            users: [],
            nik:'',
            rt:'',
            rw:'',
            alamat:'',
            province_id:'',
            provinces:[],
            regency_id:'',
            regencys:[],
            district_id:'',
            districts:[],
            village_id:'',
            villages:[],
            kode_pos:'',
            role:'',
            posisi:[
                {
                    id:1, text:'Admin'
                },
                {
                    id:2, text:'Principle'
                },
                {
                    id:3, text:'Distributor'
                },
                {
                    id:4, text:'Agen'
                },
                {
                    id:5, text:'End User'
                }
            ],
            msg:[],
            password_baru:'',
            ulangi_password_baru:'',
            passwordFieldType:'password'
        },
        mounted(){
            this.propinsi()
            this.detailUser()
            this.userRole()
        },
        methods:{
            switchVisibility() {
                this.passwordFieldType = this.passwordFieldType === "password" ? "text" : "password";
            },

            async simpan_password(){
                const url = "<?php echo route('admin.ganti-password');?>";
                await axios.post(url,{id:this.id, password:this.password_baru, password_confirmation:this.ulangi_password_baru}).then((response) =>{
                    console.log('simpan_password = '+JSON.stringify(response.data));
                    Swal.fire({
                        icon: response.data.icon,
                        title: response.data.title,
                        text: response.data.message,
                    })
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });
            },
            async detailUser(){
                const url = "<?php echo route('admin-data-user-admin');?>";
                await axios.post(url,{id:this.id, role:this.role, action:'detail'}).then((response) =>{
                    console.log('detail = '+JSON.stringify(response.data));
                    this.detail = response.data;
                    this.kabupaten(this.detail.province_id,'load')
                    this.kecamatan(this.detail.regency_id,'load')
                    this.kelurahan(this.detail.district_id,'load')
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async userRole(){
                const url = "<?php echo route('admin-user-role');?>";
                await axios.post(url,{id:this.id}).then((response) =>{
                    console.log(response.data);
                    this.parent_id = '';
                    this.users = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async propinsi(){
                const url = "<?php echo route('provinsi');?>";
                await axios.get(url).then((response) =>{
                    console.log(response.data);
                    this.province_id = '';
                    this.provinces = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async kabupaten(e,a){
                const url = "<?php echo route('kabupaten');?>";
                const request = { propinsi_id: e}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    if (a === 'pilih') {
                        this.detail.regency_id = '';
                        this.detail.district_id = '';
                        this.detail.village_id = '';
                    }
                   
                    this.regencys = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async kecamatan(e,a){
                const url = "<?php echo route('kecamatan');?>";
                const request = { kabupaten_id: e}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    if (a === 'pilih') {
                        this.detail.district_id = '';
                        this.detail.village_id = '';
                    }
                    this.districts = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async kelurahan(e,a){
                const url = "<?php echo route('kelurahan');?>";
                const request = { kecamatan_id: e}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    if (a === 'pilih') {
                        this.detail.village_id = '';
                    }
                    this.villages = response.data;
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });

            },
            async simpan(){
                const url = "<?php echo route('admin-create-data-user');?>";
                const request = { id:this.detail.id,nik:this.detail.nik, nama_depan : this.detail.nama_depan,  email:this.detail.email, phone:this.detail.phone, membership_id:this.detail.membership_id, rt:this.detail.rt, rw:this.detail.rw, alamat:this.detail.alamat, province_id:this.detail.province_id, regency_id:this.detail.regency_id, district_id:this.detail.district_id, village_id:this.detail.village_id, kode_pos:this.detail.kode_pos, role:this.detail.user_role_id, parent_id: this.detail.parent_id, action:'update'}
                await axios.post(url,request).then((response) =>{
                    console.log(response.data);
                    Swal.fire({
                        icon: response.data.icon,
                        title: response.data.title,
                        text: response.data.message,
                    })
                    if (response.data.code === 200) {
                        window.location.href = response.data.url;
                    }
                },(response)=>{
                    console.log('ERROR: '+response);
                    Swal.close()
                });
            }
        }
    })
</script>
@endpush