@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create Material</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create Material</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <form action="{{route('admin-create-material')}}" method="post" id="create">
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Nama Material</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('material') parsley-error @enderror"
                                                type="text" name="material" value="{{old('material')}}"
                                                placeholder="Nama material" id="example-text-input">
                                            @error('material')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Keterangan</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control @error('keterangan') parsley-error @enderror"
                                                type="text" name="keterangan"
                                                placeholder="Keterangan" id="example-text-input">{{old('keterangan')}}</textarea>
                                            @error('keterangan')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="create">
                                <div class="mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                            Simpan
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
@endpush