@extends('layouts.app')
@push('css')
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a href="{{route('admin-create-product')}}" class="btn btn-success">Create</a>
                        </div>
                        <h4>Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Product</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body" id="product">
                        <form action="{{route('admin-product')}}" method="post">
                            @csrf
                                <div class="row">
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                            <label class="form-label" for="basicpill-firstname-input">Product Name</label>
                                            <input type="text" name="product_name" value="" class="table-primary form-control" placeholder="Enter Product Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="mb-3">
                                        <label class="form-label" for="basicpill-firstname-input"></label>
                                            <button class="mt-6 btn btn-primary form-control">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <br>
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Harga Distributor</th>
                                            <th>Harga Agen</th>
                                            <th>Harga End User</th>
                                            <th>Stok</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($datas as $key => $data)
                                        <tr>
                                            <th scope="row">{{$key+1}}</th>
                                            <td><img src="{{$data->Url}}" alt="" width="50px"></td>
                                            <td>{{$data->product_name}}</td>
                                            <td>{{number_format($data->harga_distributor,0,',','.')}}</td>
                                            <td>{{number_format($data->harga_agen,0,',','.')}}</td>
                                            <td>{{number_format($data->harga_end_user,0,',','.')}}</td>
                                            <td>{{$data->stok}}</td>
                                            <td><a href="{{route('admin-edit-product',$data->id)}}" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a class="btn btn-outline-secondary btn-sm delete" @click="hapus(`{{$data->id}}`)" title="Delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @if(count($datas) == 0)
                                        <tr>
                                            <td colspan="9" class="text-center">No Data</td>
                                        </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script type="text/javascript" src="https://unpkg.com/vue@2.5.6/dist/vue.js"></script>
<script>
var form = new Vue({
    el:'#product',
    data:{},
    methods: {
        loading(text){
            Swal.fire({
                text: text,
                allowEscapeKey: false,
                allowOutsideClick: false,
                background: '#FFFFFF',
                showConfirmButton: false,
                onOpen: ()=>{
                                Swal.showLoading();
                }
            }).then((dismiss) => {
                // Swal.showLoading();
                }
            );
        },
        hapus(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert ",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    this.delete(id)
                }
            })
        },
        async delete(id){
            this.loading('Waiting...')
            const deletestore = "<?php echo route('admin-delete-product');?>";
            const request = {product_id:id};
            await axios.post(deletestore,request).then((response) =>{
                console.log(response.data);
                Swal.fire(
                    response.data.title,
                    response.data.message,
                    response.data.icon
                )
                if (response.data.code === 200) {
                    setTimeout(() => {
                        window.location.href = "{{route('admin-product')}}";
                    }, 1000);
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        }
    },
});
</script>
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
@endpush