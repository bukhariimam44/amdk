@extends('layouts.app')
@push('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="page-content">
    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a href="{{route('admin-create-category-product')}}" class="btn btn-success">Create</a>
                        </div>
                        <h4>Principle Akses</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Principle Akses</li>
                        </ol>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid" id="category">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Akses</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for="(user, i) in users" :key="i">
                                            <td>@{{i+1}}.</td>
                                            <td>@{{user.nama_depan}}</td>
                                            <td>
                                                <span v-for="aks in akses">
                                                    <input type="checkbox" :id="aks.action+''+user.id" :value="aks.action" @click="changeAkses(aks.action,user.id)" :checked="checkAkses(aks.action,user.actions)">
                                                    <label :for="aks.action+''+user.id">@{{aks.action}} &nbsp; &nbsp; &nbsp; </label>
                                                </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')

<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script> 
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.3"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
var category = new Vue({
    el:'#category',
    data:{
        users:[],
        akses:[]
    },
    mounted(){
        this.load()
    },
    methods: {
        load(){
            const url = "<?php echo route('admin-data-principle-akses');?>";
            const request = {};
            this.$http.get(url).then(function(response) {
                this.users = response.data.users;
                this.akses = response.data.akses;
            });
        },
        checkAkses(akses,userItems){
            if (userItems.includes(akses)) {
                return true;
            }
            return false
        },
        changeAkses(akses,userId){
            const url = "<?php echo route('admin-change-akses');?>";
            const request = {
                akses:akses, user_id: userId
            };
            this.$http.post(url,request).then(function(response) {
                // console.log(JSON.stringify(response.data));
                this.load()
            });
        },
        confirmHapus(id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                if (result.isConfirmed) {
                    this.hapus(id)
                }
            })
        },
        async hapus(id){
            const url = "<?php echo route('admin-create-category-product');?>";
            const request = {category_id:id, action: 'hapus'};
            await axios.post(url,request).then((response) =>{
                Swal.fire({
                    icon: response.data.icon,
                    title: response.data.title,
                    text: response.data.message,
                })
                if (response.data.code === 200) {
                    location.reload()
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
            
        },
        async edit(item){
            const {
                    value: text
                } = await Swal.fire({
                    input: 'textarea',
                    inputValue: item.category,
                    inputLabel: 'Category Product',
                    inputPlaceholder: 'Category Product',
                    inputAttributes: {
                        autocapitalize: 'on'
                    },
                    showCancelButton: true
                })

                if (text) {
                    this.updateCategory(item,text)
                }
        },
        async updateCategory(item,category){
            const url = "<?php echo route('admin-create-category-product');?>";
            const request = {category_id:item.id, category:category,action: 'update'};
            await axios.post(url,request).then((response) =>{
                Swal.fire({
                    icon: response.data.icon,
                    title: response.data.title,
                    text: response.data.message,
                })
                if (response.data.code === 200) {
                    location.reload()
                }
            },(response)=>{
                console.log('ERROR: '+response);
                Swal.close()
            });
        }            
    },
});
</script>
@endpush