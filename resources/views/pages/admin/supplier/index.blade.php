@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content" id="supplier">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <div class="float-end d-sm-block">
                            <a
                                href="{{route('admin-form-supplier')}}"
                                class="btn btn-success">Create</a>
                        </div>
                        <h4>Data Supplier</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Data Supplier</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Supplier</th>
                                            <th>Telephone</th>
                                            <th>Alamat</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($datas as $key => $data)
                                        <tr>
                                            <td>{{$key+1}}.</td>
                                            <td>{{$data->supplier}}</td>
                                            <td>{{$data->telp}}</td>
                                            <td>{{$data->alamat}}</td>
                                            <td>
                                            <a href="{{route('admin-edit-supplier',$data->id)}}" class="btn btn-outline-secondary btn-sm edit" title="Edit">
                                                    <i class="fas fa-pencil-alt"></i>
                                                </a>
                                                <a class="btn btn-outline-secondary btn-sm delete" @click="hapus(`{{$data->id}}`)" title="Delete">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script> -->
<script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    var pabrik = new Vue({
        el:'#supplier',
        data:{},
        methods:{
            hapus(id){
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        this.lanjutHapus(id)
                    }
                })
            },
            async lanjutHapus(id){
                var url = "{{route('admin-create-supplier')}}";
                const request = {id:id, action:'delete'};
                this.$http.post(url,request).then(function(response) {
                    if (response.data.code === 200) {
                        location.reload();
                    }
                });
            },
        }
    });
</script>
@endpush