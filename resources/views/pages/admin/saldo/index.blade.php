@extends('layouts.app')
@push('css')

@endpush
@section('content')
<div class="page-content" id="supplier">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-12">
                    <div class="page-title">
                        <h4>Saldo Akhir Pabrik</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Saldo Akhir Pabrik</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                        @foreach($datas as $key => $data)
                            <div class="table-responsive">
                                <table class="table mb-0 table-primarys">
                                    <thead>
                                        <tr colspan="2">
                                            <th>Pabrik {{$data->nama_pabrik}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Material :</td>
                                            <td>Saldo Akhir :</td>
                                        </tr>
                                        @foreach($data->saldoAkhir as $key => $akhir)
                                        <tr>
                                            <td>* {{$akhir->material->material}}</td>
                                            <td>{{number_format($akhir->balance,0,',','.')}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->

        </div>


    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14"></script> -->
<script src="https://cdn.jsdelivr.net/vue.resource/1.0.3/vue-resource.min.js"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    var pabrik = new Vue({
        el:'#supplier',
        data:{},
        methods:{
            hapus(id){
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                    if (result.isConfirmed) {
                        this.lanjutHapus(id)
                    }
                })
            },
            async lanjutHapus(id){
                var url = "{{route('admin-create-supplier')}}";
                const request = {id:id, action:'delete'};
                this.$http.post(url,request).then(function(response) {
                    if (response.data.code === 200) {
                        location.reload();
                    }
                });
            },
        }
    });
</script>
@endpush