@extends('layouts.app')
@push('css')
<link rel="stylesheet" href="{{asset('assets/libs/twitter-bootstrap-wizard/prettify.css')}}">
<link href="{{asset('assets/libs/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
<style>
    .rupiah{
        text-align:left !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">

    <!-- start page title -->
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h4>Create Product</h4>
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Edit Product</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page title -->


    <div class="container-fluid">
        <div class="page-content-wrapper" id="form-store">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card">
                        <div class="card-body"><br>
                            <form action="{{route('admin-edit-product',$product->id)}}" method="post" enctype="multipart/form-data" id="create">
                                @csrf
                                <div class="row">
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Kategori *</label>
                                        <div class="col-sm-10">
                                            <select name="category_id" class="form-control @error('category_id') parsley-error @enderror">
                                            <option value="">Selecte Category</option>
                                            @foreach($categories as $category)    
                                                <option value="{{$category->id}}" @if($product->category_id == $category->id) selected @endif>{{$category->category}}</option>
                                                @endforeach
                                            </select>
                                            @error('product_name')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Product
                                            Name *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('product_name') parsley-error @enderror"
                                                type="text" name="product_name"
                                                placeholder="Enter Product Name" value="{{$product->product_name}}" id="example-text-input">
                                            @error('product_name')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Harga Distributor *</label>
                                        <div class="col-sm-10">
                                            <input  name="harga_distributor" value="{{$product->harga_distributor}}" class="form-control input-mask text-left @error('harga_distributor') parsley-error @enderror rupiah">
                                            @error('harga_distributor')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Harga Agen *</label>
                                        <div class="col-sm-10">
                                        <input name="harga_agen" value="{{$product->harga_agen}}" class="form-control input-mask text-left @error('harga_agen') parsley-error @enderror rupiah">
                                            @error('harga_agen')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Harga End User *</label>
                                        <div class="col-sm-10">
                                        <input name="harga_end_user" value="{{$product->harga_end_user}}" class="form-control input-mask text-left @error('harga_end_user') parsley-error @enderror rupiah">
                                            @error('harga_end_user')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-text-input" class="col-sm-2 col-form-label">Stok *</label>
                                        <div class="col-sm-10">
                                            <input class="form-control @error('stok') parsley-error @enderror"
                                                type="text" name="stok"
                                                placeholder="Enter Stok" value="{{$product->stok}}" id="example-text-input">
                                            @error('stok')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-search-input"
                                            class="col-sm-2 col-form-label">Description *</label>
                                        <div class="col-sm-10">
                                            <textarea name="description"
                                                class="form-control @error('description') parsley-error @enderror" cols="30"
                                                rows="4" placeholder="Enter Description">{{$product->description}}</textarea>
                                            @error('description')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="example-email-input" class="col-sm-2 col-form-label">Product Image</label>
                                        <div class="col-sm-10">
                                        <img src="{{$product->url}}" alt="" width="150px"><br>
                                            <input class="form-control @error('product_image') parsley-error @enderror"
                                                type="file" accept="image/*" name="product_image" value="{{old('product_image')}}">
                                            @error('product_image')
                                            <ul class="parsley-errors-list filled" role="alert">
                                                <li class="parsley-required">{{ $message }}</li>
                                            </ul>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="update">
                                <div class="mb-0">
                                    <div>
                                        <button type="submit" class="btn btn-primary waves-effect waves-light me-1">
                                            Update
                                        </button>
                                        <button type="reset" class="btn btn-secondary waves-effect">
                                            Reset
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
            <!-- end row -->

        </div>
    </div> <!-- container-fluid -->
</div>
@endsection
@push('js')
<!-- form wizard init -->
<script src="{{asset('assets/libs/select2/js/select2.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-advanced.init.js')}}"></script>

<script src="{{asset('assets/libs/inputmask/jquery.inputmask.min.js')}}"></script>
<script src="{{asset('assets/js/pages/form-mask.init.js')}}"></script>
@endpush