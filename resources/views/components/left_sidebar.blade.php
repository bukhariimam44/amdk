<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div class="user-sidebar text-center">
            <div class="dropdown">
                <div class="user-img">
                    <img src="{{asset('img/KS212.png')}}" alt="">
                    <span class="avatar-online bg-success"></span>
                </div>
                <div class="user-info">
                    <h5 class="mt-3 font-size-16 text-white">{{ ucfirst(Auth::user()->nama_depan) }}</h5>
                    <span class="font-size-13 text-white-50">{{ Auth::user()->roleId->role }}</span>
                </div>
            </div>
        </div>
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Dashboard</li>
                @if(Auth::check() && Auth::user()->user_role_id == 1)
                <li>
                    <a href="{{route('home')}}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{setMmActive(['admin-detail-user'])}}">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-user-group"></i>
                        <span>User</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('admin-data-user-admin')}}">Admin</a></li>
                        <li><a href="{{route('admin-data-user-principle')}}">Principle</a></li>
                        <li><a href="{{route('admin-data-user-distributor')}}">Distributor</a></li>
                        <li><a href="{{route('admin-data-user-agen')}}">Agen</a></li>
                        <li><a href="{{route('admin-data-user-end-user')}}">End User</a></li>
                    </ul>
                </li>
                <li class="menu-title">Catalog</li>
                <li>
                    <a href="{{route('admin-category-product')}}" class="waves-effect">
                        <i class="dripicons-tags"></i>
                        <span>Category Product</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('admin-product')}}" class="waves-effect">
                        <i class="dripicons-jewel"></i>
                        <span>Product</span>
                    </a>
                </li>
                <li class="menu-title">Order</li>
                <li class="">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-view-list"></i>
                        <span>Data Order</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('admin-orderan-distributor')}}">Distributor</a></li>
                        <li><a href="{{route('admin-orderan-agen')}}">Agen</a></li>
                        <li><a href="{{route('admin-orderan-enduser')}}">End User</a></li>
                    </ul>
                </li>
                <!-- <li class="{{setMmActive(['admin-data-pre-order'])}}">
                    <a href="{{route('admin-data-pre-order')}}" class="waves-effect">
                        <i class="dripicons-view-list"></i>
                        <span>Data PreOrder</span>
                    </a>
                </li> -->
                <li class="{{setMmActive(['agen-akumulasi'])}}">
                    <a href="{{route('agen-akumulasi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Akumulasi</span>
                    </a>
                </li>
                <!-- <li class="{{setMmActive(['admin-detail-data-order'])}}">
                    <a href="{{route('admin-data-order')}}" class="waves-effect">
                        <i class="dripicons-view-list"></i>
                        <span>Report Order</span>
                    </a>
                </li> -->
                <li class="menu-title">Pabrik</li>
                <li class="{{setMmActive(['admin-pabrik','admin-form-pabrik'])}}">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-store"></i>
                        <span>Monitor Pabrik</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a class="{{setMmActive(['admin-pabrik','admin-form-pabrik'])}}" href="{{route('admin-pabrik')}}">Data Pabrik</a></li>
                        <li><a href="{{route('admin-supplier')}}">Data Supplier</a></li>
                        <li><a href="{{route('admin-material')}}">Data Material</a></li>
                        <li><a href="{{route('admin-material-masuk')}}">Material Masuk</a></li>
                        <li><a href="{{route('admin-material-keluar')}}">Material Keluar</a></li>
                        <li><a href="{{route('admin-mutasi-saldo')}}">Mutasi Saldo</a></li>
                        <li><a href="{{route('admin-saldo-pabrik')}}">Data Saldo Pabrik</a></li>
                        <!-- <li><a href="{{route('admin-data-order-material')}}">Data Order Material</a></li> -->
                    </ul>
                </li>
                
                <li class="menu-title">Settings</li>
                <!-- <li class="">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-gear"></i>
                        <span>Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="#">Provinsi</a></li>
                        <li><a href="#">Kabupaten/Kota</a></li>
                        <li><a href="#">Kecamatan</a></li>
                        <li><a href="#">Kelurahan/Desa</a></li>
                    </ul>
                </li> -->
                <li>
                    <a href="{{route('admin-principle-akses')}}" class="waves-effect">
                        <i class="dripicons-gear"></i>
                        <span>Principle Akses</span>
                    </a>
                </li>
                @elseif(Auth::check() && Auth::user()->user_role_id == 2)
                <li>
                    <a href="{{route('home')}}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{setMmActive(['principle-detail-user'])}}">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-user-group"></i>
                        <span>User</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('principle-data-user-principle')}}">Principle</a></li>
                        <li><a href="{{route('principle-data-user-distributor')}}">Distributor</a></li>
                        <li><a href="{{route('principle-data-user-agen')}}">Agen</a></li>
                        <li><a href="{{route('principle-data-user-end-user')}}">End User</a></li>
                    </ul>
                </li>
                <li class="menu-title">Catalog</li>
                <li>
                    <a href="{{route('principle-category-product')}}" class="waves-effect">
                        <i class="dripicons-tags"></i>
                        <span>Category Product</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('principle-product')}}" class="waves-effect">
                        <i class="dripicons-jewel"></i>
                        <span>Product</span>
                    </a>
                </li>
                <li class="menu-title">Orderan Distributor</li>
                <li class="{{setMmActive(['principle-request-order','principle-detail-request-order'])}}">
                    <a href="{{route('principle-request-order')}}" class="waves-effect">
                        <i class="dripicons-weight"></i> @if(count(App\Models\Transaction::where('upline_id',auth()->user()->id)->where('order_status_id',1)->get()) > 0)<span class="badge rounded-pill bg-info float-end">{{count(App\Models\Transaction::where('upline_id',auth()->user()->id)->where('order_status_id',1)->get())}}</span>@endif
                        <span>Request Order</span>
                    </a>
                </li>
                <li class="{{setMmActive(['principle-detail-status-order'])}}">
                    <a href="{{route('principle-status-order')}}" class="waves-effect">
                        <i class="dripicons-question"></i>
                        <span>Status Order</span>
                    </a>
                </li>
                <!-- <li class="menu-title">Report</li>
                <li class="{{setMmActive(['principle-detail-data-order'])}}">
                    <a href="{{route('principle-data-order')}}" class="waves-effect">
                        <i class="dripicons-view-list"></i>
                        <span>Report Order</span>
                    </a>
                </li> -->
                <li class="menu-title">Order</li>
                <li class="">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-view-list"></i>
                        <span>Data Order</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('principle-orderan-distributor')}}">Distributor</a></li>
                        <li><a href="{{route('principle-orderan-agen')}}">Agen</a></li>
                        <li><a href="{{route('principle-orderan-enduser')}}">End User</a></li>
                    </ul>
                </li>
                <li class="{{setMmActive(['agen-akumulasi'])}}">
                    <a href="{{route('agen-akumulasi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Akumulasi</span>
                    </a>
                </li>
                
                <li class="menu-title">Monitor Pabrik </li>
                <li class="">
                    <a href="{{route('admin-material-masuk')}}" class="waves-effect">
                        <i class="dripicons-store"></i>
                        <span>Saldo Masuk</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin-material-keluar')}}" class="waves-effect">
                        <i class="dripicons-store"></i>
                        <span>Saldo Keluar</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin-mutasi-saldo')}}" class="waves-effect">
                        <i class="dripicons-store"></i>
                        <span>Mutasi Saldo</span>
                    </a>
                </li>
                <li class="">
                    <a href="{{route('admin-saldo-pabrik')}}" class="waves-effect">
                        <i class="dripicons-store"></i>
                        <span>Saldo Pabrik</span>
                    </a>
                </li>
                @elseif(Auth::check() && Auth::user()->user_role_id == 3)
                <li>
                    <a href="{{route('home')}}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{setMmActive(['distributor-detail-agen'])}}">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-user"></i>
                        <span>User</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="{{setMmActive(['distributor-detail-agen'])}}"><a href="{{route('distributor-data-agen')}}">Agen</a></li>
                        <li class="{{setMmActive(['distributor-detail-agen'])}}"><a href="{{route('distributor-data-end-user')}}">End User</a></li>
                    </ul>
                </li>
                <li class="menu-title">Transaksi</li>
                <li class="{{setMmActive(['distributor-detail-product'])}}">
                    <a href="{{route('distributor-product')}}" class="waves-effect">
                        <i class="dripicons-jewel"></i>
                        <span>Product</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('distributor-keranjang')}}" class="waves-effect">
                        <i class="dripicons-cart"></i>@if(count(App\Models\Cart::where('user_id',auth()->user()->id)->get()) > 0)<span class="badge rounded-pill bg-info float-end">{{count(App\Models\Cart::where('user_id',auth()->user()->id)->get())}}</span>@endif
                        <span>Keranjang</span>
                    </a>
                </li>
                <li class="{{setMmActive(['detail-status-transaksi','distributor-detail-status-transaksi'])}}">
                    <a href="{{route('distributor-status-transaksi')}}" class="waves-effect">
                        <i class="dripicons-question"></i>
                        <span>Status Transaksi</span>
                    </a>
                </li>
                <li class="{{setMmActive(['distributor-report-transaksi','distributor-detail-report-transaksi'])}}">
                    <a href="{{route('distributor-report-transaksi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Report Transaksi</span>
                    </a>
                </li>
                <li class="menu-title">Orderan Agen</li>
                <li class="{{setMmActive(['distributor-request-order','distributor-detail-request-order'])}}">
                    <a href="{{route('distributor-request-order')}}" class="waves-effect">
                        <i class="dripicons-weight"></i> @if(count(App\Models\Transaction::where('upline_id',auth()->user()->id)->where('order_status_id',1)->get()) > 0)<span class="badge rounded-pill bg-info float-end">{{count(App\Models\Transaction::where('upline_id',auth()->user()->id)->where('order_status_id',1)->get())}}</span>@endif
                        <span>Request Order</span>
                    </a>
                </li>
                <li class="{{setMmActive(['distributor-detail-status-order'])}}">
                    <a href="{{route('distributor-status-order')}}" class="waves-effect">
                        <i class="dripicons-question"></i>
                        <span>Status Order</span>
                    </a>
                </li>
                {{--<li class="{{setMmActive(['distributor-report-order'])}}">
                    <a href="{{route('distributor-report-order')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Report Order</span>
                    </a>
                </li>--}}
                <!-- <li class="menu-title">Reporting</li> -->
                <li class="menu-title">Order</li>
                <li class="">
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-view-list"></i>
                        <span>Data Order</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="{{route('distributor-orderan-agen')}}">Agen</a></li>
                        <li><a href="{{route('distributor-orderan-enduser')}}">End User</a></li>
                    </ul>
                </li>
                <li class="{{setMmActive(['agen-akumulasi'])}}">
                    <a href="{{route('agen-akumulasi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Akumulasi</span>
                    </a>
                </li>
                @elseif(Auth::check() && Auth::user()->user_role_id == 4)
                <li>
                    <a href="{{route('home')}}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-detail-end-user'])}}">
                    <a href="{{route('agen-data-end-user')}}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span>End User</span>
                    </a>
                </li>
                <li class="menu-title">Transaction</li>
                <li class="{{setMmActive(['agen-detail-product'])}}">
                    <a href="{{route('agen-product')}}" class="waves-effect">
                        <i class="dripicons-jewel"></i>
                        <span>Product</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-keranjang'])}}">
                    <a href="{{route('agen-keranjang')}}" class="waves-effect">
                        <i class="dripicons-cart"></i>@if(count(App\Models\Cart::where('user_id',auth()->user()->id)->get()) > 0)<span class="badge rounded-pill bg-info float-end">{{count(App\Models\Cart::where('user_id',auth()->user()->id)->get())}}</span>@endif
                        <span>Keranjang</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-detail-status-transaksi','agen-detail-status-transaksi'])}}">
                    <a href="{{route('agen-status-transaksi')}}" class="waves-effect">
                        <i class="dripicons-question"></i>
                        <span>Status Transaksi</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-report-transaksi','agen-detail-report-transaksi'])}}">
                    <a href="{{route('agen-report-transaksi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Report Transaksi</span>
                    </a>
                </li>
                <li class="menu-title">Orderan End User</li>
                <li class="{{setMmActive(['agen-request-order','agen-detail-request-order'])}}">
                    <a href="{{route('agen-request-order')}}" class="waves-effect">
                        <i class="dripicons-weight"></i>@if(count(App\Models\Transaction::where('upline_id',auth()->user()->id)->where('order_status_id',1)->get()) > 0)<span class="badge rounded-pill bg-info float-end">{{count(App\Models\Transaction::where('upline_id',auth()->user()->id)->where('order_status_id',1)->get())}}</span>@endif
                        <span>Request Order</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-detail-status-order','agen-status-order'])}}">
                    <a href="{{route('agen-status-order')}}" class="waves-effect">
                        <i class="dripicons-question"></i>
                        <span>Status Order</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-report-order'])}}">
                    <a href="{{route('agen-report-order')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Report Order</span>
                    </a>
                </li>
                <li class="{{setMmActive(['agen-akumulasi'])}}">
                    <a href="{{route('agen-akumulasi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Akumulasi</span>
                    </a>
                </li>
                @elseif(Auth::check() && Auth::user()->user_role_id == 5)
                <!-- <li>
                    <a href="{{route('end-user-home')}}" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="menu-title">Transaction</li> -->
                <li class="{{setMmActive(['end-user-detail-product','end-user-home'])}}">
                    <a href="{{route('end-user-product')}}" class="waves-effect">
                        <i class="dripicons-jewel"></i>
                        <span>Product</span>
                    </a>
                </li>
                <li>
                    <a href="{{route('end-user-keranjang')}}" class="waves-effect">
                        <i class="dripicons-cart"></i>@if(count(App\Models\Cart::where('user_id',auth()->user()->id)->get()) > 0)<span class="badge rounded-pill bg-info float-end">{{count(App\Models\Cart::where('user_id',auth()->user()->id)->get())}}</span>@endif
                        <span>Keranjang</span>
                    </a>
                </li>
                <li class="{{setMmActive(['end-user-status-transaksi','end-user-detail-status-transaksi','end-user-detail-status-transaksi'])}}">
                    <a href="{{route('end-user-status-transaksi')}}" class="waves-effect">
                        <i class="dripicons-question"></i>
                        <span>Status Transaksi</span>
                    </a>
                </li>
                <li class="{{setMmActive(['end-user-report-transaksi','end-user-detail-report-transaksi'])}}">
                    <a href="{{route('end-user-report-transaksi')}}" class="waves-effect">
                    <i class="dripicons-view-list"></i>
                        <span>Report Transaksi</span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>