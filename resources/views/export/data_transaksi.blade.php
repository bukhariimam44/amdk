<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                <th>No. Order</th>
                <th>Tanggal Order</th>
                <th>Total Item</th>
                <th>Total Qty</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th>{{$user}}</th>
            </tr>
        </thead>
        <tbody>
            @php
            $tot_item = 0;
            $totqty = 0;
            $tot_harga = 0;
            @endphp
            @foreach($datas as $key => $data)
            @php
            $tot_item+=(int)(count($data->item));
            $totqty+=(int)($data->item->sum('qty'));
            $tot_harga+=(int)($data->harga_total);
            @endphp
            <tr>
                <td>{{$data->no_order}}</td>
                <td>{{$data['transaction_date']}}</td>
                <td>{{count($data->item)}}</td>
                <td>{{$data->item->sum('qty')}}</td>
                <td>{{$data->harga_total}}</td>
                <td>
                    <div
                        class="badge @if($data->order_status_id == 2) badge-soft-primary @else badge-soft-danger @endif font-size-14">
                        {{$data->status->status}}</div>
                </td>
                <td>{{$data->user->nama_depan}}</td>
            </tr>
            @endforeach
            @if(count($datas) > 0)
            <tr>
                <td colspan="2" class="text-center">Total</td>
                <td class="text-center">{{$tot_item}}</td>
                <td class="text-center">{{$totqty}}</td>
                <td class="text-center">{{$tot_harga}}</td>
                <td colspan="2"></td>
            </tr>
            @endif
        </tbody>
    </table>
</body>

</html>