<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <table id="tech-companies-1" class="table table-striped">
        <thead>
            <tr>
                <th>No. Order</th>
                <th>Tanggal Order</th>
                <th>Nama Product</th>
                <th>Harga</th>
                <th>Qty</th>
                <th>Total Harga</th>
                <th>Status</th>
                <th>@if(Auth::user()->user_role_id == 2) Distributor @elseif(Auth::user()->user_role_id == 3) Agen
                    @elseif(Auth::user()->user_role_id == 4) End User @endif</th>
            </tr>
        </thead>
        <tbody>
            @php
            $totqty = 0;
            $tot_harga = 0;
            $total_harga = 0;
            @endphp
            @foreach($datas as $key => $data)
            @foreach($data->item as $dt)
            @php
            $totqty+=(int)($dt['qty']) ;
            $tot_harga+=(int)($dt->harga);
            $total_harga+=(int)($dt->harga*$dt['qty']);
            @endphp
            <tr>
                <td>{{$data['no_order']}}</td>
                <td>{{$data['transaction_date']}}</td>
                <td>{{$dt['product_name']}}</td>
                <td>{{$dt->harga}}</td>
                <td>{{$dt['qty']}}</td>
                <td>{{$dt->harga * $dt['qty']}}</td>
                <td>
                    <div
                        class="badge @if($data->order_status_id == 2) badge-soft-primary @else badge-soft-danger @endif font-size-14">
                        {{$data->status->status}}</div>
                </td>
                <td>{{$data->user->nama_depan}}</td>
            </tr>
            @endforeach
            @endforeach
            @if(count($datas) > 0)
            <tr>
                <td colspan="3" class="text-center">Total</td>
                <td class="text-center">{{$tot_harga}}</td>
                <td class="text-center">{{$totqty}}</td>
                <td class="text-center">{{$total_harga}}</td>
                <td colspan="2"></td>
            </tr>
            @endif
        </tbody>
    </table>
</body>

</html>