@extends('layouts.auth')
@push('css')
<meta name="_token" id="token" value="{{csrf_token()}}">
@endpush
@section('content')
<div class="home-center" id="register">
    <div class="home-desc-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-5 mt-2">
                    <div class="card">
                        <div class="card-body">
                            <div class="px-2 py-3">
                                <div class="text-center">
                                    <h2 class="text-primary mb-2 mt-4">DAFTAR</h2>
                                    <p class="text-muted">Gratis daftar sebagai End User di {{config('app.name')}}.</p>
                                </div>


                                <form class="form-horizontal mt-4 pt-2" action="{{route('register')}}" method="post">
                                    @csrf
                                    <div class="mb-3">
                                        <label for="name">Nama Lengkap</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi mdi-account-tie"></i></span>
                                            <input type="text" class="form-control" :class="msg.name ? 'is-invalid':''"
                                                id="name" v-model="name" placeholder="Enter Full Name">
                                            <span :class="msg.name ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.name }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="nik">NIK</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi mdi-account-tie"></i></span>
                                            <input type="text" class="form-control" :class="msg.nik ? 'is-invalid':''"
                                                id="nik" v-model="nik" placeholder="NIK">
                                            <span :class="msg.nik ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.nik }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    
                                    <div class="mb-3">
                                        <label for="username">Phone</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi mdi-cellphone"></i></span>
                                            <input type="text" class="form-control" :class="msg.phone ? 'is-invalid':''"
                                                id="phone" v-model="phone" placeholder="Enter Phone">
                                            <span :class="msg.phone ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.phone }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="rt">RT</label>
                                                <div class="input-group" id="datepicker1">
                                                    <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                                    <input type="text" class="form-control"
                                                        :class="msg.rt ? 'is-invalid':''" id="rt" v-model="rt"
                                                        placeholder="RT">
                                                    <span :class="msg.rt ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.rt }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="rw">RW</label>
                                                <div class="input-group" id="datepicker1">
                                                    <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                                    <input type="text" class="form-control"
                                                        :class="msg.rw ? 'is-invalid':''" id="rw" v-model="rw"
                                                        placeholder="RW">
                                                    <span :class="msg.rw ? 'invalid-feedback':''" role="alert">
                                                        <strong>@{{ msg.rw }}</strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    
                                    
                                    <div class="mb-3">
                                        <label for="rw">Alamat</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                            <textarea class="form-control"
                                                :class="msg.alamat ? 'is-invalid':''" id="alamat" v-model="alamat"
                                                placeholder="Alamat" id="" cols="30" rows="4"></textarea>
                                            <span :class="msg.alamat ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.alamat }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="provinsi">Provinsi</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                            <select class="form-control" :class="msg.provinsi ? 'is-invalid':''" id="provinsi" v-model="provinsi" @change="getKabupaten()">
                                                <option value="">PILIH PROVINSI</option>
                                                <option v-for="p in provinsis" :value="p.id">@{{p.name}}</option>
                                            </select>
                                            <span :class="msg.provinsi ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.provinsi }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="kabupaten">Kota/Kabupaten</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                            <select class="form-control" :class="msg.kabupaten ? 'is-invalid':''" id="kabupaten" v-model="kabupaten" @change="getKecamatan()">
                                                <option value="">PILIH KOTA/KABUPATEN</option>
                                                <option v-for="k in kabupatens" :value="k.id">@{{k.name}}</option>
                                            </select>
                                            <span :class="msg.kabupaten ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.kabupaten }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="kecamatan">Kecamatan</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                            <select class="form-control" :class="msg.kecamatan ? 'is-invalid':''" id="provinsi" v-model="kecamatan" @change="getKelurahan()">
                                                <option value="">PILIH KECAMATAN</option>
                                                <option v-for="k in kecamatans" :value="k.id">@{{k.name}}</option>
                                            </select>
                                            <span :class="msg.kecamatan ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.kecamatan }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="desa">Desa/Kelurahan</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                            <select class="form-control" :class="msg.desa ? 'is-invalid':''" id="desa" v-model="desa">
                                                <option value="">PILIH DESA/KELURAHAN</option>
                                                <option v-for="k in desas" :value="k.id">@{{k.name}}</option>
                                            </select>
                                            <span :class="msg.desa ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.desa }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="kode_pos">Kode POS</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi dripicons-location"></i></span>
                                            <input type="text" class="form-control"
                                                :class="msg.kode_pos ? 'is-invalid':''" id="kode_pos" v-model="kode_pos"
                                                placeholder="Kode POS">
                                            <span :class="msg.kode_pos ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.kode_pos }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="email">Email</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi mdi-email"></i></span>
                                            <input type="email" class="form-control"
                                                :class="msg.email ? 'is-invalid':''" id="email" v-model="email"
                                                placeholder="Enter email">
                                            <span :class="msg.email ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.email }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="userpassword">Password</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi mdi-lock"></i></span>
                                            <input :type="passwordFieldType" class="form-control"
                                                :class="msg.password ? 'is-invalid':''" id="userpassword"
                                                v-model="password" placeholder="Enter password">
                                            <span @click="switchVisibility()" class="input-group-text"><i class="mdi"
                                                    :class="passwordFieldType === 'password'? 'mdi-eye':'mdi-eye-off'"></i></span>
                                            <span :class="msg.password ? 'invalid-feedback':''" role="alert">
                                                <strong>@{{ msg.password }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="userpassword">Ulangi Password</label>
                                        <div class="input-group" id="datepicker1">
                                            <span class="input-group-text"><i class="mdi mdi-lock"></i></span>
                                            <input :type="passwordFieldType" class="form-control"
                                                :class="msg.password_confirmation ? 'is-invalid':''" id="userpassword"
                                                v-model="password_confirmation"
                                                placeholder="Enter password Confirmation">
                                            <span :class="msg.password_confirmation ? 'invalid-feedback':''"
                                                role="alert">
                                                <strong>@{{ msg.password_confirmation }}</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div>
                                        <button class="btn btn-primary w-100 waves-effect waves-light" type="button"
                                            @click="daftar()">DAFTAR</button>
                                    </div>
                                    <div class="mt-4 text-center">
                                        <p class="mb-0">Dengan mendaftar, Anda menyetujui <a href="#" class="text-primary">Ketentuan Penggunaan</a> {{config('app.name')}}</p>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>

                    <div class="mt-5 text-center text-white">
                        <p>Sudah memiliki akun ? <a href="{{route('login')}}" class="fw-bold text-white"> Login </a>
                        </p>
                        <p>© 2021 {{config('app.name')}}.
                        </p>
                    </div>
                </div>
            </div>

        </div>


    </div>
    <!-- End Log In page -->
</div>
@endsection
@push('js')
<script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.5.3"></script>
<script>
    Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');
    var register = new Vue({
        el: "#register",
        data: {
            name: '',
            nik: '',
            rt: '',
            rw: '',
            alamat: '',
            provinsi: '',
            provinsis: [],
            kabupaten: '',
            kabupatens: [],
            kecamatan: '',
            kecamatans: [],
            desa: '',
            desas: [],
            kode_pos: '',
            email: '',
            phone: '',
            password: '',
            password_confirmation: '',
            msg: [],
            passwordFieldType: 'password'
        },
        mounted(){
            this.getProvinsi();
        },
        watch: {
            email(value) {
                this.email = value;
                this.validateEmail(value);
            },
            phone(value) {
                this.phone = value;
                this.validatePhone(value);
            },
            password(value) {
                this.password = value;
                this.validatePassword(value);
            },
            password_confirmation(value) {
                this.password_confirmation = value;
                this.validatePasswordConf(value);
            },
        },
        methods: {
            getProvinsi(){
                const url = "<?php echo route('provinsi');?>";
                this.$http.get(url).then(function (response) {
                    this.provinsis = response.data
                });
            },
            getKabupaten(){
                const url = "<?php echo route('kabupaten');?>";
                const request = {propinsi_id:this.provinsi};
                this.kabupaten = ''
                this.$http.post(url,request).then(function (response) {
                    this.kabupatens = response.data
                });
            },
            getKecamatan(){
                const url = "<?php echo route('kecamatan');?>";
                const request = {kabupaten_id:this.kabupaten};
                this.kecamatan = ''
                this.$http.post(url,request).then(function (response) {
                    this.kecamatans = response.data
                });
            },
            getKelurahan(){
                const url = "<?php echo route('kelurahan');?>";
                const request = {kecamatan_id:this.kecamatan};
                this.desa = ''
                this.$http.post(url,request).then(function (response) {
                    this.desas = response.data
                });
            },
            validateEmail(value) {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
                    this.msg['email'] = '';
                } else {
                    this.msg['email'] = 'Invalid Email Address';
                }
            },
            validatePhone(value) {
                if (value.length < 10) {
                    this.msg['phone'] = 'Min 10 characters!';
                } else if (value.length > 13) {
                    this.msg['phone'] = 'Max 13 characters!';
                } else {
                    this.msg['phone'] = '';
                }
            },
            validatePassword(value) {
                let difference = 8 - value.length;
                if (value.length < 8) {
                    this.msg['password'] = 'Must be 8 characters! ' + difference + ' characters left';
                } else {
                    this.msg['password'] = '';
                }
            },
            validatePasswordConf(value) {
                let difference = 8 - value.length;
                if (value.length < 8) {
                    this.msg['password_confirmation'] = 'Must be 8 characters! ' + difference +
                        ' characters left';
                } else {
                    this.msg['password_confirmation'] = '';
                }
            },
            switchVisibility() {
                this.passwordFieldType = this.passwordFieldType === "password" ? "text" : "password";
            },
            async daftar() {
                const url = "<?php echo route('daftar');?>";
                const request = {
                    name: this.name,
                    nik: this.nik,
                    rt: this.rt,
                    rw: this.rw,
                    alamat: this.alamat,
                    provinsi: this.provinsi,
                    kabupaten: this.kabupaten,
                    kecamatan: this.kecamatan,
                    desa: this.desa,
                    kode_pos: this.kode_pos,
                    email: this.email,
                    phone: this.phone,
                    password: this.password,
                    password_confirmation: this.password_confirmation
                };
                this.$http.post(url, request).then(function (response) {
                    if (response.data.code === 401) {
                        this.msg = response.data.error
                    } else if (response.data.code === 200) {
                        Swal.fire({
                            icon: response.data.icon,
                            title: response.data.title,
                            text: response.data.message,
                            showConfirmButton: false,
                        })
                        setTimeout(() => {
                            location.replace("{{route('login')}}")
                        }, 1000);
                    } else {
                        Swal.fire({
                            icon: response.data.icon,
                            title: response.data.title,
                            text: response.data.message,
                            showConfirmButton: true,
                        })
                    }

                });
            }
        }
    })
</script>
@endpush
