@extends('layouts.auth')
@push('css')
@endpush
@section('content')
<div class="home-center">
        <div class="home-desc-center">

            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card">
                            <div class="card-body">
                                <div class="px-2 py-3">
                                

                                    <div class="text-center">
                                        <a href="#">
                                            <img src="{{asset('img/amdk212.jpg')}}" width="30%" alt="logo">
                                        </a>

                                        
                                        <p class="text-muted">Sign in to continue to {{config('app.name')}}.</p>
                                    </div>


                                    <form class="form-horizontal mt-4 pt-2" action="{{route('login')}}" method="post">
                                        @csrf
                                        <div class="mb-3">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                placeholder="Enter Email">
                                        </div>

                                        <div class="mb-3">
                                            <label for="userpassword">Password</label>
                                            <input type="password" class="form-control" id="userpassword" name="password"
                                                placeholder="Enter password">
                                        </div>

                                        <div class="mb-3">
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input"
                                                    id="customControlInline">
                                                <label class="form-label" name="remember" value="on" for="customControlInline">Tetap login</label>
                                            </div>
                                        </div>

                                        <div>
                                            <button class="btn btn-primary w-100 waves-effect waves-light"
                                                type="submit">Log In</button>
                                        </div>

                                        <div class="mt-4 text-center">
                                            <a href="#" class="text-muted"><i
                                                    class="mdi mdi-lock me-1"></i> Lupa kata sandi?</a>
                                        </div>


                                    </form>


                                </div>
                            </div>
                        </div>

                        <div class="mt-5 text-center text-white">
                            <p>Ingin menjadi EndUser ? <a href="{{route('register')}}" class="fw-bold text-white">
                                    Daftar Sekarang</a> </p>
                            <p>© <script>
                                    document.write(new Date().getFullYear())
                                </script> {{config('app.name')}}. Crafted with <i class="mdi mdi-heart text-danger"></i> by 212
                            </p>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <!-- End Log In page -->
    </div>
@endsection
@push('js')


@endpush