<?php
namespace App\Helpers\Orders;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\Transaction;
use DB;
use Log;

class ProsesBayar
{
    private $no_order;
    private $user_id;
    public $response;

    public function __construct($attributes)
    {
        $this->no_order = $attributes['no_order'];
        $this->user_id = $attributes['user_id'];
        $this->prosesBayar($attributes['no_order']);
    }
    public function get()
    {
        return $this->response;
    }
    public function prosesBayar($products)
    {
        DB::beginTransaction();
        try {
            $data = Transaction::where('no_order',decrypt($this->no_order))->where('upline_id',$this->user_id)->first();
            $data->order_status_id = 2;
            $data->update();
        } catch (\Throwable $th) {
            Log::info('Gagagl simpan = '.$th);
            DB::rollBack();
            return $this->response = [
                'status' => [
                    'code' => 400,
                    'confirm' => 'failed',
                    'message' => 'Proses gagal. Silahkan ulangi kembali. ',
                ],
            ];
        }
        DB::commit();
        return $this->response = [
            'status' => [
                'code' => 200,
                'confirm' => 'success',
                'message' => 'Proses gagal. Silahkan ulangi kembali. ',
            ],
        ];
        
    }
}