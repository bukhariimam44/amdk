<?php
namespace App\Helpers\Orders;

class Order
{
    public static function simpan($attributes){
        return new SaveOrder($attributes);
    }
    public static function bayar($attributes){
        return new ProsesBayar($attributes);
    }
}