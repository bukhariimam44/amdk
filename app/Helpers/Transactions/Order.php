<?php
namespace App\Helpers\Transactions;

class Order
{
    public static function po($attributes){
        return new Po($attributes);
    }
    public static function bayar($attributes){
        return new ProsesBayar($attributes);
    }
}