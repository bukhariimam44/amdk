<?php
namespace App\Helpers\Transactions;

use App\Http\Controllers\Api\BaseController as BaseController;
use App\Models\Transaction;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Product;
use DB;
use Log;

class Po
{
    private $no_order;
    private $user_id;
    public $response;

    public function __construct($attributes)
    {
        $this->user_id = $attributes['user_id'];
        $this->upline_id = $attributes['upline_id'];
        $this->user_role_id = $attributes['user_role_id'];
        $this->prosesBayar();
    }
    public function get()
    {
        return $this->response;
    }
    public function prosesBayar()
    {
        DB::beginTransaction();
        try {
            $sum = 0;
            $cart = Cart::where('user_id',$this->user_id)->get();
            foreach ($cart as $key => $value) {
                $product = Product::find($value->product_id);
                if ($this->user_role_id == 5) {
                    $sum+=$product->harga_end_user * $value->jumlah; 
                }elseif ($this->user_role_id == 4) {
                    $sum+=$product->harga_agen * $value->jumlah;
                }elseif ($this->user_role_id == 3) {
                    $sum+=$product->harga_distributor * $value->jumlah;
                }
                           
            }
            $tahun = date('y');
            $sequence = 1000001;
            if ($cek = Transaction::orderBy('id','DESC')->first()) {
                if (substr($cek->no_order,0,-8) == $tahun) {
                    $sequence = (int)($cek->sequence)+1;
                }
            }
            $tr = Transaction::create([
                'sequence'=>$sequence,
                'no_order'=>$tahun.date('m').''.substr($sequence,1),
                'user_id'=>$this->user_id,
                'upline_id'=> $this->upline_id,
                'transaction_date'=>date('Y-m-d H:i:s'),
                'harga_total'=>$sum,
                'order_status_id'=>1
            ]);
            foreach ($cart as $key => $value) {
                $product = Product::find($value->product_id);
                $cart = Cart::where('id',$value->id)->where('user_id',$this->user_id)->first()->delete();
                if ($this->user_role_id == 5) {
                    $harga = $product->harga_end_user; 
                }elseif ($this->user_role_id == 4) {
                    $harga = $product->harga_agen;
                }elseif ($this->user_role_id == 3) {
                    $harga = $product->harga_distributor;
                }
                Item::create([
                    'transaction_id'=>$tr->id,
                    'product_id'=>$product->id,
                    'product_name'=>$product->product_name,
                    'harga'=>$harga,
                    'qty'=>$value->jumlah
                ]);
                
            }
        } catch (\Throwable $th) {
            Log::info('Gagagl simpan = '.$th);
            DB::rollBack();
            return $this->response = [
                'status' => [
                    'code' => 400,
                    'confirm' => 'failed',
                    'message' => 'Proses gagal. Silahkan ulangi kembali. ',
                ],
            ];
        }
        DB::commit();
        return $this->response = [
            'status' => [
                'code' => 200,
                'confirm' => 'success',
                'message' => 'Proses gagal. Silahkan ulangi kembali. ',
            ],
        ];
        
    }
}