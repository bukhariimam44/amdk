<?php 
use App\Models\User;
use App\Models\Akses;

function setActive($uri, $output = "active")
{
  if (is_array($uri)) {
    foreach ($uri as $u) {
      if (Route::is($u)) {
        return $output;
      }
    }
  } else {
    if (Route::is($uri)) {
      return $output;
    }
  }
}

function setMmActive($uri, $output = "mm-active")
{
  if (is_array($uri)) {
    foreach ($uri as $u) {
      if (Route::is($u)) {
        return $output;
      }
    }
  } else {
    if (Route::is($uri)) {
      return $output;
    }
  }
}

function setMmCollapse($uri, $output = "mm-collapse")
{
  if (is_array($uri)) {
    foreach ($uri as $u) {
      if (Route::is($u)) {
        return $output;
      }
    }
  } else {
    if (Route::is($uri)) {
      return $output;
    }
  }
}

function setMmShow($uri, $output = "mm-show")
{
  if (is_array($uri)) {
    foreach ($uri as $u) {
      if (Route::is($u)) {
        return $output;
      }
    }
  } else {
    if (Route::is($uri)) {
      return $output;
    }
  }
}

function setAriaExpanded($uri, $output = true)
{
  if (is_array($uri)) {
    foreach ($uri as $u) {
      if (Route::is($u)) {
        return $output;
      }
    }
  } else {
    if (Route::is($uri)) {
      return $output;
    }
  }
}

function generate($text,$linkImage){
  $image = \QrCode::format('png')
					->merge($linkImage, 0.2, true)
          ->backgroundColor(223,228,240)
					->size(400)->errorCorrection('H')
					->generate($text);
 return $image;
}

 function imageUpload($image, $namePrefix, $destination)
{
   list($type, $file) = explode(';', $image);
   list(, $extension) = explode('/', $type);
   list(, $file) = explode(',', $file);
   if (file_exists(base_path('public/qrcode/' . $namePrefix . '.' . $extension))) {
      chmod(base_path('public/qrcode/' . $namePrefix . '.' . $extension), 0777);
      unlink(base_path('public/qrcode/' . $namePrefix . '.' . $extension));
  }
  $fileNameToStore = $namePrefix . '.' . $extension;
  $source = fopen($image, 'r');
  $destination = fopen($destination . $fileNameToStore, 'w');
  stream_copy_to_stream($source, $destination);
  chmod(base_path('public/qrcode/' . $namePrefix . '.' . $extension), 0777);
  fclose($source);
  fclose($destination);
  return $fileNameToStore;
}
function principleAll(){
  $data = User::where('user_role_id',2)->where('open',1)->get();
  return $data;
}
function aksesAll(){
  $data = Akses::get();
  return $data;
}