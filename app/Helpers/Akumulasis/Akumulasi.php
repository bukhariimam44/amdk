<?php
namespace App\Helpers\Akumulasis;

class Akumulasi
{
    public static function downline($attributes,$type="downline"){
        return new Downline($attributes,$type);
    }
    public static function upline($attributes,$type="upline"){
        return new Upline($attributes,$type);
    }
}