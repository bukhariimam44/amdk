<?php
namespace App\Helpers\Mutasis;

class Mutasi
{
    public static function masuk($attributes, $type = 'masuk'){
        return new SaldoMutasi($attributes, $type);
    }
    public static function keluar($attributes, $type = 'keluar'){
        return new SaldoMutasi($attributes, $type);
    }
}