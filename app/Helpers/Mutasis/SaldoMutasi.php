<?php
namespace App\Helpers\Mutasis;

use App\Models\SaldoMasuk;
use App\Models\MutasiSaldo;
use App\Models\SaldoAkhir;
use DB;
use Log;

class SaldoMutasi
{
    private $material;
    private $pabrik;
    private $supplier;
    private $transaction_date;
    private $keterangan;
    private $saldo;
    private $admin_id;
    private $type;
    public $response;

    public function __construct($attributes, $type)
    {
        $this->material = $attributes['material'];
        $this->pabrik = $attributes['pabrik'];
        $this->supplier = $attributes['supplier'];
        $this->transaction_date = $attributes['transaction_date'];
        $this->keterangan = $attributes['keterangan'];
        $this->saldo = $attributes['saldo'];
        $this->admin_id = $attributes['admin_id'];
        $this->type = $type;
        $this->prosesMutasi();
    }
    public function get()
    {
        return $this->response;
    }
    public function prosesMutasi()
    {
        DB::beginTransaction();
        try {
            if ($cek_saldo = SaldoAkhir::where('pabrik_id',$this->pabrik)->where('material_id',$this->material)->first()) {
                $saldo_akhir = $cek_saldo->balance;
                $balance = $this->type == 'masuk' ? (int)($cek_saldo->balance + $this->saldo): (int)($cek_saldo->balance - $this->saldo);
            }else {
                if ($this->type == 'masuk') {
                    $saldo_baru = SaldoAkhir::create([
                        'material_id'=>$this->material,
                        'pabrik_id'=>$this->pabrik,
                        'balance'=>$this->saldo,
                    ]);
                    $saldo_akhir = $saldo_baru->balance;
                    $balance = $saldo_baru->balance;
                }else {
                    DB::rollBack();
                    return $this->response = [
                        'code' => 400,
                        'icon' => 'error',
                        'title'=>'Opps',
                        'message' => 'Proses gagal. Saldo kurang. ',
                    ];
                }
                
            }
            if ($balance < 0) {
                DB::rollBack();
                return $this->response = [
                    'code' => 400,
                    'icon' => 'error',
                    'title'=>'Opps',
                    'message' => 'Proses gagal. Saldo kurang. ',
                ];
            }
            MutasiSaldo::create([
                'material_id'=>$this->material,
                'supplier_id'=>$this->supplier,
                'pabrik_id'=>$this->pabrik,
                'mutasi'=>$this->type,
                'transaction_date'=>$this->transaction_date,
                'admin_id'=>$this->admin_id,
                'keterangan'=>$this->keterangan,
                'saldo'=>$this->saldo,
                'balance'=>$balance
            ]);
            $update_saldo = SaldoAkhir::where('pabrik_id',$this->pabrik)->where('material_id',$this->material)->where('balance',$saldo_akhir)->first()->update([
                'balance'=>$balance
            ]);
            if (!$update_saldo) {
                DB::rollBack();
                return $this->response = [
                    'code' => 400,
                    'icon' => 'error',
                    'title'=>'Opps',
                    'message' => 'Proses gagal. Saldo Salah. ',
                ];
            }
        } catch (\Throwable $th) {
            Log::info('Gagagl simpan = '.$th);
            DB::rollBack();
            return $this->response = [
                'code' => 400,
                'icon' => 'error',
                'title'=>'Opps',
                'message' => 'Silahkan diulangi. ',
            ];
        }
        DB::commit();
        return $this->response = [
            'code' => 200,
            'icon' => 'success',
            'title'=>'OK',
            'message' => 'Proses berhasil.',
        ];
        
    }
}