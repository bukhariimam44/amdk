<?php

namespace App\Exports;

use App\Models\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class TransactionExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct(string $from, string $to,string $status, string $type, string $report, string $role, string $role_user_id)
    {
        $this->from = $from;
        $this->to = $to;
        $this->status = $status;
        $this->type = $type;
        $this->report = $report;
        $this->role = $role;
        $this->role_user_id = $role_user_id;
    }
    public function view(): View
    {
        $datas = [];
        if ($this->role == 'admin') {
            $datas =  Transaction::select('transactions.*')->join('users','users.id','=','transactions.user_id')->where('user_role_id',$this->role_user_id)->whereBetween('transaction_date',[$this->from.' 00:00:00',$this->to.' 23:59:59'])->where('order_status_id','=',$this->status)->get();
        }elseif($this->role == 'mytrx'){
            $datas =  Transaction::select('transactions.*')->join('users','users.id','=','transactions.user_id')->where('transactions.user_id',auth()->user()->id)->whereBetween('transaction_date',[$this->from.' 00:00:00',$this->to.' 23:59:59'])->where('order_status_id','=',$this->status)->get();
        }else {
            $datas =  Transaction::select('transactions.*')->join('users','users.id','=','transactions.user_id')->where('user_role_id',$this->role_user_id)->whereBetween('transaction_date',[$this->from.' 00:00:00',$this->to.' 23:59:59'])->where('order_status_id','=',$this->status)->get();
            // if (auth()->user()->user_role_id == 3 && $this->report == 'agen') {
            //     $datas =  Transaction::select('transactions.*')->join('users','users.id','=','transactions.user_id')->where('users.parent_id',auth()->user()->id)->where('users.user_role_id',$this->role_user_id)->whereBetween('transaction_date',[$this->from.' 00:00:00',$this->to.' 23:59:59'])->where('order_status_id','=',$this->status)->get();
            // }elseif (auth()->user()->user_role_id == 3 && $this->report == 'enduser') {
            //     $banyak = auth()->user()->banyak;
            //     $parent = [];
            //     foreach ($banyak as $key => $value) {
            //         $parent[]= $value->id;
            //     }
            //     $datas =  Transaction::select('transactions.*')->join('users','users.id','=','transactions.user_id')->whereIn('users.parent_id',$parent)->where('users.user_role_id',$this->role_user_id)->whereBetween('transaction_date',[$this->from.' 00:00:00',$this->to.' 23:59:59'])->where('order_status_id','=',$this->status)->get();
            // }elseif (auth()->user()->user_role_id == 4 && $this->report == 'enduser') {
            //     # code...
            // }
        }
        
        if ($this->type == 'item') {
            return view('export.data_item', [
                'user'=>$this->report,
                'datas' => $datas
            ]);
        }elseif ($this->type == 'transaksi') {
            return view('export.data_transaksi', [
                'user'=>$this->report,
                'datas' => $datas
            ]);
        }
        
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $event) {
                $cellRange = 'A1:A8'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
            },
        ];
    }
}
