<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'category',
        'open'
    ];

    public function storeId(){
        return $this->belongsTo('App\Models\Store','store_id');
    }
    public function product(){
        return $this->hasMany('App\Models\Product','category_id');
    }
}
