<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaldoKeluar extends Model
{
    use HasFactory;
    protected $fillable = [
        'material_id',
        'pabrik_id',
        'transaction_date',
        'admin_id',
        'keterangan',
        'saldo',
    ];
}
