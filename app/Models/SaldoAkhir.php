<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaldoAkhir extends Model
{
    use HasFactory;
    protected $fillable = [
        'material_id',
        'pabrik_id',
        'balance',
    ];
    public function material(){
        return $this->belongsTo('App\Models\Material','material_id');
    }
}
