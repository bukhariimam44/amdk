<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaldoMasuk extends Model
{
    use HasFactory;
    protected $fillable = [
        'material_id',
        'pabrik_id',
        'supplier_id',
        'transaction_date',
        'admin_id',
        'keterangan',
        'saldo',
    ];
    public function pabrik(){
        return $this->belongsTo('App\Models\Pabrik','pabrik_id');
    }
    public function supplier(){
        return $this->belongsTo('App\Models\Supplier','supplier_id');
    }
    public function material(){
        return $this->belongsTo('App\Models\Material','material_id');
    }
    public function admin(){
        return $this->belongsTo('App\Models\User','admin_id');
    }
}
