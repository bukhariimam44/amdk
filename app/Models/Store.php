<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Store extends Model
{
    use HasFactory;
    public $appends = ['url','karyawan'];
    protected $fillable = [
        'id',
        'parent_id',
        'store_name',
        'logo',
        'user_id',
        'address',
        'type',
        'admin_id',
        'open'
    ];
    public function getUrlAttribute()
    {
        return Storage::disk('s3')->url($this->logo);
    }
    public function parentStore(){
        return $this->hasMany('App\Models\Store','parent_id');
    }
    public function getKaryawanAttribute()
    {
        return $this->jumlahKaryawan()->count();
    }
    public function jumlahKaryawan(){
        return $this->hasMany('App\Models\User');
    }
    public function QRTable(){
        return $this->hasMany('App\Models\Table');
    }
    public function allCategory(){
        return $this->hasMany('App\Models\CategoryProduct');
    }
    public function MyStore()
    {
        $stores = Store::where('admin_id',auth()->user()->id)->where('open',1)->orderBy('id','ASC')->get();
        $store = [];
        foreach ($stores as $key => $value) {
            $store[]= $value->id;
        }
        return $store;
    }
}
