<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pabrik extends Model
{
    use HasFactory;
    protected $fillable = [
        'nama_pabrik',
        'telp',
        'alamat',
        'admin_id',
        'open'
    ];
    public function saldoAkhir(){
        return $this->hasMany('App\Models\SaldoAkhir','pabrik_id');
    }
}
