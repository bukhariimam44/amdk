<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'parent_id',
        'nik',
        'nama_depan',
        'email',
        'phone',
        'user_role_id',
        'membership_id',
        'rt',
        'rw',
        'alamat',
        'province_id',
        'regency_id',
        'district_id',
        'village_id',
        'kode_pos',
        'password',
        'open',
    ];

    public function getUserRoleAttribute()
    {
        return $this->roleId()->first();
    }
    public function roleId(){
        return $this->belongsTo('App\Models\UserRole','user_role_id');
    }
    public function storeId(){
        return $this->belongsTo('App\Models\Store','store_id');
    }
    public function allStore(){
        return $this->hasMany('App\Models\Store','admin_id');
    }
    public function membership(){
        return $this->belongsTo('App\Models\Membership','membership_id');
    }
    public function parentId(){
        return $this->belongsTo('App\Models\User','parent_id');
    }
    public function provinsi(){
        return $this->belongsTo('App\Models\Province','province_id');
    }
    public function kabupaten(){
        return $this->belongsTo('App\Models\Regency','regency_id');
    }
    public function kecamatan(){
        return $this->belongsTo('App\Models\District','district_id');
    }
    public function kelurahan(){
        return $this->belongsTo('App\Models\Village','village_id');
    }
    public function banyak(){
        return $this->hasMany('App\Models\User','parent_id');
    }
    public function userAkses(){
        return $this->hasMany(UserAksesInput::class,'user_id');
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
