<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Product extends Model
{
    use HasFactory;
    public $appends = ['AllStore','Url'];
    protected $fillable = [
        'category_id',
        'product_name',
        'product_image',
        'harga_distributor',
        'harga_agen',
        'harga_end_user',
        'description',
        'stok',
        'open',
        'admin_id'
    ];
    public function getUrlAttribute(){
        return Storage::disk('public')->url($this->product_image); 
    }

    public function store(){
        return $this->belongsTo('App\Models\Store','store_id');
    }
    public function getAllStoreAttribute()
    {
        return $this->AllCode($this->product_code);
    }
    public function AllCode($code)
    {
        $data = Product::where('product_code',$code)->where('open',1)->orderBy('id','ASC')->get();
        $id = [];
        foreach ($data as $key => $value) {
            $id[]=$value->store_id;
        }
        return $id;
    }
    public function cartId(){
        return $this->hasMany('App\Models\Cart','product_id')->where('user_id',auth()->user()->id);
    }
    public function category(){
        return $this->belongsTo('App\Models\CategoryProduct','category_id');
    }
    // public function checkCart($user_id)
    // {
    //     return $this->cartId()->where('user_id',auth()->user()->id)->get();
    // }
}
