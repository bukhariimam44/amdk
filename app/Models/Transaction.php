<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $appends = ['Users','Items'];
    protected $fillable = [
        'user_id',
        'upline_id',
        'no_order',
        'transaction_date',
        'harga_total',
        'order_status_id',
        'sequence'
    ];
    public function getItemsAttribute()
    {
        return $this->item()->first();
    } 
    public function item(){
        return $this->hasMany('App\Models\Item');
    }
    public function status(){
        return $this->belongsTo('App\Models\OrderStatus','order_status_id');
    }
    public function getUsersAttribute()
    {
        return $this->user()->first();
    } 
    public function user(){
        return $this->belongsTo('App\Models\User','user_id');
    }
}
