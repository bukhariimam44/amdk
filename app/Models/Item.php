<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;
    protected $fillable = [
        'transaction_id',
        'product_id',
        'product_name',
        'harga',
        'qty'
    ];
    public function product(){
        return $this->belongsTo('App\Models\Product','product_id');
    }
} 
