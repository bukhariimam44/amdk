<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller
{
    protected function credentials(Request $request)
    { 
        if(is_numeric($request->username)){
            return ['phone'=>$request->username,'password'=>$request->password];
        }elseif (filter_var($request->username, FILTER_VALIDATE_EMAIL)) {
            return ['email' => $request->username, 'password'=>$request->password];
        }
        return ['username' => $request->username, 'password'=>$request->password];
    }
    public function validasiError($errorMessages = [], $code = 400)
    {
        $response = [
            'code'=>$code,
            'title'=>'Opps...',
            'icon'=>'error',
            'message'=>$errorMessages->first()
        ];
        return response()->json($response);
    }
    public function sendSuccess($messages = [], $code = 200)
    {
        $response = [
            'code'=>$code,
            'title'=>'Berhasil',
            'icon'=>'success',
            'message'=> $messages
        ];
        return response()->json($response);
    }
    public function sendError($errorMessages = [], $code = 400)
    {
        $response = [
            'code'=>$code,
            'title'=>'Gagal',
            'icon'=>'error',
            'message'=> $errorMessages
        ];
        return response()->json($response);
    }
    public function sendResponseLogin($result, $message, $token)
    {
        $response = [
            'code'=>200,
            'message'=>$message,
            'data'    => $result,
            'token' => 'Bearer '.$token
        ];
        return response()->json($response, 200);
    }
    public function sendGetData($data = [], $code = 200)
    {
        $response = [
            'code'=>$code,
            'title'=>'Berhasil',
            'icon'=>'success',
            'data'=> $data
        ];
        return response()->json($response);
    }
}
