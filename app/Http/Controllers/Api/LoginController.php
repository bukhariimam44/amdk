<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Validator;
use Log;

class LoginController extends BaseController
{
    public function login(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, [
            'username' => 'required',
            'password' => 'required|min:6'
		],[
			'username.required'=>'Username harus diisi',
			'password.required'=>'Password minimal 6 digit',
			'password.min'=>'Password minimal 6 digit',
		]);
		if($validator->fails()){
            return $this->validasiError($validator->errors());
        }
		$credential = $this->credentials($request);
		if (Auth::attempt($credential)) {
			$user = Auth::user();
			if ($user->open == 0) {
				return $this->sendError('Akun tidak aktif');
			}
			return $this->sendResponseLogin($user, 'Berhasil Login.',$user->createToken('Resto')->accessToken);
		} else {
			return $this->sendError('Username atau Password salah');
		}
    }
}
