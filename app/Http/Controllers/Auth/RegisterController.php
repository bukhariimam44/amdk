<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Rules\CheckTag;
use DB;
use Log;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    // protected function validator(array $data)
    // {
    //     return Validator::make($data, [
    //         'name' => ['required', 'string', 'max:255'],
    //         'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
    //         'password' => ['required', 'string', 'min:8', 'confirmed'],
    //     ]);
    // }

    // /**
    //  * Create a new user instance after a valid registration.
    //  *
    //  * @param  array  $data
    //  * @return \App\Models\User
    //  */
    // protected function create(array $data)
    // {
    //     return User::create([
    //         'name' => $data['name'],
    //         'email' => $data['email'],
    //         'password' => Hash::make($data['password']),
    //     ]);
    // }

    protected function daftar(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', new CheckTag],
            'nik' => ['required', 'numeric', 'unique:users', new CheckTag],
            'rt' => ['required', 'string', 'max:5', new CheckTag],
            'rw' => ['required', 'string', 'max:5', new CheckTag],
            'alamat' => ['required', 'string', 'max:255', new CheckTag],
            'provinsi' => ['required', 'numeric', new CheckTag],
            'kabupaten' => ['required', 'numeric', new CheckTag],
            'kecamatan' => ['required', 'numeric', new CheckTag],
            'desa' => ['required', 'numeric', new CheckTag],
            'kode_pos' => ['required', 'numeric', 'min:5',  new CheckTag],
            'email' => ['required', 'string', 'email', 'unique:users', new CheckTag],
            'phone' => ['required', 'string', 'max:15', 'unique:users', new CheckTag],
            'password' => ['required', 'string', 'min:8', 'confirmed','regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/', new CheckTag],
            'password_confirmation' => ['required', 'string', 'min:8', new CheckTag],
        ],[
          'password.regex'=>'Kata sandi harus mengandung setidaknya 1 Huruf Besar, 1 Huruf Kecil, 1 Angka, dan 1 karakter khusus.'
        ]);

        if ($validator->fails()) {
            $err = [
                'name'=>$validator->errors()->first('name'),
                'nik'=>$validator->errors()->first('nik'),
                'rt'=>$validator->errors()->first('rt'),
                'rw'=>$validator->errors()->first('rw'),
                'alamat'=>$validator->errors()->first('alamat'),
                'provinsi'=>$validator->errors()->first('provinsi'),
                'kabupaten'=>$validator->errors()->first('kabupaten'),
                'kecamatan'=>$validator->errors()->first('kecamatan'),
                'desa'=>$validator->errors()->first('desa'),
                'kode_pos'=>$validator->errors()->first('kode_pos'),
                'email'=>$validator->errors()->first('email'),
                'phone'=>$validator->errors()->first('phone'),
                'password'=>$validator->errors()->first('password'),
                'password_confirmation'=>$validator->errors()->first('password_confirmation'),
            ];
            return response()->json([
                'code'=>401,
                'title'=>'Opps..',
                'icon'=>'error',
                'error'=>$err
            ]);
        }
        DB::beginTransaction();
        try {
            $kode_pos_agen = User::where('user_role_id',4)->where('open',1)->get();
            $kode_pos = [];
            foreach ($kode_pos_agen as $key => $value) {
                $kode_pos[] = $value->kode_pos;
            }
            $closest = null;
            foreach ($kode_pos as $item) {
                if ($closest === null || abs($request->kode_pos - $closest) > abs($item - $request->kode_pos)) {
                    $closest = $item;
                }
            }
            $parent =  User::where('kode_pos',$closest)->where('user_role_id',4)->where('open',1)->get();
            $userId = [];
            foreach ($parent as $ky => $pr) {
                $userId[] = $pr->id;
            }
            $random_keys=array_rand($userId);
            User::create([
                'nik'=>$request->nik,
                'parent_id'=>$userId[$random_keys],
                'nama_depan' =>$request->name, 
                'email'=>$request->email,
                'phone'=>$request->phone,
                'membership_id'=>2,
                'rt'=>$request->rt,
                'rw'=>$request->rw,
                'alamat'=>$request->alamat,
                'province_id'=>$request->provinsi,
                'regency_id'=>$request->kabupaten,
                'district_id'=>$request->kecamatan,
                'village_id'=>$request->desa,
                'kode_pos'=>$request->kode_pos,
                'user_role_id'=>5,
                'password'=>Hash::make($request->password),
            ]);
        } catch (\Throwable $th) {
            Log::info($th);
            DB::rollback();
            return response()->json([
                'code'=>400,
                'icon'=>'error',
                'title'=>'Opps..',
                'message'=>'Failed'
            ]);
        }
        DB::commit();
        return response()->json([
            'code'=>200,
            'icon'=>'success',
            'title'=>'Success',
            'message'=>'Register Success'
        ]);
    }
}
