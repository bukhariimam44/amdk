<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;
use App\Models\Transaction;
use App\Models\Item;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Pabrik;
use App\Models\Supplier;
use App\Models\Material;
use Illuminate\Support\Arr;

use Log;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if (auth()->user()->user_role_id == 5) {
            return redirect()->route('end-user-home');
        }else {
            $year = date('Y');
            if ($request->year > 0) {
                $year = $request->year;
            }
            return view('pages.chart',compact('year'));
        }
        
    }

    public function order_sekarang(Request $request){
        $products = $request->product_id;
        $sum = 0;
        $jumlah = $request->jumlah;
        foreach ($products as $key => $value) {
            $product = Product::find($value);
            if ($request->user()->user_role_id == 3) {
                $sum+=$product->harga_distributor * $jumlah[$key];
            }elseif ($request->user()->user_role_id == 4) {
                $sum+=$product->harga_agen * $jumlah[$key];
            }elseif ($request->user()->user_role_id == 5) {
                $sum+=$product->harga_end_user * $jumlah[$key];
            }
            
        }
        DB::beginTransaction();
        try {
            $tr = Transaction::create([
                'no_order'=>$request->user()->id.$request->user()->parentId->id.rand(100,999),
                'user_id'=>$request->user()->id,
                'upline_id'=>$request->user()->parentId->id,
                'transaction_date'=>date('Y-m-d H:i:s'),
                'harga_total'=>$sum,
                'order_status_id'=>1
            ]);
            foreach ($products as $key => $value) {
                $product = Product::find($value);
                $cart = Cart::where('product_id',$value)->where('user_id',$request->user()->id)->first()->delete();
                if ($request->user()->user_role_id == 3) {
                    Item::create([
                        'transaction_id'=>$tr->id,
                        'product_id'=>$product->id,
                        'product_name'=>$product->product_name,
                        'harga'=>$product->harga_distributor,
                        'qty'=>$jumlah[$key]
                    ]);
                }elseif ($request->user()->user_role_id == 4) {
                    Item::create([
                        'transaction_id'=>$tr->id,
                        'product_id'=>$product->id,
                        'product_name'=>$product->product_name,
                        'harga'=>$product->harga_agen,
                        'qty'=>$jumlah[$key]
                    ]);
                }elseif ($request->user()->user_role_id == 5) {
                    Item::create([
                        'transaction_id'=>$tr->id,
                        'product_id'=>$product->id,
                        'product_name'=>$product->product_name,
                        'harga'=>$product->harga_end_user,
                        'qty'=>$jumlah[$key]
                    ]);
                }
                
            }
            
        } catch (\Throwable $th) {
            Log::info('Order = '.$th);
            DB::rollBack();
            return redirect()->back()->with('error','Order Gagal');
        }
        DB::commit();
        return redirect()->route('status-transaksi')->with('success','Order Berhasil');
    }
    public function pabrik(Request $request){
        $data = Pabrik::where('open',1)->get();
        return response()->json($data);
    }
    public function supplier(Request $request){
        $data = Supplier::where('open',1)->get();
        return response()->json($data);
    }
    public function material(Request $request){
        $data = Material::where('open',1)->get();
        return response()->json($data);
    }
    public function akumulasi(Request $request){
        $from = date('Y-m-01');
        $to = date('Y-m-d');
        if ($request->action =='cari') {
            $from = date('Y-m-d', strtotime($request->from));
            $to = date('Y-m-d', strtotime($request->to));
        }
        $status = $request->status;
        $posisi = $request->posisi;
        $items = [];
        if ($request->user()->user_role_id == 1 || $request->user()->user_role_id == 2) {
            $items = DB::table('items')
            ->join('transactions','items.transaction_id','=','transactions.id')
            ->join('users','transactions.user_id','=','users.id')
            ->select('product_name',DB::raw('SUM(harga*qty) as total_harga'), DB::raw('SUM(qty) as quantity'))
            ->groupBy('product_name')
            ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])
            // ->where('transactions.upline_id','=',$request->user()->id)
            ->where('users.user_role_id','=',$posisi)
            ->where('transactions.order_status_id','LIKE','%'.$status.'%')
            ->get();
        }elseif ($request->user()->user_role_id == 3) {
            if ($posisi == 4) {
                $items = DB::table('items')
                ->join('transactions','items.transaction_id','=','transactions.id')
                ->join('users','transactions.user_id','=','users.id')
                ->select('product_name',DB::raw('SUM(harga*qty) as total_harga'), DB::raw('SUM(qty) as quantity'))
                ->groupBy('product_name')
                ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])
                ->where('transactions.upline_id','=',$request->user()->id)
                ->where('users.user_role_id','=',$posisi)
                ->where('transactions.order_status_id','LIKE','%'.$status.'%')
                ->get();
            }elseif ($posisi == 5) {
                $id = [];
                foreach ($request->user()->banyak as $key => $value) {
                    $id[] = $value->id;
                }
                $items = DB::table('items')
                ->join('transactions','items.transaction_id','=','transactions.id')
                ->join('users','transactions.user_id','=','users.id')
                ->select('product_name',DB::raw('SUM(harga*qty) as total_harga'), DB::raw('SUM(qty) as quantity'))
                ->groupBy('product_name')
                ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])
                ->whereIn('transactions.upline_id',$id)
                ->where('users.user_role_id','=',$posisi)
                ->where('transactions.order_status_id','LIKE','%'.$status.'%')
                ->get();
            }
        }elseif ($request->user()->user_role_id == 4) {
            if ($posisi == 5) {
                $items = DB::table('items')
                ->join('transactions','items.transaction_id','=','transactions.id')
                ->join('users','transactions.user_id','=','users.id')
                ->select('product_name',DB::raw('SUM(harga*qty) as total_harga'), DB::raw('SUM(qty) as quantity'))
                ->groupBy('product_name')
                ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])
                ->where('transactions.upline_id','=',$request->user()->id)
                ->where('users.user_role_id','=',$posisi)
                ->where('transactions.order_status_id','LIKE','%'.$status.'%')
                ->get();
            }
        }        
        return view('pages.akumulasi',compact('items','from','to','status','posisi'));
    }
    public function api_quantity(Request $request){
        $cart = Cart::where('id',decrypt($request->id))->where('user_id',$request->user()->id)->first();
        if ($request->action == 'tambah') {
            $cart->jumlah = $cart->jumlah + 1;
        }elseif ($request->action == 'kurang') {
            if ($cart->jumlah > 1) {
                $cart->jumlah = $cart->jumlah - 1;
            }else {
                $cart->delete();
                return response()->json([
                    'code'=>200,
                    'message'=>'Berhasil hapus'
                ]);
            }
            
        }
        if ($cart->update()) {
            return response()->json([
                'code'=>200,
                'message'=>'Berhasil update'
            ]);
        }
        return response()->json([
            'code'=>400,
            'message'=>'Gagal update'
        ]);

    }
    public function grafik(Request $request){
        $start_date = date('Y-01-01');
        $end_date   = date('Y-12-31');
        $datas = Item::selectRaw('(sum(harga)) as total_harga')
                ->selectRaw('(sum(qty)) as total_item')
                ->selectRaw("DATE_FORMAT(items.created_at, '%m-%Y') new_date, YEAR(items.created_at) year, MONTH(items.created_at) month")
                ->join('transactions','transactions.id','=','items.transaction_id')
                ->whereBetween('transactions.transaction_date', [$start_date. ' 00:00:00', $end_date. ' 23:59:59'])
                ->groupBy('new_date','year','month')
                ->get();
                Log::info(json_encode($datas));
        $harga = [];
        for($i=0; $i < 12; $i+=1) { 
            if (isset($datas[$i]['month']) && $datas[$i]['month'] == ($i+1)) {
                $harga[] = $datas[$i]['total_harga'];
            }else {
                $harga[] = 0;
            }
            
        }
        $total = [
            [
                "name"=> "Total Jual",
                "data"=> $harga
            ]
            ];
            
        $name = Item::selectRaw('(sum(harga)) as total_harga')
                ->selectRaw("items.product_name name")
                ->selectRaw("DATE_FORMAT(items.created_at, '%m-%Y') new_date, YEAR(items.created_at) year, MONTH(items.created_at) month")
                ->join('transactions','transactions.id','=','items.transaction_id')
                ->whereBetween('transactions.transaction_date', [$start_date. ' 00:00:00', $end_date. ' 23:59:59'])
                ->groupBy('name','new_date','year','month')
                ->get();
                Log::info('NAME = '.$name);
        
        $totals = [];
        for($i=0; $i < 12; $i+=1) { 
            if (isset($name[$i]['month']) && $name[$i]['month'] == ($i+1)) {
                $totals[] = $name[$i]['total_harga'];
            }else {
                $totals[] = 0;
            }
            
        }
        $categori = [
            [
                "name"=> "Category 1",
                "data"=> $totals
            ]
            ];
        return response()->json([
            'bulanan'=>$total,
            'categori'=>$categori
        ]);
    }
    public function statistik(Request $request){
        $statistics=array();
        $prod = Product::where('open',1)->get();
        $months=['01','02','03','04','05','06','07','08','09','10','11','12'];
        if (auth()->user()->user_role_id == 1 || auth()->user()->user_role_id == 2) {
            foreach ($months as $month){
                $statistics[]=Transaction::join('users','transactions.user_id','=','users.id')->where('users.user_role_id','=',3)->where('transaction_date','LIKE',$request->year.'-'.$month.'%')->where('order_status_id','=',4)->sum('harga_total');
            }
            $pp = [];
            foreach ($prod as $key => $pro){
                $statisticsProduct = [];
                foreach ($months as $month) {
                    $item = Item::join('transactions','items.transaction_id','=','transactions.id')->join('users','transactions.user_id','=','users.id')->where('users.user_role_id','=',3)->where('transactions.transaction_date','LIKE',$request->year.'-'.$month.'%')->where('transactions.order_status_id','=',4)->where('product_id',$pro->id)->sum('harga');
                    $qty = Item::join('transactions','items.transaction_id','=','transactions.id')->join('users','transactions.user_id','=','users.id')->where('users.user_role_id','=',3)->where('transactions.transaction_date','LIKE',$request->year.'-'.$month.'%')->where('transactions.order_status_id','=',4)->where('product_id',$pro->id)->sum('qty');
                    $statisticsProduct[]=$item * $qty;
                }
                $pp[] = [
                    'name'=>$pro->product_name,
                    'data'=>$statisticsProduct,
                ];
            }
        }elseif (auth()->user()->user_role_id == 3) {
            foreach ($months as $month){
                $statistics[]=Transaction::join('users','transactions.user_id','=','users.id')->where('users.user_role_id','=',4)->where('users.parent_id',auth()->user()->id)->where('transaction_date','LIKE',$request->year.'-'.$month.'%')->where('order_status_id','=',4)->sum('harga_total');
            }
            $pp = [];
            foreach ($prod as $key => $pro){
                $statisticsProduct = [];
                foreach ($months as $month) {
                    $item = Item::join('transactions','items.transaction_id','=','transactions.id')->join('users','transactions.user_id','=','users.id')->where('users.parent_id',auth()->user()->id)->where('users.user_role_id','=',4)->where('transactions.transaction_date','LIKE',$request->year.'-'.$month.'%')->where('transactions.order_status_id','=',4)->where('product_id',$pro->id)->sum('harga');
                    $qty = Item::join('transactions','items.transaction_id','=','transactions.id')->join('users','transactions.user_id','=','users.id')->where('users.parent_id',auth()->user()->id)->where('users.user_role_id','=',4)->where('transactions.transaction_date','LIKE',$request->year.'-'.$month.'%')->where('transactions.order_status_id','=',4)->where('product_id',$pro->id)->sum('qty');
                    $statisticsProduct[]=$item * $qty;
                }
                $pp[] = [
                    'name'=>$pro->product_name,
                    'data'=>$statisticsProduct,
                ];
            }
        }elseif (auth()->user()->user_role_id == 4) {
            foreach ($months as $month){
                $statistics[]=Transaction::join('users','transactions.user_id','=','users.id')->where('users.user_role_id','=',5)->where('users.parent_id',auth()->user()->id)->where('transaction_date','LIKE',$request->year.'-'.$month.'%')->where('order_status_id','=',4)->sum('harga_total');
            }
            $pp = [];
            foreach ($prod as $key => $pro){
                $statisticsProduct = [];
                foreach ($months as $month) {
                    $item = Item::join('transactions','items.transaction_id','=','transactions.id')->join('users','transactions.user_id','=','users.id')->where('users.parent_id',auth()->user()->id)->where('users.user_role_id','=',5)->where('transactions.transaction_date','LIKE',$request->year.'-'.$month.'%')->where('transactions.order_status_id','=',4)->where('product_id',$pro->id)->sum('harga');
                    $qty = Item::join('transactions','items.transaction_id','=','transactions.id')->join('users','transactions.user_id','=','users.id')->where('users.parent_id',auth()->user()->id)->where('users.user_role_id','=',5)->where('transactions.transaction_date','LIKE',$request->year.'-'.$month.'%')->where('transactions.order_status_id','=',4)->where('product_id',$pro->id)->sum('qty');
                    $statisticsProduct[]=$item * $qty;
                }
                $pp[] = [
                    'name'=>$pro->product_name,
                    'data'=>$statisticsProduct,
                ];
            }
        }
        
        $total = [
            [
                "name"=> "TOTAL",
                "data"=> $statistics
            ]
            ];
        $count = [];
        foreach ($pp as $key => $value) {
             $count[] = array_merge($value['data']);
        }  
        $array = Arr::collapse($count);
        return response()->json([
            'bulanan'=>$total,
            'product'=>$pp,
            'max'=>$array
        ]); 
    }
    
}