<?php

namespace App\Http\Controllers\Principle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Membership;
use App\Models\Province;
use DB;
use Log;
use Validator;

class UserController extends BaseController
{
    public function index(Request $request){
        $role = UserRole::find($request['role']);
        $nama = $request->nama;
        $phone = $request->phone;
        $parent = $request['parent_id'];
        if ($role->id == 5) {
            $url = route('principle-data-user-end-user');
        }else {
            $url = route('principle-data-user-'.strtolower($role->role));
        }
        
        if ($request['role'] == 2 || $request['role']==3) {
            $datas = User::where('user_role_id',$role->id)->where('nama_depan','LIKE','%'.$nama.'%')->where('phone','LIKE','%'.$phone.'%')->where('open',1)->get();
        }else {
            if ($request->parent_id > 0) {
                $datas = User::where('user_role_id',$role->id)->where('parent_id',$parent)->where('nama_depan','LIKE','%'.$nama.'%')->where('phone','LIKE','%'.$phone.'%')->where('open',1)->get();
            }else {
                $datas = User::where('user_role_id',$role->id)->where('nama_depan','LIKE','%'.$nama.'%')->where('phone','LIKE','%'.$phone.'%')->where('open',1)->get();
            }
        }
        
        $users = User::where('user_role_id',(int)$request['role']-1)->where('open',1)->get();
        return view('pages.principle.user',compact('datas','role','url','users','nama','phone','parent'));
    }
    public function user_principle(Request $request){
        $request['role'] = 2;
        return $this->index($request);
    }
    public function user_distributor(Request $request){
        $request['role'] = 3;
        return $this->index($request);
    }
    public function user_agen(Request $request){
        $request['role'] = 4;
        return $this->index($request);
    }
    public function user_end_user(Request $request){
        $request['role'] = 5;
        return $this->index($request);
    }
    public function create(Request $request){
        if ($request->role == 4 || $request->role == 5) {
            return redirect()->route('principle-data-user-agen');
        }elseif ($request->role == 5) {
            return redirect()->route('principle-data-user-end-user');
        }

        $role = UserRole::find($request->role);
        $url = route('principle-data-user-'.strtolower($role->role));
        if ($request->action == 'create') {
            $validator = Validator::make($request->all(), [
                'nik' => 'required|string|unique:users',
                'nama_depan' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'phone' => 'required|min:10|unique:users',
                'membership_id'=>'required|integer',
                'rt'=>'required|string',
                'rw'=>'required|string',
                'alamat' => 'required|string',
                'province_id'=>'required|integer',
                'regency_id'=>'required|integer',
                'district_id'=>'required|integer',
                'village_id'=>'required|integer',
                'kode_pos'=>'required|integer',
                'password' => 'required|min:6',
            ],[
                'nik.unique'=>'Nik sudah digunakan.',
                'nik.required'=>'Nik harus diisi.',
                'nama_depan.required'=>'Nama Depan harus diisi',
                'email.required'=>'Email harus diisi.',
                'email.email'=>'Email tidak valid.',
                'email.unique'=>'Email sudah digunakan.',
                'phone.required'=>'Phone harus diisi.',
                'phone.unique'=>'Phone sudah digunakan.',
                'phone.min'=>'Phone minimal 10 digit.',
                'membership_id.required'=>'Membership harus diisi.',
                'rt.required'=>'RT harus diisi.',
                'rw.required'=>'RW harus diisi.',
                'alamat.required'=>'Alamat harus diisi.',
                'province_id.required'=>'Propinsi harus diisi.',
                'regency_id.required'=>'Kabupaten/Kota harus diisi.',
                'village_id.required'=>'Kelurahan harus diisi.',
                'kode_pos.required'=>'Kode POS harus diisi.',
                'password.required'=>'This value is required.',
                'password.min'=>'This value is required.',
            ]);
            if ($validator->fails()) {
                Log::info('GAGAL VAL = '.$validator->errors()->first());
                return $this->validasiError($validator->errors());
            }
            
            DB::beginTransaction();
            try {
                User::create([
                    'nik'=>$request->nik,
                    'parent_id'=>$request->user()->id,
                    'nama_depan' =>$request->nama_depan.' '.$request->nama_belakang, 
                    'email'=>$request->email,
                    'phone'=>$request->phone,
                    'membership_id'=>$request->membership_id,
                    'rt'=>$request->rt,
                    'rw'=>$request->rw,
                    'alamat'=>$request->alamat,
                    'province_id'=>$request->province_id,
                    'regency_id'=>$request->regency_id,
                    'district_id'=>$request->district_id,
                    'village_id'=>$request->village_id,
                    'kode_pos'=>$request->kode_pos,
                    'user_role_id'=>$role->id,
                    'password'=>Hash::make($request->password),
                ]);
            } catch (\Throwable $th) {
                Log::info($th);
                DB::rollback();
                return $this->sendError('Gagal disimpan', 400);
            }
            DB::commit();
            return $this->sendSuccess('Berhasil disimpan', 200);
        }

        $memberships = Membership::get();
        return view('pages.principle.create_user',compact('role','url','memberships'));
    }
    public function detail_user(Request $request,$id){
        $user = User::find($id);
        return view('pages.principle.detail_user',compact('user'));
    }
}
