<?php

namespace App\Http\Controllers\Principle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Exports\TransactionExport;
use App\Models\OrderStatus;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class OrderController extends Controller
{
    public function index(Request $request){
        $no_order = $request->no_order;
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->where('upline_id',$request->user()->id)->where('order_status_id',1)->get();
        return view('pages.principle.request_order',compact('datas','no_order'));
    }
    public function request_order(Request $request){
        $datas = Transaction::where('upline_id',$request->user()->id)->where('order_status_id',1)->get();
        return view('pages.request_order',compact('datas'));
    }
    public function detail_request_order(Request $request, $id){
        $datas = Transaction::where('id',decrypt($id))->first();
        return view('pages.principle.detail_order',compact('datas'));
    }
    public function proses_pembayaran(Request $request){
        $datas = Transaction::where('no_order',decrypt($request->id))->where('upline_id',$request->user()->id)->where('order_status_id',1)->first();
        $datas->order_status_id = 2;
        $datas->update();
        return redirect()->route('principle-status-order');
    }
    public function status_order(Request $request){
        if ($request->action == 'proses') {
            $proses = Transaction::where('no_order',decrypt($request->no_order))->where('upline_id',$request->user()->id)->whereIn('order_status_id',[2,3])->first();
            if ($proses->order_status_id == 2) {
                $proses->order_status_id = 3;
            }elseif ($proses->order_status_id == 3) {
                $proses->order_status_id = 4;
            }
            $proses->update();
            return redirect()->back();
        }
        $no_order = $request->no_order;
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->where('upline_id',$request->user()->id)->whereIn('order_status_id',[2,3])->orderBy('id','DESC')->get();
        return view('pages.principle.status_order',compact('datas','no_order'));
    }
    // public function data_order(Request $request){
    //     $no_order = $request->no_order;
    //     $from = date('Y-m-01');
    //     $to = date('Y-m-d');
    //     if ($request->action == 'cari') {
    //         $from = $request->from;
    //         $to = $request->to;
    //     }elseif ($request->action == 'transaksi') {
    //         return Excel::download(new TransactionExport($request->from,$request->to,'transaksi','order'), 'tansaksi.xlsx');
    //     }elseif ($request->action == 'item') {
    //         return Excel::download(new TransactionExport($request->from,$request->to,'item','order'), 'tansaksi.xlsx');
    //     }
    //     $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->wherebetween('transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('upline_id',$request->user()->id)->where('order_status_id',4)->orderBy('id','ASC')->get();
    //     return view('pages.principle.data_order',compact('datas','no_order','from','to'));
    // }
    public function orderan_distributor(Request $request, $role = 3){
        $status = $request->status;
        $from = date('Y-m-d',strtotime(date('Y-m-01')));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }elseif ($request->action == 'transaksi') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to, $status,'transaksi','order','admin',$role), 'tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to,$status,'item','order','admin',$role), 'tansaksi.xlsx');
        }
        $datas = Transaction::select('transactions.*')->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('transactions.order_status_id',$status)->where('users.user_role_id','=',$role)->orderBy('id','ASC')->get();
        $statuses = OrderStatus::get();
        return view('pages.principle.data_order_distributor',compact('datas','from','to','statuses','status'));
    }
    public function orderan_agen(Request $request, $role = 4){
        $status = $request->status;
        $from = date('Y-m-d',strtotime(date('Y-m-01')));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }elseif ($request->action == 'transaksi') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to, $status,'transaksi','order','admin',$role), 'tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to,$status,'item','order','admin',$role), 'tansaksi.xlsx');
        }
        $datas = Transaction::select('transactions.*')->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('transactions.order_status_id',$status)->where('users.user_role_id','=',$role)->orderBy('id','ASC')->get();
        $statuses = OrderStatus::get();
        return view('pages.principle.data_order_agen',compact('datas','from','to','statuses','status'));
    }
    public function orderan_enduser(Request $request,$role = 5){
        $status = $request->status;
        $from = date('Y-m-d',strtotime(date('Y-m-01')));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }elseif ($request->action == 'transaksi') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to, $status,'transaksi','order','admin',$role), 'tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to,$status,'item','order','admin',$role), 'tansaksi.xlsx');
        }
        $datas = Transaction::select('transactions.*')->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('transactions.order_status_id',$status)->where('users.user_role_id','=',$role)->orderBy('id','ASC')->get();
        $statuses = OrderStatus::get();
        return view('pages.principle.data_order_enduser',compact('datas','from','to','statuses','status'));
    }
}
