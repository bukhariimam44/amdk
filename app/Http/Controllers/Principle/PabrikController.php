<?php

namespace App\Http\Controllers\Principle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pabrik;

class PabrikController extends Controller
{
    public function index(){
        $datas = Pabrik::where('open',1)->get();
        return view('pages.principle.data_pabrik',compact('datas'));
    }
}
