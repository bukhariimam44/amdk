<?php

namespace App\Http\Controllers\Principle;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;
use App\Models\CategoryProduct;
use DB;
use Log;

class ProductController extends Controller
{
    public function index(Request $request){
        $datas = Product::where('product_name','LIKE','%'.$request->product_name.'%')->where('open',1)->orderBy('id','ASC')->get();
        return view('pages.principle.product',compact('datas'));
    }
    public function create(Request $request){
        if ($request->action == 'create') {
            // return $request->all();
            $request->validate([
                'category_id' => 'required',
                'product_name' => 'required',
                'product_image' => 'required|mimes:jpeg,jpg,png,gif|max:10000',
                'harga_distributor' => 'required',
                'harga_agen' => 'required',
                'harga_end_user' => 'required',
                'description' => 'required',
                'stok' => 'required',
            ],[
                'category_id.required'=>'This value is required.',
                'product_name.required'=>'This value is required.',
                'product_image.required'=>'This value is required.',
                'harga_distributor.required'=>'This value is required.',
                'harga_agen.required'=>'This value is required.',
                'harga_end_user.required'=>'This value is required.',
                'description.required'=>'This value is required.',
                'stok.required'=>'This value is required.'
            ]);

            DB::beginTransaction();
            try {
                $imageName = time().'.'.$request->product_image->extension();  
                $request->product_image->move(public_path('img/product'), $imageName);
                Product::create([
                    'admin_id'=>$request->user()->id,
                    'category_id'=>$request->category_id,
                    'product_name'=>$request->product_name,
                    'product_image'=>$imageName,
                    'harga_distributor'=>preg_replace("/[^0-9]/", "", $request->harga_distributor),
                    'harga_agen'=>preg_replace("/[^0-9]/", "", $request->harga_agen),
                    'harga_end_user'=>preg_replace("/[^0-9]/", "", $request->harga_end_user),
                    'description'=>$request->description,
                    'stok'=>$request->stok,
                    'open'=>1
                ]);
            } catch (\Throwable $th) {
                Log::info('Gagal Add product = '.$th);
                DB::rollback();
                return redirect()->back()->with('error','Gagal');
            }
            DB::commit();
            return redirect()->route('principle-product')->with('success','Success');
        }
        $categories = CategoryProduct::where('open',1)->get();
        return view('pages.principle.create_product',compact('categories'));
    }
    public function edit(Request $request, $id){
        $product = Product::where('user_id',auth()->user()->id)->where('id',$id)->first();
        if ($request->action == 'update') {
            $request->validate([
                'store_id' => 'required',
                'product_name' => 'required',
                'base_price' => 'required',
                'selling_price' => 'required',
                'description' => 'required',
                'stok' => 'required',
            ],[
                'store_id.required'=>'This value is required.',
                'product_name.required'=>'This value is required.',
                'base_price.required'=>'This value is required.',
                'selling_price.required'=>'This value is required.',
                'description.required'=>'This value is required.',
                'stok.required'=>'This value is required.'
            ]);
            if ($request->file('product_image')) {
                $request->validate([
                    'product_image' => 'required|mimes:jpeg,jpg,png,gif|max:10000',
                ],[
                    'product_image.required'=>'This value is required.',
                ]);
            }
            DB::beginTransaction();
            try {
                $variable = $request->store_id;
                foreach ($variable as $key => $value) {
                    $p = Product::updateOrcreate([
                        'store_id'=>$value,
                        'product_code'=>$product->product_code,
                    ],[
                        'user_id'=>$request->user()->id,
                        'product_name'=>$request->product_name,
                        'base_price'=>$result = preg_replace("/[^0-9]/", "", $request->base_price),
                        'selling_price'=>$result = preg_replace("/[^0-9]/", "", $request->selling_price),
                        'description'=>$request->description,
                        'stok'=>$request->stok,
                        'open'=>1
                    ]);
                    if ($request->file('product_image')) {
                        $path = Storage::disk('s3')->put('public/product',$request->file('product_image'), 'public');
                        $p->update([
                            'product_image'=>$path,
                        ]);
                    }
                }
                
            } catch (\Throwable $th) {
                Log::info('Gagal Add product = '.$th);
                DB::rollback();
                return redirect()->back()->with('error','Gagal');
            }
            DB::commit();
            return redirect()->route('admin-product')->with('success','Success');
        }
        $main_stores = Store::whereIn('id',$product->AllStore)->where('admin_id',$request->user()->id)->orderBy('id','ASC')->get();
        return view('pages.principle.edit_product',compact('product','main_stores'));
    }
    public function delete(Request $request){
        $product = Product::where('id',$request->product_id)->where('user_id',$request->user()->id)->first();
        $product->open = 0;
        if ($product->update()) {
            return response()->json([
                'code'=>200,
                'title'=>'Deleted!',
                'icon'=>'success',
                'message'=>'Your file has been deleted.'
            ]);
        }
        return response()->json([
            'code'=>400,
            'title'=>'Failed!',
            'icon'=>'error',
            'message'=>'Your failed to delete.'
        ]);
    }
}
