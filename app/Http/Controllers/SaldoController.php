<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pabrik;

class SaldoController extends Controller
{
    
    public function index(Request $request){
        $datas = Pabrik::where('open',1)->get();
        return view('pages.admin.saldo.index',compact('datas'));
    }
}
