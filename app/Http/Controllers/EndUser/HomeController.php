<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;

class HomeController extends Controller
{
    public function index(){
        $datas = Product::where('open',1)->orderBy('id','ASC')->get();
        return view('pages.enduser.product',compact('datas'));
    }
}
