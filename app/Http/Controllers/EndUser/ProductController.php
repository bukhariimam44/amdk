<?php

namespace App\Http\Controllers\EndUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;
use App\Models\Transaction;
use App\Models\Item;
use App\Exports\TransactionExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\Transactions\Order;
use DB;
use Log;

class ProductController extends Controller
{
    public function index(Request $request){
        $datas = Product::where('open',1)->orderBy('id','ASC')->get();
        return view('pages.enduser.product',compact('datas'));
    }
    public function detail(Request $request, $id){
        $data = Product::find(decrypt($id));
        return view('pages.enduser.detail_product',compact('data'));
    }
    public function keranjang(Request $request){
        $datas = Cart::where('user_id',$request->user()->id)->orderBy('id','ASC')->get();
        return view('pages.enduser.keranjang',compact('datas'));
    }
    public function product(Request $request){
        $id = decrypt($request->product_id);
        if ($request->action == 'add') {
            $cart = Cart::updateOrCreate([
                'product_id'=>$id,
                'user_id'=>$request->user()->id
            ],[
                'jumlah'=>1
            ]);
            if ($cart) {
                return redirect()->back()->with('success','Berhasil disimpan');
            }
            return redirect()->back()->with('error','Gagal disimpan');
        }elseif ($request->action == 'delete') {
            $cart = Cart::where('product_id',$id)->first()->delete();
            if ($cart) {
                return redirect()->back()->with('success','Berhasil dihapus');
            }
            return redirect()->back()->with('error','Gagal dihapus');
        }       
        
    }
    public function qty(Request $request){
        $cart = Cart::find(decrypt($request->id));
        if ($request->action == 'tambah') {
            $cart->jumlah = $cart->jumlah + 1;
        }elseif ($request->action == 'kurang') {
            if ($cart->jumlah > 1) {
                $cart->jumlah = $cart->jumlah - 1;
            }else {
                $cart->delete();
                return redirect()->back();
            }
            
        }elseif ($request->action == 'hapus') {
            $cart->delete();
            return redirect()->back();
        }elseif ($request->action == 'ganti') {
            $cart->jumlah = $request->jumlah;
            if ($cart->update()) {
                return response()->json([
                    'code'=>200,
                    'msg'=>'Success'
                ]);
            }
            return response()->json([
                'code'=>400,
                'msg'=>'Failed'
            ]);
            
        }
        $cart->update();
        return redirect()->back();
    }
    public function order_sekarang(Request $request){
        $res = Order::po([
            'user_id'=>$request->user()->id,
            'upline_id'=>$request->user()->parentId->id,
            'user_role_id'=>$request->user()->user_role_id
        ])->get();
        if ($res['status']['code'] == 200) {
            return redirect()->route('end-user-status-transaksi')->with('success','Order Berhasil');
        }
        return redirect()->back()->with('error','Order Gagal');
    }
    public function status_transaksi(Request $request){
        if ($request->action == 'proses') {
            $update = Transaction::where('user_id',$request->user()->id)->where('no_order',decrypt($request->no_order))->where('order_status_id',3)->first();
            $update->order_status_id = 4;
            $update->update();
            return redirect()->back();
        }
        $no_order = $request->no_order;
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->where('user_id',$request->user()->id)->orderBy('id','DESC')->get();
        return view('pages.enduser.status_transaksi',compact('datas','no_order'));
    }
    public function detail_request_order(Request $request, $id){
        $datas = Transaction::where('user_id',$request->user()->id)->where('id',decrypt($id))->first();
        return view('pages.enduser.detail_transaksi',compact('datas'));
    }
    public function status_order(Request $request){
        if ($request->action == 'proses') {
            $proses = Transaction::where('no_order',decrypt($request->no_order))->where('upline_id',$request->user()->id)->whereIn('order_status_id',[2,3])->first();
            if ($proses->order_status_id == 2) {
                $proses->order_status_id = 3;
            }elseif ($proses->order_status_id == 3) {
                $proses->order_status_id = 4;
            }
            $proses->update();
            return redirect()->back();
        }
        $datas = Transaction::where('upline_id',$request->user()->id)->whereIn('order_status_id',[2,3])->orderBy('id','DESC')->get();
        return view('pages.enduser.status_order',compact('datas'));
    }

    public function report_transaksi(Request $request){
        $no_order = $request->no_order;
        $from = date('Y-m-01');
        $to = date('Y-m-d');
        $type = 'Transaksi';
        $url = route('end-user-report-transaksi');
        if ($request->action == 'cari') {
            $from = $request->from;
            $to = $request->to;
        }elseif ($request->action == 'transaksi') {
            return Excel::download(new TransactionExport($request->from,$request->to, $status=4, 'transaksi','transaksi',$role="mytrx",$roleUser = 5), 'report_tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            return Excel::download(new TransactionExport($request->from,$request->to, $status=4, 'item','transaksi',$role="mytrx",$roleUser = 5), 'report_tansaksi.xlsx');
        }
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->wherebetween('transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('user_id',$request->user()->id)->where('order_status_id',4)->orderBy('id','ASC')->get();
        return view('pages.enduser.report_transaksi',compact('datas','no_order','from','to','url','type'));
    }
    public function detail_item(Request $request,$id){
        $datas = Transaction::where('id',decrypt($id))->first();
        return view('pages.enduser.detail_item',compact('datas'));
    }
    
}
