<?php

namespace App\Http\Controllers\Distributor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Models\User;
use App\Models\UserRole;
use App\Models\Membership;
use App\Models\Province;
use DB;
use Log;
use Validator;

class UserController extends BaseController
{
    public function index(Request $request){
        $role = UserRole::find(4);
        $datas = User::where('user_role_id',4)->where('parent_id',$request->user()->id)->where('open',1)->orderBy('id','DESC')->get();
        return view('pages.distributor.user',compact('datas','role'));
    }
    public function create(Request $request){
        if ($request->action == 'create') {
            $validator = Validator::make($request->all(), [
                'nik' => 'required|string|unique:users',
                'nama_depan' => 'required|string',
                'email' => 'required|string|email|unique:users',
                'phone' => 'required|min:10|unique:users',
                'membership_id'=>'required|integer',
                'rt'=>'required|string',
                'rw'=>'required|string',
                'alamat' => 'required|string',
                'province_id'=>'required|integer',
                'regency_id'=>'required|integer',
                'district_id'=>'required|integer',
                'village_id'=>'required|integer',
                'kode_pos'=>'required|integer',
                'password' => 'required|min:6',
            ],[
                'nik.required'=>'Nik harus diisi.',
                'nik.unique'=>'Nik sudah digunakan.',
                'nama_depan.required'=>'Nama Depan harus diisi',
                'email.required'=>'Email harus diisi.',
                'email.email'=>'Email tidak valid.',
                'email.unique'=>'Email sudah digunakan.',
                'phone.required'=>'Phone harus diisi.',
                'phone.unique'=>'Phone sudah digunakan.',
                'phone.min'=>'Phone minimal 10 digit.',
                'membership_id.required'=>'Membership harus diisi.',
                'rt.required'=>'RT harus diisi.',
                'rw.required'=>'RW harus diisi.',
                'alamat.required'=>'Alamat harus diisi.',
                'province_id.required'=>'Propinsi harus diisi.',
                'regency_id.required'=>'Kabupaten/Kota harus diisi.',
                'village_id.required'=>'Kelurahan harus diisi.',
                'kode_pos.required'=>'Kode POS harus diisi.',
                'password.required'=>'Password harus diisi.',
                'password.min'=>'Password minimal 6 digit.',
            ]);
            if ($validator->fails()) {
                Log::info('GAGAL VAL = '.$validator->errors()->first());
                return $this->validasiError($validator->errors());
            }
            DB::beginTransaction();
            try {
                User::create([
                    'nik'=>$request->nik,
                    'parent_id'=>$request->user()->id,
                    'nama_depan' =>$request->nama_depan.' '.$request->nama_belakang, 
                    'email'=>$request->email,
                    'phone'=>$request->phone,
                    'membership_id'=>$request->membership_id,
                    'rt'=>$request->rt,
                    'rw'=>$request->rw,
                    'alamat'=>$request->alamat,
                    'province_id'=>$request->province_id,
                    'regency_id'=>$request->regency_id,
                    'district_id'=>$request->district_id,
                    'village_id'=>$request->village_id,
                    'kode_pos'=>$request->kode_pos,
                    'user_role_id'=>4,
                    'password'=>Hash::make($request->password),
                ]);
            } catch (\Throwable $th) {
                Log::info($th);
                DB::rollback();
                return $this->sendError('Gagal disimpan', 400);
            }
            DB::commit();
            return $this->sendSuccess('Berhasil disimpan', 200);
        }
        return view('pages.distributor.create_user');
    }
    public function detail_agen(Request $request,$id){
        $user = User::where('id',decrypt($id))->first();
        return view('pages.distributor.detail_user',compact('user'));
    }
    public function end_user(Request $request){
        $distributor = $request->user()->banyak[0]->id;
        if ($request->distributor) {
            $distributor = $request->distributor;
        }
        $datas = User::where('parent_id',$distributor)->orderBy('id','DESC')->get();
        $role = UserRole::find(5);
        // return $datas;
        return view('pages.distributor.user',compact('datas','role'));
    }
    
}
