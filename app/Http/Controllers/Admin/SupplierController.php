<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Supplier;

class SupplierController extends Controller
{
    public function index(Request $request){
        $datas = Supplier::where('open',1)->get();
        return view('pages.admin.supplier.index',compact('datas'));
    }
    public function form(Request $request){
        if ($request->action == 'create') {
            $create = Supplier::create([
                'supplier'=>$request->supplier,
                'telp'=>$request->telp,
                'alamat'=>$request->alamat,
                'admin_id'=>$request->user()->id,
                'open'=>1
            ]);
            if ($create) {
                return redirect()->route('admin-supplier')->with('success','Berhasil');
            }
            return redirect()->back()->with('error','Gagal');
        }elseif ($request->action == 'delete') {
            $data = Supplier::find($request->id);
            $data->admin_id = $request->user()->id;
            $data->open = 0;
            if ($data->update()) {
                return response()->json(['code'=>200]);
            }
            return response()->json(['code'=>400]);
        }
        return view('pages.admin.supplier.create');
    }
    public function edit(Request $request, $id){
        $data = Supplier::find($id);
        if ($request->action == 'update') {
            $data->supplier = $request->supplier;
            $data->telp = $request->telp;
            $data->alamat = $request->alamat;
            $data->admin_id = $request->user()->id;
            if ($data->update()) {
                return redirect()->route('admin-supplier')->with('success','Berhasil');
            }
            return redirect()->back()->with('error','Gagal');
        }
        return view('pages.admin.supplier.edit',compact('data'));
    }
}
