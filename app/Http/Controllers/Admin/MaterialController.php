<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\Pabrik;
use App\Models\Supplier;
use App\Models\SaldoMasuk;
use App\Models\MutasiSaldo;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Mutasis\Mutasi;
use DB;
use Log;

class MaterialController extends Controller
{
    public function index(Request $request){
        $datas = Material::where('open',1)->get();
        return view('pages.admin.material.index',compact('datas'));
    }
    public function form(Request $request){
        if ($request->action == 'create') {
            $create = Material::create([
                'material'=>$request->material,
                'keterangan'=>$request->keterangan,
                'admin_id'=>$request->user()->id,
                'open'=>1
            ]);
            if ($create) {
                return redirect()->route('admin-material')->with('success','Berhasil');
            }
            return redirect()->back()->with('error','Gagal');
        }elseif ($request->action == 'delete') {
            $data = Material::find($request->id);
            $data->admin_id = $request->user()->id;
            $data->open = 0;
            if ($data->update()) {
                return response()->json(['code'=>200]);
            }
            return response()->json(['code'=>400]);
        }
        return view('pages.admin.material.create');
    }
    public function edit(Request $request, $id){
        $data = Material::find($id);
        if ($request->action == 'update') {
            $data->material = $request->material;
            $data->keterangan = $request->keterangan;
            $data->admin_id = $request->user()->id;
            if ($data->update()) {
                return redirect()->route('admin-material')->with('success','Berhasil');
            }
            return redirect()->back()->with('error','Gagal');
        }
        return view('pages.admin.material.edit',compact('data'));
    }


    public function material_masuk(Request $request){
        $pabrik = $request->pabrik;
        $supplier = $request->supplier;
        $material = $request->material;
        $end = date('Y-m-d');
        $from = date('Y-m-d', strtotime('-7 days',strtotime($end)));
        if ($request->action == 'add') {
            $validator = Validator::make($request->all(),[
                'pabrik' => ['required'],
                'supplier' => ['required'],
                'material' => ['required'],
                'tanggal' => ['required'],
                'saldo' => ['required','numeric'],
                'keterangan' => ['required'],
            ],[
                'pabrik.required'=>'Pabrik harus dipilih.',
                'supplier.required'=>'Supplier harus dipilih.',
                'material.required'=>'Material harus dipilih.',
                'tanggal.required'=>'Tanggal harus diisi.',
                'saldo.required'=>'Saldo harus diisi.',
                'keterangan.required'=>'Keterangan harus diisi.'
            ]);
            if ($validator->fails()) {
                $msg = [
                    'pabrik' => $validator->errors()->first('pabrik'),
                    'supplier' => $validator->errors()->first('supplier'),
                    'material' => $validator->errors()->first('material'),
                    'tanggal' => $validator->errors()->first('tanggal'),
                    'saldo' => $validator->errors()->first('saldo'),
                    'keterangan' => $validator->errors()->first('keterangan'),
                ];
                return response()->json([
                    'code'=>401,
                    'errors'=>$msg
                ]);
            }
            $res = Mutasi::masuk([
                'material'=>$material,
                'pabrik'=>$pabrik,
                'supplier'=>$supplier,
                'transaction_date'=>$request->tanggal,
                'admin_id'=>$request->user()->id,
                'keterangan'=>$request->keterangan,
                'saldo'=>$request->saldo,
            ])->get();
            return response()->json($res);
        }elseif ($request->action == 'cari') {
            $from = date('Y-m-d',strtotime($request->from));
            $end = date('Y-m-d',strtotime($request->end));
        }
        $datas = MutasiSaldo::whereBetween('transaction_date',[$from.' 00:00:00',$end.' 23:59:59'])->where('pabrik_id','LIKE','%'.$request->pabrik.'%')->where('supplier_id','LIKE','%'.$request->supplier.'%')->where('material_id','LIKE','%'.$request->material.'%')->where('mutasi','masuk')->orderBy('id','ASC')->get();
        $pabriks = Pabrik::where('open',1)->get();
        $suppliers = Supplier::where('open',1)->get();
        $materials = Material::where('open',1)->get();
        $from = date('d-m-Y',strtotime($from));
        $end = date('d-m-Y',strtotime($end));
        return view('pages.admin.mutasiSaldo.saldo_masuk',compact('datas','pabriks','suppliers','materials','pabrik','supplier','material','from','end'));
    }
    public function material_keluar(Request $request){
        $pabrik = $request->pabrik;
        $material = $request->material;
        $end = date('Y-m-d');
        $from = date('Y-m-d', strtotime('-7 days',strtotime($end)));
        if ($request->action == 'add') {
            $validator = Validator::make($request->all(),[
                'pabrik' => ['required'],
                'material' => ['required'],
                'tanggal' => ['required'],
                'saldo' => ['required','numeric'],
                'keterangan' => ['required'],
            ],[
                'pabrik.required'=>'Pabrik harus dipilih.',
                'material.required'=>'Material harus dipilih.',
                'tanggal.required'=>'Tanggal harus diisi.',
                'saldo.required'=>'Saldo harus diisi.',
                'keterangan.required'=>'Keterangan harus diisi.'
            ]);
            if ($validator->fails()) {
                $msg = [
                    'pabrik' => $validator->errors()->first('pabrik'),
                    'material' => $validator->errors()->first('material'),
                    'tanggal' => $validator->errors()->first('tanggal'),
                    'saldo' => $validator->errors()->first('saldo'),
                    'keterangan' => $validator->errors()->first('keterangan'),
                ];
                return response()->json([
                    'code'=>401,
                    'errors'=>$msg
                ]);
            }
            $res = Mutasi::keluar([
                'material'=>$material,
                'pabrik'=>$pabrik,
                'supplier'=>null,
                'transaction_date'=>$request->tanggal,
                'admin_id'=>$request->user()->id,
                'keterangan'=>$request->keterangan,
                'saldo'=>$request->saldo,
            ])->get();
            return response()->json($res);
        }elseif ($request->action == 'cari') {
            $from = date('Y-m-d',strtotime($request->from));
            $end = date('Y-m-d',strtotime($request->end));
        }
        $datas = MutasiSaldo::whereBetween('transaction_date',[$from.' 00:00:00',$end.' 23:59:59'])->where('pabrik_id','LIKE','%'.$request->pabrik.'%')->where('material_id','LIKE','%'.$request->material.'%')->where('mutasi','keluar')->orderBy('id','ASC')->get();
        $pabriks = Pabrik::where('open',1)->get();
        $materials = Material::where('open',1)->get();
        $from = date('d-m-Y',strtotime($from));
        $end = date('d-m-Y',strtotime($end));
        return view('pages.admin.mutasiSaldo.saldo_keluar',compact('datas','pabriks','materials','pabrik','material','from','end'));
    }
    public function mutasi_saldo(Request $request){
        $pabrik = $request->pabrik;
        $material = $request->material;
        $end = date('Y-m-d');
        $from = date('Y-m-d', strtotime('-7 days',strtotime($end)));
        if ($request->action == 'cari') {
            $from = date('Y-m-d',strtotime($request->from));
            $end = date('Y-m-d',strtotime($request->end));
        }
        $datas = MutasiSaldo::whereBetween('transaction_date',[$from.' 00:00:00',$end.' 23:59:59'])->where('pabrik_id',$request->pabrik)->where('material_id',$request->material)->orderBy('id','ASC')->get();
        $pabriks = Pabrik::where('open',1)->get();
        $materials = Material::where('open',1)->get();
        $from = date('d-m-Y',strtotime($from));
        $end = date('d-m-Y',strtotime($end));
        return view('pages.admin.mutasiSaldo.mutasi_saldo',compact('datas','pabriks','materials','pabrik','material','from','end'));
    }

}
