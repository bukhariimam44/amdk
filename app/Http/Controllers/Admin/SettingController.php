<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Log;

class SettingController extends Controller
{
    public function principle_akses(Request $request){
        return view('pages.admin.settings.principle_akses');
    }
    public function data_principle_akses(Request $request){
        return response()->json([
            'users'=>principleAll(),
            'akses'=>aksesAll()
        ]);
    }
    public function change_akses(Request $request){
        $data = User::find($request->user_id);
        $arr = json_decode($data->actions);
        Log::info($request->akses);
        if (in_array($request->akses,$arr)) {
            array_splice($arr, array_search($request->akses, $arr ), 1);
        }else{
            array_push($arr, $request->akses);
        }
        $data->actions = $arr;
        if ($data->update()) {
            return response()->json([
                'code'=>200,
                'message'=>'Berhasil'
            ]);
        }
        return response()->json([
            'code'=>400,
            'message'=>'Gagal'
        ]);
    }
}
