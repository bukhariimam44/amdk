<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\OrderStatus;
use App\Models\Item;
use App\Models\UserRole;
use App\Exports\TransactionExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;

class OrderController extends Controller
{
    public function detail_request_order(Request $request, $id){
        $datas = Transaction::where('id',decrypt($id))->first();
        return view('pages.admin.detail_order',compact('datas'));
    }
   
    // public function data_order(Request $request){
    //     $no_order = $request->no_order;
    //     $from = date('Y-m-01');
    //     $to = date('Y-m-d');
    //     if ($request->action == 'cari') {
    //         $from = $request->from;
    //         $to = $request->to;
    //     }elseif ($request->action == 'transaksi') {
    //         return Excel::download(new TransactionExport($request->from,$request->to,'transaksi','order'), 'tansaksi.xlsx');
    //     }elseif ($request->action == 'item') {
    //         return Excel::download(new TransactionExport($request->from,$request->to,'item','order'), 'tansaksi.xlsx');
    //     }
    //     $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->wherebetween('transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('upline_id',$request->user()->id)->where('order_status_id',4)->orderBy('id','ASC')->get();
    //     return view('pages.admin.data_order',compact('datas','no_order','from','to'));
    // }
    public function orderan_distributor(Request $request, $role = 3){
        $status = $request->status;
        $from = date('Y-m-d',strtotime(date('Y-m-01')));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }elseif ($request->action == 'transaksi') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to, $status,'transaksi','distributor','admin',$role), 'tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to,$status,'item','distributor','admin',$role), 'tansaksi.xlsx');
        }
        $datas = Transaction::select('transactions.*')->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('transactions.order_status_id',$status)->where('users.user_role_id','=',$role)->orderBy('id','ASC')->get();
        $statuses = OrderStatus::get();
        return view('pages.admin.data_order_distributor',compact('datas','from','to','statuses','status'));
    }
    public function orderan_agen(Request $request, $role = 4){
        $status = $request->status;
        $from = date('Y-m-d',strtotime(date('Y-m-01')));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }elseif ($request->action == 'transaksi') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to, $status,'transaksi','agen','admin',$role), 'tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to,$status,'item','agen','admin',$role), 'tansaksi.xlsx');
        }
        $datas = Transaction::select('transactions.*')->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('transactions.order_status_id',$status)->where('users.user_role_id','=',$role)->orderBy('id','ASC')->get();
        $statuses = OrderStatus::get();
        return view('pages.admin.data_order_agen',compact('datas','from','to','statuses','status'));
    }
    public function orderan_enduser(Request $request,$role = 5){
        $status = $request->status;
        $from = date('Y-m-d',strtotime(date('Y-m-01')));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }elseif ($request->action == 'transaksi') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to, $status,'transaksi','enduser','admin',$role), 'tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            // $status = $request->status;
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
            return Excel::download(new TransactionExport($from,$to,$status,'item','enduser','admin',$role), 'tansaksi.xlsx');
        }
        $datas = Transaction::select('transactions.*')->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('transactions.order_status_id',$status)->where('users.user_role_id','=',$role)->orderBy('id','ASC')->get();
        $statuses = OrderStatus::get();
        return view('pages.admin.data_order_enduser',compact('datas','from','to','statuses','status'));
    }
    public function total_order(Request $request){
        $posisi = $request->posisi;
        $from = date('Y-m-d',strtotime('-7 days',strtotime(date('Y-m-d'))));
        $to = date('Y-m-d',strtotime(date('Y-m-d')));
        if ($request->action == 'cari') {
            $from = date('Y-m-d',strtotime($request->from));
            $to = date('Y-m-d',strtotime($request->to));
        }
        $datas = Item::select('product_name',DB::raw("(SUM(items.qty)) as total_qty"))
        ->join('transactions','items.transaction_id','=','transactions.id')
        ->join('users','transactions.user_id','=','users.id')
        ->wherebetween('transactions.transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])
        ->where('transactions.order_status_id',1)
        ->where('users.user_role_id',$posisi)
        ->orderBy('items.product_name','ASC')
        ->groupBy('items.product_name')
        ->get();
        $posisis = UserRole::whereIn('id',[3,4,5])->get();
        return view('pages.admin.total_order',compact('datas','from','to','posisis','posisi'));
    }
}
