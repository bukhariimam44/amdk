<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CategoryProduct;
use App\Models\Store;
use DB;
use Log;

class CategoryProductController extends Controller
{
    public function index(Request $request){
        $categories = CategoryProduct::where('open',1)->orderBy('id','ASC')->get();
        return view('pages.admin.category_product',compact('categories'));
    }
    public function create(Request $request){
        if ($request->action == 'create') {
            $request->validate([
                'category' => 'required',
            ],[
                'category.required'=>'This value is required.',
            ]);
            DB::beginTransaction();
            try {
                    $add = CategoryProduct::create([
                        'category'=>$request->category,
                        'open'=>1
                    ]);
            } catch (\Throwable $th) {
                Log::info('FAILED CATEGORY ADD = '.$th);
                DB::rollback();
                return redirect()->back()->with('error','Gagal disimpan');
            }
            DB::commit();
            return redirect()->route('admin-category-product',['store'=>$add->store_id])->with('success','Berhasil disimpan');
        }elseif ($request->action == 'hapus') {
            DB::beginTransaction();
            try {
                $stores = CategoryProduct::find($request->category_id);
                if (count($stores->product) > 0) {
                    $stores->open = 0;
                    $stores->update();
                }else {
                    $stores->delete();
                }
                
            } catch (\Throwable $th) {
                Log::info('HPS CAT = '.$th);
                DB::rollBack();
                return response()->json([
                    'code'=>400,
                    'icon'=>'error',
                    'message'=>'Gagal Hapus'
                ]);
            }
            DB::commit();
            return response()->json([
                'code'=>200,
                'icon'=>'success',
                'message'=>'Berhasil Hapus'
            ]);
            
        }elseif ($request->action == 'update') {
            $stores = CategoryProduct::where('id',$request->category_id)->first();
            $stores->category = $request->category;
            if ($stores->update()) {
                return response()->json([
                    'code'=>200,
                    'icon'=>'success',
                    'message'=>'Berhasil Update'
                ]);
            }
            return response()->json([
                'code'=>400,
                'icon'=>'error',
                'message'=>'Gagal Update'
            ]);

        }
        $categories = CategoryProduct::where('open',1)->get();
        return view('pages.admin.create_category_product',compact('categories'));
    }
}
