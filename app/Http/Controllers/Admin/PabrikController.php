<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pabrik;

class PabrikController extends Controller
{
    public function index(Request $request){
        $datas = Pabrik::where('open',1)->get();
        return view('pages.admin.pabrik.index',compact('datas'));
    }
    public function form(Request $request){
        if ($request->action == 'create') {
            $create = Pabrik::create([
                'nama_pabrik'=>$request->nama_pabrik,
                'telp'=>$request->telp,
                'alamat'=>$request->alamat,
                'admin_id'=>$request->user()->id,
                'open'=>1
            ]);
            if ($create) {
                return redirect()->route('admin-pabrik')->with('success','Berhasil');
            }
            return redirect()->back()->with('error','Gagal');
        }elseif ($request->action == 'delete') {
            $data = Pabrik::find($request->id);
            $data->admin_id = $request->user()->id;
            $data->open = 0;
            if ($data->update()) {
                return response()->json(['code'=>200]);
            }
            return response()->json(['code'=>400]);
        }
        return view('pages.admin.pabrik.create');
    }
    public function edit(Request $request, $id){
        $data = Pabrik::find($id);
        if ($request->action == 'update') {
            $data->nama_pabrik = $request->nama_pabrik;
            $data->telp = $request->telp;
            $data->alamat = $request->alamat;
            $data->admin_id = $request->user()->id;
            if ($data->update()) {
                return redirect()->route('admin-pabrik')->with('success','Berhasil');
            }
            return redirect()->back()->with('error','Gagal');
        }
        return view('pages.admin.pabrik.edit',compact('data'));
    }
}
