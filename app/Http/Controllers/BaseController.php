<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;
use App\Models\Village;

class BaseController extends Controller
{
    protected function credentials(Request $request)
    { 
        return ['username' => $request->username, 'password'=>$request->password];
    }
    public function provinsi(){
        $province = Province::where('open',1)->get();
        return response()->json($province);
    }
    public function kabupaten(Request $request){
        $kabupaten = Regency::where('province_id',$request->propinsi_id)->get();
        return response()->json($kabupaten);
    }
    public function kecamatan(Request $request){
        $kecamatan = District::where('regency_id',$request->kabupaten_id)->get();
        return response()->json($kecamatan);
    }
    public function kelurahan(Request $request){
        $kelurahan = Village::where('district_id',$request->kecamatan_id)->get();
        return response()->json($kelurahan);
    }
}
