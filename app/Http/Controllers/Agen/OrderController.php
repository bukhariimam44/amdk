<?php

namespace App\Http\Controllers\Agen;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Item;
use App\Models\Product;
use App\Models\Cart;
use App\Exports\TransactionExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\Transactions\Order;
use DB;
use Log;

class OrderController extends Controller
{
    public function index(){
        $datas = [];
        return view('pages.agen.request_order',compact('datas'));
    }
    public function request_order(Request $request){
        $datas = Transaction::where('upline_id',$request->user()->id)->where('order_status_id',1)->orderBy('id','DESC')->get();
        return view('pages.agen.request_order',compact('datas'));
    }
    public function order_sekarang(Request $request){
        $res = Order::po([
            'user_id'=>$request->user()->id,
            'upline_id'=>$request->user()->parentId->id,
            'user_role_id'=>$request->user()->user_role_id
        ])->get();
        if ($res['status']['code'] == 200) {
            return redirect()->route('agen-status-transaksi')->with('success','Order Berhasil');
        }
        return redirect()->back()->with('error','Order Gagal');
    }
    public function status_transaksi(Request $request){
        if ($request->action == 'proses') {
            $update = Transaction::where('user_id',$request->user()->id)->where('no_order',decrypt($request->no_order))->where('order_status_id',3)->first();
            $update->order_status_id = 4;
            $update->update();
            return redirect()->back();
        }
        $no_order = $request->no_order;
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->where('user_id',$request->user()->id)->orderBy('id','DESC')->get();
        return view('pages.agen.status_transaksi',compact('datas','no_order'));
    }
    public function detail_request_order(Request $request, $id){
        $datas = Transaction::where('upline_id',$request->user()->id)->where('id',decrypt($id))->first();
        return view('pages.agen.detail_order',compact('datas'));
    }
    public function detail_status_transaksi(Request $request, $id){
        $datas = Transaction::where('user_id',$request->user()->id)->where('id',decrypt($id))->first();
        return view('pages.agen.detail_transaksi',compact('datas'));
    }
    public function proses_pembayaran(Request $request){
        $datas = Transaction::where('no_order',decrypt($request->id))->where('upline_id',$request->user()->id)->where('order_status_id',1)->first();
        $datas->order_status_id = 2;
        $datas->update();
        return redirect()->route('agen-status-order');
    }
    public function status_order(Request $request){
        if ($request->action == 'proses') {
            $proses = Transaction::where('no_order',decrypt($request->no_order))->where('upline_id',$request->user()->id)->whereIn('order_status_id',[2,3])->first();
            if ($proses->order_status_id == 2) {
                $proses->order_status_id = 3;
            }elseif ($proses->order_status_id == 3) {
                $proses->order_status_id = 4;
            }
            $proses->update();
            return redirect()->back();
        }
        $no_order = $request->no_order;

        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->where('upline_id',$request->user()->id)->whereIn('order_status_id',[2,3])->orderBy('id','DESC')->get();
        return view('pages.agen.status_order',compact('datas','no_order'));
    }
    public function report_order(Request $request){
        $no_order = $request->no_order;
        $from = date('Y-m-01');
        $to = date('Y-m-d');
        $type = 'Order';
        $url = route('agen-report-order');
        if ($request->action == 'cari') {
            $from = $request->from;
            $to = $request->to;
        }elseif ($request->action == 'transaksi') {
            return Excel::download(new TransactionExport($request->from,$request->to,$status=4,'transaksi','enduser','nonadmin',$role = 5), 'report_order.xlsx');
        }elseif ($request->action == 'item') {
            return Excel::download(new TransactionExport($request->from,$request->to, $status=4,'item','enduser','nonadmin',$role = 5), 'report_order.xlsx');
        }
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->wherebetween('transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('upline_id',$request->user()->id)->where('order_status_id',4)->orderBy('id','ASC')->get();
        return view('pages.agen.report_order',compact('datas','no_order','from','to','url','type'));
    }
    public function report_transaksi(Request $request){
        $no_order = $request->no_order;
        $from = date('Y-m-01');
        $to = date('Y-m-d');
        $type = 'Transaksi';
        $url = route('agen-report-transaksi');
        if ($request->action == 'cari') {
            $from = $request->from;
            $to = $request->to;
        }elseif ($request->action == 'transaksi') {
            return Excel::download(new TransactionExport($request->from,$request->to,$status=4,'transaksi','transaksi',$role="mytrx",$roleUser = 4), 'report_tansaksi.xlsx');
        }elseif ($request->action == 'item') {
            return Excel::download(new TransactionExport($request->from,$request->to,$status=4,'item','transaksi',$role="mytrx",$roleUser = 4), 'report_tansaksi.xlsx');
        }
        $datas = Transaction::where('no_order','LIKE','%'.$no_order.'%')->wherebetween('transaction_date',[$from.' 00:00:00',$to.' 23:59:59'])->where('user_id',$request->user()->id)->where('order_status_id',4)->orderBy('id','ASC')->get();
        return view('pages.agen.report_order',compact('datas','no_order','from','to','url','type'));
    }
    public function detail_item(Request $request,$id){
        $datas = Transaction::where('id',decrypt($id))->first();
        return view('pages.agen.detail_item',compact('datas'));
    }
}
