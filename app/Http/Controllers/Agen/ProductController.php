<?php

namespace App\Http\Controllers\Agen;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Cart;

class ProductController extends Controller
{
    public function index(Request $request){
        $datas = Product::where('open',1)->orderBy('id','ASC')->get();
        return view('pages.agen.product',compact('datas'));
    }
    public function detail(Request $request, $id){
        $data = Product::find(decrypt($id));
        return view('pages.agen.detail_product',compact('data'));
    }
    public function keranjang(Request $request){
        $datas = Cart::where('user_id',$request->user()->id)->orderBy('id','ASC')->get();
        return view('pages.agen.keranjang',compact('datas'));
    }
    public function product(Request $request){
        $id = decrypt($request->product_id);
        if ($request->action == 'add') {
            $cart = Cart::updateOrCreate([
                'product_id'=>$id,
                'user_id'=>$request->user()->id
            ],[
                'jumlah'=>1
            ]);
            if ($cart) {
                return redirect()->back()->with('success','Berhasil disimpan');
            }
            return redirect()->back()->with('error','Gagal disimpan');
        }elseif ($request->action == 'delete') {
            $cart = Cart::where('product_id',$id)->first()->delete();
            if ($cart) {
                return redirect()->back()->with('success','Berhasil dihapus');
            }
            return redirect()->back()->with('error','Gagal dihapus');
        }       
        
    }
    public function qty(Request $request){
        $cart = Cart::find(decrypt($request->id));
        if ($request->action == 'tambah') {
            $cart->jumlah = $cart->jumlah + 1;
        }elseif ($request->action == 'kurang') {
            if ($cart->jumlah > 1) {
                $cart->jumlah = $cart->jumlah - 1;
            }else {
                $cart->delete();
                return redirect()->back();
            }
            
        }elseif ($request->action == 'hapus') {
            $cart->delete();
            return redirect()->back();
        }elseif ($request->action == 'ganti') {
            $cart->jumlah = $request->jumlah;
            if ($cart->update()) {
                return response()->json([
                    'code'=>200,
                    'msg'=>'Success'
                ]);
            }
            return response()->json([
                'code'=>400,
                'msg'=>'Failed'
            ]);
            
        }
        $cart->update();
        return redirect()->back();
    }
}
