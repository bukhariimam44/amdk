<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AksesPrinciple
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard="principle")
    {
        if(auth()->check() && auth()->user()->user_role_id == 2) {
            return $next($request);
        }
        return redirect(route('login'));
    }
}
