<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AksesAgen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard="user")
    {
        if(auth()->check() && auth()->user()->user_role_id == 4) {
            return $next($request);
        }
        return redirect(route('login'));
    }
}
