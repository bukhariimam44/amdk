<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class Scope
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $scope)
    {
        if (!Auth::check()) return redirect('login');
        if ($request->user()->user_role_id == 1) {
            return $next($request);
        }
        if (in_array($scope, json_decode($request->user()->actions))) {
            return $next($request);
        }else{
            return redirect()->back()->with('error','Akses ditolak');
        }
    }
}
