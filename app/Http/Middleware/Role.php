<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ... $roles)
    {
        if (!Auth::check()) return redirect('login');
        $user = Auth::user();
        if (in_array(strtolower($user->roleId->role), $roles)) {
            return $next($request);
        }else{
            return response('Unauthorized.', 401);
        }
    }
}
