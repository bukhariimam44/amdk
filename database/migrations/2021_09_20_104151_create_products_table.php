<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->foreign('category_id')
            ->references('id')->on('category_products');
            $table->string('product_name',150);
            $table->string('product_image');
            $table->double('harga_distributor',12);
            $table->double('harga_agen',12);
            $table->double('harga_end_user',12);
            $table->text('description');
            $table->integer('stok');
            $table->tinyInteger('open')->default(1);
            $table->unsignedBigInteger('admin_id');
            $table->foreign('admin_id')
            ->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
