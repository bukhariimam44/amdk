<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaldoKeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saldo_keluars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_id');
            $table->foreign('material_id')
            ->references('id')->on('materials');

            $table->unsignedBigInteger('pabrik_id');
            $table->foreign('pabrik_id')
            ->references('id')->on('pabriks');

            $table->date('transaction_date');

            $table->double('saldo');

            $table->unsignedBigInteger('admin_id');
            $table->foreign('admin_id')
            ->references('id')->on('users');
            $table->text('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saldo_keluars');
    }
}
