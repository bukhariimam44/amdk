<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePabriksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pabriks', function (Blueprint $table) {
            $table->id();
            $table->string('nama_pabrik',100);
            $table->string('telp',100);
            $table->text('alamat');
            $table->bigInteger('saldo');
            $table->unsignedBigInteger('principle_id');
            $table->foreign('principle_id')
            ->references('id')->on('users');
            $table->tinyInteger('open')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pabriks');
    }
}
