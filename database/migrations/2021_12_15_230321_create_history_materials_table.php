<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_materials', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('material_id');
            $table->foreign('material_id')
            ->references('id')->on('materials');
            $table->unsignedBigInteger('supplier_id');
            $table->foreign('supplier_id')
            ->references('id')->on('suppliers');
            $table->unsignedBigInteger('pabrik_id');
            $table->foreign('pabrik_id')
            ->references('id')->on('pabriks');
            $table->date('tanggal_terima');
            $table->bigInteger('jumlah');
            $table->unsignedBigInteger('admin_id');
            $table->foreign('admin_id')
            ->references('id')->on('users');
            $table->tinyInteger('open')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_materials');
    }
}
