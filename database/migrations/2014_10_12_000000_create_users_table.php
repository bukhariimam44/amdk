<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('parent_id')->default(0);
            $table->string('nik')->unique();
            $table->string('nama_depan');
            
            $table->string('email')->unique();
            $table->string('phone')->unique();

            $table->unsignedBigInteger('user_role_id');
            $table->foreign('user_role_id')
            ->references('id')->on('user_roles');

            $table->unsignedBigInteger('membership_id');
            $table->foreign('membership_id')
            ->references('id')->on('memberships');
            $table->string('rt');
            $table->string('rw');
            $table->text('alamat');

            $table->char('province_id',2);
            $table->foreign('province_id')
            ->references('id')->on('provinces');

            $table->char('regency_id',4);
            $table->foreign('regency_id')
            ->references('id')->on('regencies');

            $table->char('district_id',7);
            $table->foreign('district_id')
            ->references('id')->on('districts');

            $table->char('village_id',10);
            $table->foreign('village_id')
            ->references('id')->on('villages');

            $table->string('kode_pos');
            $table->string('password');
            $table->tinyInteger('open')->default(1);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
