<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserRole;

class UserRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['role'=>'Admin'],
            ['role'=>'Principle'],
            ['role'=>'Distributor'],
            ['role'=>'Agen'],
            ['role'=>'End User']
        ];
        foreach ($roles as $key => $value) {
            UserRole::create([
                'id'=>$key+1,
                'role'=>$value['role'],
            ]);
        }
    }
}
