<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Akses;

class AksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Akses::create([
            'id'=>1,
            'action'=>'Saldo Masuk'
        ]);
        Akses::create([
            'id'=>2,
            'action'=>'Saldo Keluar'
        ]);
    }
}
