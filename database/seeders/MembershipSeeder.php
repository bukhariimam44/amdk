<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Membership;

class MembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Membership::create([
            'id'=>1,
            'membership'=>'Anggota'
        ]);
        Membership::create([
            'id'=>2,
            'membership'=>'Non Anggota'
        ]);
    }
}
