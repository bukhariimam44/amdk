<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserRole;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'=>1,
            'parent_id'=>0,
            'nik'=>'234235456734',
            'nama_depan'=>'Nama Admin',
            'email'=>'admin@gmail.com',
            'phone'=>'082312543007',
            'alamat'=>'Jl. Harapan. Cipadu tangerang',
            'user_role_id'=>1,
            'rt'=>'08',
            'rw'=>'02',
            'kode_pos'=>'15155',
            'province_id'=>36,
            'regency_id'=>3671,
            'district_id'=>3671011,
            'village_id'=>3671011005,
            'membership_id'=>1,
            'password'=>Hash::make('123456'),
            'open'=>1
        ]);
        User::create([
            'id'=>2,
            'parent_id'=>1,
            'nik'=>'1234567890',
            'nama_depan'=>'Imam Bukhari',
            'email'=>'principle@gmail.com',
            'phone'=>'082312543008',
            'alamat'=>'Jl. Harapan. Cipadu tangerang',
            'user_role_id'=>2,
            'rt'=>'08',
            'rw'=>'02',
            'kode_pos'=>'15155',
            'province_id'=>36,
            'regency_id'=>3671,
            'district_id'=>3671011,
            'village_id'=>3671011005,
            'membership_id'=>1,
            'password'=>Hash::make('123456'),
            'open'=>1
        ]);
        User::create([
            'id'=>3,
            'parent_id'=>2,
            'nik'=>'5678901234',
            'nama_depan'=>'Eka Yuliana',
            'email'=>'distributor@gmail.com',
            'phone'=>'082312543009',
            'alamat'=>'Jl. Harapan. Cipadu tangerang',
            'user_role_id'=>3,
            'rt'=>'08',
            'rw'=>'02',
            'kode_pos'=>'15155',
            'province_id'=>36,
            'regency_id'=>3671,
            'district_id'=>3671011,
            'village_id'=>3671011005,
            'membership_id'=>2,
            'password'=>Hash::make('123456'),
            'open'=>1
        ]);
        User::create([
            'id'=>4,
            'parent_id'=>3,
            'nik'=>'21243547367654',
            'nama_depan'=>'Khayla Izzatunnisa Azzahra',
            'email'=>'agen@gmail.com',
            'phone'=>'082312543010',
            'alamat'=>'Jl. Harapan. Cipadu tangerang',
            'user_role_id'=>4,
            'rt'=>'08',
            'rw'=>'02',
            'kode_pos'=>'15155',
            'province_id'=>36,
            'regency_id'=>3671,
            'district_id'=>3671011,
            'village_id'=>3671011005,
            'membership_id'=>1,
            'password'=>Hash::make('123456'),
            'open'=>1
        ]);
        User::create([
            'id'=>5,
            'parent_id'=>4,
            'nik'=>'8786576667868',
            'nama_depan'=>'Shanum Mansaura',
            'email'=>'enduser@gmail.com',
            'phone'=>'082312543011',
            'alamat'=>'Jl. Harapan. Cipadu tangerang',
            'user_role_id'=>5,
            'rt'=>'08',
            'rw'=>'02',
            'kode_pos'=>'15155',
            'province_id'=>36,
            'regency_id'=>3671,
            'district_id'=>3671011,
            'village_id'=>3671011005,
            'membership_id'=>1,
            'password'=>Hash::make('123456'),
            'open'=>1
        ]);
        
    }
}
