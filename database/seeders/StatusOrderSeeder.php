<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrderStatus;

class StatusOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'id'=>1,
            'status'=>'PO'
        ]);
        OrderStatus::create([
            'id'=>2,
            'status'=>'Pembayaran'
        ]);
        OrderStatus::create([
            'id'=>3,
            'status'=>'Pengiriman'
        ]);
        OrderStatus::create([
            'id'=>4,
            'status'=>'Close'
        ]);
    }
}
