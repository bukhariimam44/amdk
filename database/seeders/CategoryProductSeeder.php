<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CategoryProduct;

class CategoryProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryProduct::create([
            'id'=>1,
            'category'=>'AMDK',
        ]);
        CategoryProduct::create([
            'id'=>2,
            'category'=>'Minyak Goreng',
        ]);
        CategoryProduct::create([
            'id'=>3,
            'category'=>'Beras',
        ]);
    }
}
