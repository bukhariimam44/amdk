var items = document.querySelector("#line_chart_datalabel");
var data = [];
console.log('DDD ' + items.dataset.grafik);
if (items !== "" || items !== null) {
    data = JSON.parse(items.dataset.grafik)
}
var options = {
        chart: {
            height: 380,
            type: "line",
            zoom: {
                enabled: !1
            },
            toolbar: {
                show: !1
            }
        },
        colors: ["#525ce5", "#23c58f"],
        dataLabels: {
            enabled: !1
        },
        stroke: {
            width: [3, 3],
            curve: "straight"
        },
        series: data,
        title: {
            text: "Total Penjualan",
            align: "left"
        },
        grid: {
            row: {
                colors: ["transparent", "transparent"],
                opacity: .2
            },
            borderColor: "#f1f1f1"
        },
        markers: {
            style: "inverted",
            size: 6
        },
        xaxis: {
            categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Des"],
            title: {
                text: "Bulan"
            }
        },
        yaxis: {
            title: {
                text: "Jumlah"
            },
            min: 1,
            max: Math.max.apply(Math, data[0]['data']),
        },
        legend: {
            position: "top",
            horizontalAlign: "right",
            floating: !0,
            offsetY: -25,
            offsetX: -5
        },
        responsive: [{
            breakpoint: 600,
            options: {
                chart: {
                    toolbar: {
                        show: !1
                    }
                },
                legend: {
                    show: !1
                }
            }
        }]
    },
    chart = new ApexCharts(document.querySelector("#line_chart_datalabel"), options).render();